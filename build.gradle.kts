/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import smartefact.gradle.fmpp.FmppTask

plugins {
    id("smartefact.convention.java11") version "0.3.0"
    id("smartefact.convention.java-library") version "0.3.0"
    id("smartefact.fmpp") version "0.3.0"
}

version = "0.3.0"

sourceSets.all {
    // Add support for Java FMPP to source set
    val srcDir = layout.projectDirectory.dir("src")
    val srcFmppDir = srcDir.dir("fmpp")
    val srcJavaFmppDir = srcDir.dir("$name/javaFmpp")
    val buildJavaFmppDir = layout.buildDirectory.dir("javaFmpp/$name")
    val buildJavaFmppJavaDir = layout.buildDirectory.dir("generated/sources/javaFmpp/java/$name")
    val prepareJavaFmppTask = tasks.create(getTaskName("prepare", "javaFmpp"), Sync::class) {
        from(srcFmppDir)
        from(srcJavaFmppDir)
        into(buildJavaFmppDir)
    }
    val generateJavaFmppTask = tasks.create(getTaskName("generate", "javaFmpp"), FmppTask::class) {
        dependsOn(prepareJavaFmppTask)
        sourceRoot.set(prepareJavaFmppTask.destinationDir)
        include("**/*.java.ftl")
        outputRoot.set(buildJavaFmppJavaDir)
    }
    java.srcDir(generateJavaFmppTask)
}

testing {
    suites {
        unitTest {
            dependencies {
                implementation("commons-codec:commons-codec:1.16.0")
            }
        }
    }
}
