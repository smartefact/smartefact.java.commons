/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.nio;

import java.nio.ByteBuffer;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class ByteBuffersTests {
    @Test
    void putAsMuchAsPossible() {
        final var buffer = ByteBuffer.allocate(10);
        final var from = ByteBuffer.allocate(10);
        assertAll(() -> {
            buffer.clear();
            from.clear().limit(5);
            assertEquals(5, ByteBuffers.putAsMuchAsPossible(buffer, from));
            assertEquals(5, buffer.position());
            assertEquals(5, from.position());
            buffer.clear().limit(5);
            from.clear().limit(10);
            assertEquals(5, ByteBuffers.putAsMuchAsPossible(buffer, from));
            assertEquals(5, buffer.position());
            assertEquals(5, from.position());
        });
    }
}
