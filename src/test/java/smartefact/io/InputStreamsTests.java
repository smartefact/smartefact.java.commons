/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class InputStreamsTests {
    @Test
    void discard() {
        assertAll(() -> {
            assertEquals(
                0L,
                InputStreams.discard(InputStream.nullInputStream(), 0L)
            );
            assertEquals(
                0L,
                InputStreams.discard(InputStream.nullInputStream(), 10L)
            );
            assertEquals(
                0L,
                InputStreams.discard(new ByteArrayInputStream(new byte[10]), 0L)
            );
            assertEquals(
                5L,
                InputStreams.discard(new ByteArrayInputStream(new byte[10]), 5L)
            );
            assertEquals(
                10L,
                InputStreams.discard(new ByteArrayInputStream(new byte[10]), 10L)
            );
            assertEquals(
                10L,
                InputStreams.discard(new ByteArrayInputStream(new byte[10]), 20L)
            );
        });
    }
}
