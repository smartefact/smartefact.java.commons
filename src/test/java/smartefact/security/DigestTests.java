/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.security;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import smartefact.util.Strings;

class DigestTests {
    @ParameterizedTest
    @CsvSource({
        "MD5, abc, 900150983cd24fb0d6963f7d28e17f72",
        "SHA-1, abc, a9993e364706816aba3e25717850c26c9cd0d89d",
        "SHA-256, abc, ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad",
    })
    void testDigest(String algorithm, String string, String digestHex) throws DecoderException {
        final var digest = Digest.createDigest(algorithm);
        assertArrayEquals(
            Hex.decodeHex(digestHex),
            digest.digest(Strings.toByteArrayUtf8(string))
        );
    }

    @Test
    void testDigestUnsupportedAlgorithm() {
        assertThrowsExactly(IllegalArgumentException.class, () -> Digest.createDigest("x"));
    }
}
