/*
 * Copyright 2022-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.text.ParseException;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class RomanNumeralFormatTests {
    @ParameterizedTest
    @MethodSource("dataNumbersAndStrings")
    void format(int number, String string) {
        assertEquals(string, RomanNumeralFormat.getUpperCaseInstance().format(number));
    }

    @ParameterizedTest
    @MethodSource("dataNumbersAndStrings")
    void parseRomanNumeral(int number, String string) throws ParseException {
        assertEquals(number, RomanNumeralFormat.parseRomanNumeral(string));
    }

    static Iterable<Arguments> dataNumbersAndStrings() {
        return List.of(
            arguments(1, "I"),
            arguments(2, "II"),
            arguments(3, "III"),
            arguments(4, "IV"),
            arguments(5, "V"),
            arguments(6, "VI"),
            arguments(7, "VII"),
            arguments(8, "VIII"),
            arguments(9, "IX"),
            arguments(10, "X"),
            arguments(11, "XI"),
            arguments(12, "XII"),
            arguments(13, "XIII"),
            arguments(14, "XIV"),
            arguments(15, "XV"),
            arguments(16, "XVI"),
            arguments(17, "XVII"),
            arguments(18, "XVIII"),
            arguments(19, "XIX"),
            arguments(20, "XX"),
            arguments(50, "L"),
            arguments(100, "C"),
            arguments(500, "D"),
            arguments(1000, "M"),
            arguments(1234, "MCCXXXIV"),
            arguments(9999, "MMMMMMMMMCMXCIX")
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "",
        "a",
        "1",
        "Ia",
        "I1"
    })
    void parseRomanNumeralIllegal(String string) {
        assertThrowsExactly(ParseException.class, () -> RomanNumeralFormat.parseRomanNumeral(string));
    }
}
