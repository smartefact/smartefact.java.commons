/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Locale;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RelativeDateTimeFormatterTests {
    static final Temporal now = ZonedDateTime.of(
        LocalDateTime.of(1977, 10, 10, 19, 40),
        ZoneId.of("Europe/Brussels")
    );

    @ParameterizedTest
    @CsvSource({
        "en, 0, SECONDS, now",
        "en, -1, SECONDS, a second ago",
        "en, -2, SECONDS, 2 seconds ago",
        "en, -1, MINUTES, a minute ago",
        "en, -2, MINUTES, 2 minutes ago",
        "en, -1, HOURS, an hour ago",
        "en, -2, HOURS, 2 hours ago",
        "en, -1, DAYS, yesterday",
        "en, -2, DAYS, 2 days ago",
        "en, -7, DAYS, last week",
        "en, -14, DAYS, 2 weeks ago",
        "en, -1, MONTHS, last month",
        "en, -2, MONTHS, 2 months ago",
        "en, -1, YEARS, last year",
        "en, -2, YEARS, 2 years ago",
        "en, -1000, YEARS, '1,000 years ago'",
        "en, 1, SECONDS, in a second",
        "en, 2, SECONDS, in 2 seconds",
        "en, 1, MINUTES, in a minute",
        "en, 2, MINUTES, in 2 minutes",
        "en, 1, HOURS, in an hour",
        "en, 2, HOURS, in 2 hours",
        "en, 1, DAYS, tomorrow",
        "en, 2, DAYS, in 2 days",
        "en, 7, DAYS, next week",
        "en, 14, DAYS, in 2 weeks",
        "en, 1, MONTHS, next month",
        "en, 2, MONTHS, in 2 months",
        "en, 1, YEARS, next year",
        "en, 2, YEARS, in 2 years",
        "en, 1000, YEARS, 'in 1,000 years'",
        // A locale for which there is no bundle should default to English
        "ja, 0, SECONDS, now",
    })
    void format(String locale, int amount, ChronoUnit unit, String expected) {
        final RelativeDateTimeFormatter formatter = new RelativeDateTimeFormatter(new Locale(locale), () -> now);
        assertEquals(expected, formatter.format(now.plus(amount, unit)));
    }
}
