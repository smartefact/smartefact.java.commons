/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.util.Locale;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DigitalSizeFormatterTests {
    @ParameterizedTest
    @CsvSource({
        "en, METRIC, FULL, 0, 0 bytes",
        "en, METRIC, FULL, 1, 1 byte",
        "en, METRIC, FULL, 2, 2 bytes",
        "en, METRIC, FULL, 999, 999 bytes",
        "en, METRIC, FULL, 1_000, 1 kilobyte",
        "en, METRIC, FULL, 1_999, 1.999 kilobytes",
        "en, METRIC, FULL, 2_000, 2 kilobytes",
        "en, METRIC, FULL, 1_000_000, 1 megabyte",
        "en, METRIC, FULL, 1_000_000_000, 1 gigabyte",
        "en, METRIC, FULL, 1_000_000_000_000, 1 terabyte",
        "en, METRIC, FULL, 1_000_000_000_000_000, 1 petabyte",
        "en, METRIC, FULL, 1_000_000_000_000_000_000, 1 exabyte",
        "en, METRIC, SYMBOL, 0, 0 B",
        "en, METRIC, SYMBOL, 1, 1 B",
        "en, METRIC, SYMBOL, 999, 999 B",
        "en, METRIC, SYMBOL, 1_000, 1 kB",
        "en, METRIC, SYMBOL, 1_999, 1.999 kB",
        "en, METRIC, SYMBOL, 2_000, 2 kB",
        "en, METRIC, SYMBOL, 1_000_000, 1 MB",
        "en, METRIC, SYMBOL, 1_000_000_000, 1 GB",
        "en, METRIC, SYMBOL, 1_000_000_000_000, 1 TB",
        "en, METRIC, SYMBOL, 1_000_000_000_000_000, 1 PB",
        "en, METRIC, SYMBOL, 1_000_000_000_000_000_000, 1 EB",
        "en, BINARY, FULL, 0, 0 bytes",
        "en, BINARY, FULL, 1, 1 byte",
        "en, BINARY, FULL, 2, 2 bytes",
        "en, BINARY, FULL, 1_023, 1023 bytes",
        "en, BINARY, FULL, 1_024, 1 kibibyte",
        "en, BINARY, FULL, 2_047, 1.999 kibibytes",
        "en, BINARY, FULL, 2_048, 2 kibibytes",
    })
    void format(
        String locale,
        DigitalSizeFormatter.Prefix prefix,
        DigitalSizeFormatter.Style style,
        long bytes,
        String expected
    ) {
        final DigitalSizeFormatter formatter = DigitalSizeFormatter.builder()
            .locale(new Locale(locale))
            .prefix(prefix)
            .style(style)
            .build();
        assertEquals(expected, formatter.format(bytes));
    }
}
