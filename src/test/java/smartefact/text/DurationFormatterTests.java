/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DurationFormatterTests {
    static final LocalDateTime REF_DATE_TIME = LocalDateTime.of(2023, 10, 10, 19, 40);

    @ParameterizedTest(name = "{0}, {6}")
    @CsvSource({
        "en, 1, 2, 3, 4, 5, 1 day 2 hours 3 minutes 4 seconds 5 milliseconds",
        "en, 1, 0, 0, 0, 0, 1 day",
        "en, 0, 1, 0, 0, 0, 1 hour",
        "en, 0, 0, 1, 0, 0, 1 minute",
        "en, 0, 0, 0, 1, 0, 1 second",
        "en, 0, 0, 0, 0, 1, 1 millisecond",
        "fr, 1, 2, 3, 4, 5, 1 jour 2 heures 3 minutes 4 secondes 5 millisecondes",
        "fr, 1, 0, 0, 0, 0, 1 jour",
        "fr, 0, 1, 0, 0, 0, 1 heure",
        "fr, 0, 0, 1, 0, 0, 1 minute",
        "fr, 0, 0, 0, 1, 0, 1 seconde",
        "fr, 0, 0, 0, 0, 1, 1 milliseconde",
    })
    void format(
        String locale,
        int days,
        int hours,
        int minutes,
        int seconds,
        int millis,
        String expected
    ) {
        final DurationFormatter formatter = new DurationFormatter(new Locale(locale));
        final Duration duration = Duration.between(
            REF_DATE_TIME,
            REF_DATE_TIME
                .plusDays(days)
                .plusHours(hours)
                .plusMinutes(minutes)
                .plusSeconds(seconds)
                .plus(millis, ChronoUnit.MILLIS)
        );
        assertEquals(expected, formatter.format(duration));
    }
}
