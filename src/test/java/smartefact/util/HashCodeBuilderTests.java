/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.junit.jupiter.api.Test;

class HashCodeBuilderTests {
    @Test
    void noProperties() {
        assertNotEquals(
            0,
            HashCodeBuilder.hashCodeBuilder()
                .build()
        );
    }

    @Test
    void propertiesOfAllTypes() {
        assertNotEquals(
            0,
            HashCodeBuilder.hashCodeBuilder()
                // Primitives
                .property(true)
                .property((byte) 1)
                .property('a')
                .property(1.0)
                .property(1.0f)
                .property(1)
                .property(1L)
                .property((short) 1)
                // Reference
                .property("abc")
                // Arrays
                .property(new boolean[] {true, false})
                .property(new byte[] {1, 2})
                .property(new char[] {'a', 'b'})
                .property(new double[] {1.0, 2.0})
                .property(new float[] {1.0f, 2.0f})
                .property(new int[] {1, 2})
                .property(new long[] {1, 2})
                .property(new short[] {1, 2})
                .property(new String[] {"abc", "def"})
                .build()
        );
    }

    @Test
    void samePropertiesHaveSameHashCode() {
        final int hashCode1 = HashCodeBuilder.hashCodeBuilder()
            .property("a")
            .property(3)
            .property(true)
            .property(new Object[] {"a", "b"})
            .build();
        final int hashCode2 = HashCodeBuilder.hashCodeBuilder()
            .property("a")
            .property(3)
            .property(true)
            .property(new Object[] {"a", "b"})
            .build();
        assertEquals(hashCode1, hashCode2);
    }

    @Test
    void differentPropertiesHaveDifferentHashCode() {
        final int hashCode1 = HashCodeBuilder.hashCodeBuilder()
            .property("a")
            .property(3)
            .property(true)
            .build();
        final int hashCode2 = HashCodeBuilder.hashCodeBuilder()
            .property("b")
            .property(3)
            .property(true)
            .build();
        final int hashCode3 = HashCodeBuilder.hashCodeBuilder()
            .property("a")
            .property(4)
            .property(true)
            .build();
        final int hashCode4 = HashCodeBuilder.hashCodeBuilder()
            .property("a")
            .property(3)
            .property(false)
            .build();
        assertAll(() -> {
            assertNotEquals(hashCode2, hashCode1);
            assertNotEquals(hashCode3, hashCode1);
            assertNotEquals(hashCode4, hashCode1);
            assertNotEquals(hashCode3, hashCode2);
            assertNotEquals(hashCode4, hashCode2);
            assertNotEquals(hashCode4, hashCode3);
        });
    }
}
