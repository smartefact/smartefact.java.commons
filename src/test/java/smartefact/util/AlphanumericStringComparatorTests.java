/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class AlphanumericStringComparatorTests {
    @Test
    void alphanumericStringComparator() {
        final List<String> strings = new ArrayList<>(List.of("b10", "b1b", "b1a", "b2", "a100", "a10", "a1", "a2", "a20", "a200"));
        strings.sort(AlphanumericStringComparator.INSTANCE);
        assertEquals(List.of("a1", "a2", "a10", "a20", "a100", "a200", "b1a", "b1b", "b2", "b10"), strings);
    }
}
