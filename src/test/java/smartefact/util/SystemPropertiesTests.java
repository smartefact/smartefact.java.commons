/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.opentest4j.AssertionFailedError;
import org.opentest4j.TestAbortedException;

class SystemPropertiesTests {
    @ParameterizedTest(name = "{0}")
    @ValueSource(strings = {
        "getFileSeparator",
        "getFileSeparatorAsChar",
        "getJavaClassPath",
        "getJavaClassPathAsList",
        "getJavaClassVersion",
        "getJavaCompiler",
        "getJavaHome",
        "getJavaHomeAsFile",
        "getJavaHomeAsPath",
        "getJavaIoTmpdir",
        "getJavaIoTmpdirAsFile",
        "getJavaIoTmpdirAsPath",
        "getJavaLibraryPath",
        "getJavaLibraryPathAsList",
        "getJavaSpecificationName",
        "getJavaSpecificationVendor",
        "getJavaSpecificationVersion",
        "getJavaSpecificationVersionAsInt",
        "getJavaVendor",
        "getJavaVendorUrl",
        "getJavaVendorVersion",
        "getJavaVersion",
        "getJavaVersionAsVersion",
        "getJavaVersionDate",
        "getJavaVersionDateAsLocalDate",
        "getJavaVmName",
        "getJavaVmSpecificationName",
        "getJavaVmSpecificationVendor",
        "getJavaVmSpecificationVersion",
        "getJavaVmSpecificationVersionAsInt",
        "getJavaVmVendor",
        "getJavaVmVersion",
        "getJavaVmVersionAsVersion",
        "getLineSeparator",
        "getLineSeparatorAsLineSeparator",
        "getOsArch",
        "getOsFamily",
        "getOsName",
        "getOsVersion",
        "getPathSeparator",
        "getPathSeparatorAsChar",
        "getUserDir",
        "getUserDirAsFile",
        "getUserDirAsPath",
        "getUserHome",
        "getUserHomeAsFile",
        "getUserHomeAsPath",
        "getUserName"
    })
    void getPropertyMethod(String methodName) {
        final Method method;
        try {
            method = SystemProperties.class.getDeclaredMethod(methodName);
        } catch (NoSuchMethodException e) {
            throw new TestAbortedException("The method <" + methodName + "> does not exist", e);
        }
        try {
            method.invoke(null);
        } catch (InvocationTargetException e) {
            throw new AssertionFailedError("Invoking the method <" + methodName + "> threw an exception", e.getCause());
        } catch (IllegalAccessException | IllegalArgumentException | NullPointerException e) {
            throw new TestAbortedException("The method <" + methodName + "> could not be invoked", e);
        }
    }
}
