/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

class LongsTests {
    @Test
    void clearBit() {
        assertAll(() -> {
            assertEquals(0b00, Integers.clearBit(0b01, 0));
            assertEquals(0b01, Integers.clearBit(0b01, 1));
            assertEquals(0x00, Integers.clearBit(0x80000000, 31));
        });
    }

    @Test
    void setBit() {
        assertAll(() -> {
            assertEquals(0b01, Integers.setBit(0b00, 0));
            assertEquals(0b10, Integers.setBit(0b00, 1));
            assertEquals(0x80000000, Integers.setBit(0x00, 31));
        });
    }

    @Test
    void testBit() {
        assertAll(() -> {
            assertTrue(Integers.testBit(0b01, 0));
            assertFalse(Integers.testBit(0b01, 1));
            assertTrue(Integers.testBit(0x80000000, 31));
        });
    }

    @Test
    void toggleBit() {
        assertAll(() -> {
            assertEquals(0b00, Integers.toggleBit(0b01, 0));
            assertEquals(0b11, Integers.toggleBit(0b01, 1));
            assertEquals(0x80000000, Integers.toggleBit(0x00, 31));
            assertEquals(0x00, Integers.toggleBit(0x80000000, 31));
        });
    }
}
