/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import smartefact.util.function.CharFunction;

@SuppressWarnings("NullAway")
class StringsTests {
    static final CharFunction<String> CHAR_TO_STRING = c -> "(" + c + ')';
    static final CharFunction<Pair<String, String>> CHAR_TO_PAIR = c -> new Pair<>("k" + c, "v" + c);
    static final BiFunction<Character, Character, String> CHAR_CHAR_TO_STRING = (c1, c2) -> new String(new char[] {c1, c2});

    @Test
    void asIterable() {
        assertIterableEquals(List.of(), Strings.asIterable(""));
        assertIterableEquals(List.of('a'), Strings.asIterable("a"));
        assertIterableEquals(List.of('a', 'b', 'c'), Strings.asIterable("abc"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.asIterable(null));
    }

    @Test
    void abbreviate() {
        assertEquals("abc", Strings.abbreviate("abc", 3, "x"));
        assertEquals("abc", Strings.abbreviate("abc", 4, "x"));
        assertEquals("x", Strings.abbreviate("abc", 1, "x"));
        assertEquals("ax", Strings.abbreviate("abc", 2, "x"));
        assertEquals("abcx", Strings.abbreviate("abcde", 4, "x"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviate(null, 1, "x"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviate("a", -1, "x"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviate("abc", 2, "xxx"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviate("a", 10, null));
    }

    @Test
    void abbreviateMiddle() {
        assertEquals("abc", Strings.abbreviateMiddle("abc", 3, "x"));
        assertEquals("abc", Strings.abbreviateMiddle("abc", 4, "x"));
        assertEquals("axd", Strings.abbreviateMiddle("abcd", 3, "x"));
        assertEquals("abxef", Strings.abbreviateMiddle("abcdef", 5, "x"));
        assertEquals("abxxf", Strings.abbreviateMiddle("abcdef", 5, "xx"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviateMiddle(null, 1, "x"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviateMiddle("a", -1, "x"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviateMiddle("abc", 2, "xxx"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.abbreviateMiddle("a", 10, null));
    }

    @Test
    void associate() {
        assertEquals(Map.of(), Strings.associate("", CHAR_TO_PAIR));
        assertEquals(Map.of("ka", "va"), Strings.associate("a", CHAR_TO_PAIR));
        assertEquals(Map.of("ka", "va", "kb", "vb", "kc", "vc"), Strings.associate("abc", CHAR_TO_PAIR));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.associate(null, CHAR_TO_PAIR));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.associate("", null));
    }

    @Test
    void associateBy() {
        assertEquals(Map.of(), Strings.associateBy("", CHAR_TO_STRING));
        assertEquals(Map.of("(a)", 'a'), Strings.associateBy("a", CHAR_TO_STRING));
        assertEquals(Map.of("(a)", 'a', "(b)", 'b', "(c)", 'c'), Strings.associateBy("abc", CHAR_TO_STRING));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.associateBy(null, CHAR_TO_STRING));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.associateBy("", null));
    }

    @Test
    void associateWith() {
        assertEquals(Map.of(), Strings.associateWith("", CHAR_TO_STRING));
        assertEquals(Map.of('a', "(a)"), Strings.associateWith("a", CHAR_TO_STRING));
        assertEquals(Map.of('a', "(a)", 'b', "(b)", 'c', "(c)"), Strings.associateWith("abc", CHAR_TO_STRING));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.associateWith(null, CHAR_TO_STRING));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.associateWith("", null));
    }

    @Test
    void capitalize() {
        assertEquals("", Strings.capitalize(""));
        assertEquals("A", Strings.capitalize("a"));
        assertEquals("A", Strings.capitalize("A"));
        assertEquals("Abc", Strings.capitalize("abc"));
        assertEquals("ABC", Strings.capitalize("ABC"));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.capitalize(null));
    }

    @Test
    void chunked() {
        assertEquals(List.of(), Strings.chunked("", 1));
        assertEquals(List.of("a"), Strings.chunked("a", 1));
        assertEquals(List.of("a", "b", "c"), Strings.chunked("abc", 1));
        assertEquals(List.of("a"), Strings.chunked("a", 2));
        assertEquals(List.of("ab"), Strings.chunked("ab", 2));
        assertEquals(List.of("ab", "c"), Strings.chunked("abc", 2));
        assertEquals(List.of("ab", "cd"), Strings.chunked("abcd", 2));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.chunked(null, 1));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.chunked("", 0));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.chunked("", -1));
    }

    @Test
    void commonPrefixWith() {
        assertEquals("", Strings.commonPrefixWith("", ""));
        assertEquals("", Strings.commonPrefixWith("", "a"));
        assertEquals("", Strings.commonPrefixWith("a", ""));
        assertEquals("a", Strings.commonPrefixWith("a", "a"));
        assertEquals("a", Strings.commonPrefixWith("aa", "ab"));
        assertEquals("ab", Strings.commonPrefixWith("aba", "abb"));
        assertEquals("", Strings.commonPrefixWith("ab", "ba"));
        assertEquals("", Strings.commonPrefixWith("a", "A"));
        assertEquals("a", Strings.commonPrefixWith("a", "A", true));
        assertEquals("A", Strings.commonPrefixWith("A", "a", true));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.commonPrefixWith(null, ""));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.commonPrefixWith("", null));
    }

    @Test
    void commonSuffixWith() {
        assertEquals("", Strings.commonSuffixWith("", ""));
        assertEquals("", Strings.commonSuffixWith("", "a"));
        assertEquals("", Strings.commonSuffixWith("a", ""));
        assertEquals("a", Strings.commonSuffixWith("a", "a"));
        assertEquals("b", Strings.commonSuffixWith("ab", "bb"));
        assertEquals("ab", Strings.commonSuffixWith("aab", "bab"));
        assertEquals("", Strings.commonSuffixWith("ab", "aa"));
        assertEquals("", Strings.commonSuffixWith("a", "A"));
        assertEquals("a", Strings.commonSuffixWith("a", "A", true));
        assertEquals("A", Strings.commonSuffixWith("A", "a", true));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.commonSuffixWith(null, ""));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.commonSuffixWith("", null));
    }

    @Test
    void contains() {
        assertFalse(Strings.contains("", 'a'));
        assertTrue(Strings.contains("a", 'a'));
        assertTrue(Strings.contains("abc", 'a'));
        assertTrue(Strings.contains("abc", 'b'));
        assertTrue(Strings.contains("abc", 'c'));
        assertFalse(Strings.contains("a", 'b'));
        assertFalse(Strings.contains("abc", 'd'));
        assertTrue(Strings.contains("a", 'A', true));
        assertTrue(Strings.contains("A", 'a', true));
        assertTrue(Strings.contains("", ""));
        assertTrue(Strings.contains("abc", ""));
        assertTrue(Strings.contains("a", "a"));
        assertTrue(Strings.contains("abc", "a"));
        assertTrue(Strings.contains("abc", "ab"));
        assertTrue(Strings.contains("abc", "bc"));
        assertFalse(Strings.contains("a", "b"));
        assertFalse(Strings.contains("abc", "ac"));
        assertTrue(Strings.contains("a", "A", true));
        assertTrue(Strings.contains("A", "a", true));
        assertTrue(Strings.contains("ABC", "abc", true));
        assertTrue(Strings.contains("abc", "ABC", true));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.contains(null, 'a'));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.contains(null, ""));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.contains("", null));
    }

    @Test
    void contentEquals() {
        assertTrue(Strings.contentEquals("", ""));
        assertTrue(Strings.contentEquals("a", "a"));
        assertTrue(Strings.contentEquals("abc", "abc"));
        assertFalse(Strings.contentEquals("", "a"));
        assertFalse(Strings.contentEquals("a", ""));
        assertFalse(Strings.contentEquals("abc", "ab"));
        assertTrue(Strings.contentEquals("a", "A", true));
        assertTrue(Strings.contentEquals("A", "a", true));
        assertTrue(Strings.contentEquals("abc", "ABC", true));
        assertTrue(Strings.contentEquals("ABC", "abc", true));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.contentEquals(null, ""));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.contentEquals("", null));
    }

    @Test
    void count() {
        assertEquals(0, Strings.count("", 'a'));
        assertEquals(0, Strings.count("b", 'a'));
        assertEquals(0, Strings.count("A", 'a'));
        assertEquals(1, Strings.count("a", 'a'));
        assertEquals(1, Strings.count("abc", 'b'));
        assertEquals(3, Strings.count("aaa", 'a'));
        assertEquals(3, Strings.count("abaca", 'a'));
        assertEquals(1, Strings.count("A", 'a', true));
        assertEquals(1, Strings.count("a", 'A', true));
        assertEquals(0, Strings.count("", c -> c == 'a'));
        assertEquals(0, Strings.count("b", c -> c == 'a'));
        assertEquals(1, Strings.count("a", c -> c == 'a'));
        assertThrowsExactly(IllegalArgumentException.class, () -> Strings.count(null, 'a'));
    }

    @Test
    void javaEscaped() {
        assertEquals("", Strings.javaEscaped(""));
        assertEquals("a", Strings.javaEscaped("a"));
        assertEquals("\\\\\\\"\\b\\t\\n\\f\\r", Strings.javaEscaped("\\\"\b\t\n\f\r"));
        assertEquals("\\u0019\\u007f", Strings.javaEscaped("\u0019\u007f"));
    }
}
