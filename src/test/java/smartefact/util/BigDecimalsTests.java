/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

class BigDecimalsTests {
    @Test
    void toBigIntegerExactOrNull() {
        final var string = "123456789012345678901234567890123456789012345678901234567890";
        assertAll(() -> {
            assertEquals(new BigInteger(string), BigDecimals.toBigIntegerExactOrNull(new BigDecimal(string)));
            assertEquals(new BigInteger(string), BigDecimals.toBigIntegerExactOrNull(new BigDecimal(string + ".0")));
            assertNull(BigDecimals.toBigIntegerExactOrNull(new BigDecimal(string + ".1")));
        });
    }
}
