/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class ToStringBuilderTests {
    static class Person {}

    static class Customer extends Person {}

    Person person = new Person();
    Customer customer = new Customer();

    @Test
    void noProperties() {
        assertEquals(
            "Person",
            ToStringBuilder.toStringBuilder(person)
                .build()
        );
    }

    @Test
    void oneProperty() {
        assertEquals(
            "Person(name=a)",
            ToStringBuilder.toStringBuilder(person)
                .property("name", "a")
                .build()
        );
    }

    @Test
    void multipleProperties() {
        assertEquals(
            "Person(name=a, age=42, customer=true)",
            ToStringBuilder.toStringBuilder(person)
                .property("name", "a")
                .property("age", 42)
                .property("customer", true)
                .build()
        );
    }

    @Test
    void inheritAndNoProperties() {
        final String personString = ToStringBuilder.toStringBuilder(person)
            .build();
        final String customerString = ToStringBuilder.toStringBuilder(customer)
            .inherited(personString)
            .build();
        assertEquals("Customer", customerString);
    }

    @Test
    void inheritAndOneInheritedProperty() {
        final String personString = ToStringBuilder.toStringBuilder(person)
            .property("name", "a")
            .build();
        final String customerString = ToStringBuilder.toStringBuilder(customer)
            .inherited(personString)
            .build();
        assertEquals("Customer(name=a)", customerString);
    }

    @Test
    void inheritAndMultipleInheritedProperties() {
        final String personString = ToStringBuilder.toStringBuilder(person)
            .property("name", "a")
            .property("age", 42)
            .property("customer", true)
            .build();
        final String customerString = ToStringBuilder.toStringBuilder(customer)
            .inherited(personString)
            .build();
        assertEquals("Customer(name=a, age=42, customer=true)", customerString);
    }

    @Test
    void inheritAndOneProperty() {
        final String personString = ToStringBuilder.toStringBuilder(person)
            .build();
        final String customerString = ToStringBuilder.toStringBuilder(customer)
            .inherited(personString)
            .property("name", "a")
            .build();
        assertEquals("Customer(name=a)", customerString);
    }

    @Test
    void inheritAndMultipleProperties() {
        final String personString = ToStringBuilder.toStringBuilder(person)
            .build();
        final String customerString = ToStringBuilder.toStringBuilder(customer)
            .inherited(personString)
            .property("name", "a")
            .property("age", 42)
            .property("customer", true)
            .build();
        assertEquals("Customer(name=a, age=42, customer=true)", customerString);
    }

    @Test
    void propertiesOfAllTypes() {
        assertEquals(
            "Person(boolean=true, byte=3, char=a, double=1.2, float=1.2, int=3, long=3, short=3, string=abc, booleanArray=[true, false], byteArray=[1, 2], charArray=[a, b], doubleArray=[1.2, 3.4], floatArray=[1.2, 3.4], intArray=[1, 2], longArray=[1, 2], shortArray=[1, 2], stringArray=[abc, def])",
            ToStringBuilder.toStringBuilder(person)
                .property("boolean", true)
                .property("byte", (byte) 3)
                .property("char", 'a')
                .property("double", 1.2)
                .property("float", 1.2f)
                .property("int", 3)
                .property("long", 3L)
                .property("short", (short) 3)
                .property("string", "abc")
                .property("booleanArray", new boolean[] {true, false})
                .property("byteArray", new byte[] {1, 2})
                .property("charArray", new char[] {'a', 'b'})
                .property("doubleArray", new double[] {1.2, 3.4})
                .property("floatArray", new float[] {1.2f, 3.4f})
                .property("intArray", new int[] {1, 2})
                .property("longArray", new long[] {1, 2})
                .property("shortArray", new short[] {1, 2})
                .property("stringArray", new String[] {"abc", "def"})
                .build()
        );
    }
}
