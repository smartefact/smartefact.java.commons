/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

class PreconditionsTests {
    @Test
    void requireTrue() {
        assertAll(() -> {
            assertDoesNotThrow(() -> Preconditions.require(true));
            assertDoesNotThrow(() -> Preconditions.require(true, "a"));
            assertDoesNotThrow(() -> Preconditions.require(true, () -> "a"));
            assertDoesNotThrow(() -> Preconditions.require(true, () -> null));
        });
    }

    @Test
    void requireFalse() {
        assertAll(() -> {
            IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> Preconditions.require(false));
            assertNull(e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalArgumentException.class, () -> Preconditions.require(false, "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalArgumentException.class, () -> Preconditions.require(false, () -> "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalArgumentException.class, () -> Preconditions.require(false, () -> null));
            assertNull(e.getMessage());
            assertNull(e.getCause());
        });
    }

    @Test
    void requireNotNullNotNull() {
        assertAll(() -> {
            assertEquals("a", Preconditions.requireNotNull("a"));
            assertEquals("a", Preconditions.requireNotNull("a", "b"));
            assertEquals("a", Preconditions.requireNotNull("a", () -> "b"));
            assertEquals("a", Preconditions.requireNotNull("a", () -> null));
        });
    }

    @Test
    void requireNotNullNull() {
        assertAll(() -> {
            IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> Preconditions.requireNotNull(null));
            assertEquals("The value is null", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalArgumentException.class, () -> Preconditions.requireNotNull(null, "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalArgumentException.class, () -> Preconditions.requireNotNull(null, () -> "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalArgumentException.class, () -> Preconditions.requireNotNull(null, () -> null));
            assertNull(e.getMessage());
            assertNull(e.getCause());
        });
    }

    @Test
    void checkTrue() {
        assertAll(() -> {
            assertDoesNotThrow(() -> Preconditions.check(true));
            assertDoesNotThrow(() -> Preconditions.check(true, "a"));
            assertDoesNotThrow(() -> Preconditions.check(true, () -> "a"));
            assertDoesNotThrow(() -> Preconditions.check(true, () -> null));
        });
    }

    @Test
    void checkFalse() {
        assertAll(() -> {
            IllegalStateException e = assertThrows(IllegalStateException.class, () -> Preconditions.check(false));
            assertNull(e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalStateException.class, () -> Preconditions.check(false, "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalStateException.class, () -> Preconditions.check(false, () -> "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalStateException.class, () -> Preconditions.check(false, () -> null));
            assertNull(e.getMessage());
            assertNull(e.getCause());
        });
    }

    @Test
    void checkNotNullNotNull() {
        assertAll(() -> {
            assertEquals("a", Preconditions.checkNotNull("a"));
            assertEquals("a", Preconditions.checkNotNull("a", "b"));
            assertEquals("a", Preconditions.checkNotNull("a", () -> "b"));
            assertEquals("a", Preconditions.checkNotNull("a", () -> null));
        });
    }

    @Test
    void checkNotNullNull() {
        assertAll(() -> {
            IllegalStateException e = assertThrows(IllegalStateException.class, () -> Preconditions.checkNotNull(null));
            assertEquals("The value is null", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalStateException.class, () -> Preconditions.checkNotNull(null, "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalStateException.class, () -> Preconditions.checkNotNull(null, () -> "a"));
            assertEquals("a", e.getMessage());
            assertNull(e.getCause());
            e = assertThrows(IllegalStateException.class, () -> Preconditions.checkNotNull(null, () -> null));
            assertNull(e.getMessage());
            assertNull(e.getCause());
        });
    }
}
