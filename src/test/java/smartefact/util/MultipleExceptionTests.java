/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.IOException;
import static java.util.Collections.emptyList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import org.junit.jupiter.api.Test;

class MultipleExceptionTests {
    @Test
    void testMaybeThrowWithNoExceptions() {
        assertDoesNotThrow(() -> MultipleException.maybeThrow(emptyList()));
    }

    @Test
    void testMaybeThrowWith1Exception() {
        final var exceptions = List.of(
            new IllegalArgumentException("a")
        );
        final var e = assertThrowsExactly(
            MultipleException.class,
            () -> MultipleException.maybeThrow(exceptions)
        );
        assertEquals(exceptions, e.getExceptions());
    }

    @Test
    void testMaybeThrowWith3Exceptions() {
        final var exceptions = List.of(
            new IllegalArgumentException("a"),
            new RuntimeException("b"),
            new IOException("c")
        );
        final var e = assertThrowsExactly(
            MultipleException.class,
            () -> MultipleException.maybeThrow(exceptions)
        );
        assertEquals(exceptions, e.getExceptions());
    }
}
