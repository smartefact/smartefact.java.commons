/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CharactersTests {
    @ParameterizedTest
    @CsvSource({
        "0, 10, 0",
        "1, 10, 1",
        "9, 10, 9",
        "a, 16, 10",
        "A, 16, 10",
        "f, 16, 15",
        "F, 16, 15",
        "7, 8, 7",
        "0, 2, 0",
        "1, 2, 1",
        "a, 10, -1",
        "a, 8, -1",
        "8, 8, -1",
        "2, 2, -1",
    })
    void digitToInt(char c, int radix, int expected) {
        assertEquals(expected, Characters.digitToInt(c, radix));
    }
}
