/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@SuppressWarnings("NullAway")
class ObjectsTests {
    @ParameterizedTest
    @CsvSource({
        "1, 1, 1",
        "1, 0, 1",
        "1, 2, 2",
    })
    void coerceAtLeast(int value, int minValue, int expected) {
        assertEquals(expected, Objects.coerceAtLeast(value, minValue));
    }

    @Test
    void coerceAtLeastIllegal() {
        assertAll(() -> {
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceAtLeast(null, 1));
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceAtLeast(1, null));
        });
    }

    @ParameterizedTest
    @CsvSource({
        "1, 1, 1",
        "1, 2, 1",
        "1, 0, 0",
    })
    void coerceAtMost(int value, int maxValue, int expected) {
        assertEquals(expected, Objects.coerceAtMost(value, maxValue));
    }

    @Test
    void coerceAtMostIllegal() {
        assertAll(() -> {
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceAtMost(null, 1));
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceAtMost(1, null));
        });
    }

    @ParameterizedTest
    @CsvSource({
        "1, 0, 2, 1",
        "1, 1, 1, 1",
        "1, 0, 1, 1",
        "1, 1, 2, 1",
        "0, 1, 2, 1",
        "3, 1, 2, 2",
    })
    void coerceIn(int value, int minValue, int maxValue, int expected) {
        assertEquals(expected, Objects.coerceIn(value, minValue, maxValue));
    }

    @Test
    void coerceInIllegal() {
        assertAll(() -> {
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceIn(null, 1, 1));
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceIn(1, null, 1));
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceIn(1, 1, null));
            assertThrowsExactly(IllegalArgumentException.class, () -> Objects.coerceIn(1, 2, 1));
        });
    }

    @ParameterizedTest
    @CsvSource({
        "a, a, true",
        "a, b, false",
        "a, , false",
        ", a, false",
        ", , true",
    })
    void equalsMethod(Object object, Object other, boolean expected) {
        assertEquals(expected, Objects.equals(object, other));
    }

    @Test
    void hashCodeMethod() {
        assertAll(() -> {
            assertEquals("a".hashCode(), Objects.hashCode("a"));
            assertEquals(0, Objects.hashCode(null));
        });
    }

    @ParameterizedTest
    @CsvSource({
        "a, b, a",
        ", b, b",
        ", , ",
    })
    void orElse(Object object, Object defaultValue, Object expected) {
        assertEquals(expected, Objects.orElse(object, defaultValue));
    }

    @Test
    void takeIf() {
        assertAll(() -> {
            assertEquals("a", Objects.takeIf("a", it -> it.equals("a")));
            assertNull(Objects.takeIf("a", it -> it.equals("b")));
            assertNull(Objects.takeIf(null, java.util.Objects::isNull));
            assertNull(Objects.takeIf(null, java.util.Objects::nonNull));
        });
    }

    @Test
    void takeIfIllegal() {
        assertThrowsExactly(IllegalArgumentException.class, () -> Objects.takeIf("a", null));
    }

    @Test
    void takeUnless() {
        assertAll(() -> {
            assertNull(Objects.takeUnless("a", it -> it.equals("a")));
            assertEquals("a", Objects.takeUnless("a", it -> it.equals("b")));
            assertNull(Objects.takeUnless(null, java.util.Objects::isNull));
            assertNull(Objects.takeUnless(null, java.util.Objects::nonNull));
        });
    }

    @Test
    void takeUnlessIllegal() {
        assertThrowsExactly(IllegalArgumentException.class, () -> Objects.takeUnless("a", null));
    }

    @Test
    void toStringMethod() {
        assertAll(() -> {
            assertEquals("a", Objects.toString("a"));
            assertNull(Objects.toString(null));
        });
    }
}
