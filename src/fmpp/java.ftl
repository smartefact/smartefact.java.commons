<#ftl strip_text=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#assign primitivesToWrappers = {
    "boolean": "Boolean",
    "byte": "Byte",
    "char": "Character",
    "double": "Double",
    "float": "Float",
    "int": "Integer",
    "long": "Long",
    "short": "Short"
}>

<#assign primitives = primitivesToWrappers?keys>

<#assign wrappers = primitivesToWrappers?values>

<#assign primitivesAndObject = primitives + [ "Object" ]>

<#assign stdFunctionalInterfaces = [
    "DoubleBinaryOperator",
    "DoubleConsumer",
    "DoubleFunction",
    "DoublePredicate",
    "DoubleSupplier",
    "DoubleToIntFunction",
    "DoubleToLongFunction",
    "DoubleUnaryOperator",
    "IntBinaryOperator",
    "IntConsumer",
    "IntFunction",
    "IntPredicate",
    "IntSupplier",
    "IntToDoubleFunction",
    "IntToLongFunction",
    "IntUnaryOperator",
    "LongBinaryOperator",
    "LongConsumer",
    "LongFunction",
    "LongPredicate",
    "LongSupplier",
    "LongToDoubleFunction",
    "LongToIntFunction",
    "LongUnaryOperator",
    "ToDoubleBiFunction",
    "ToDoubleFunction",
    "ToIntBiFunction",
    "ToIntFunction",
    "ToLongBiFunction",
    "ToLongFunction"
]>

<#function isPrimitive t>
    <#return primitivesToWrappers[t]??>
</#function>

<#function wrapper t>
    <#return primitivesToWrappers[t]!t>
</#function>

<#macro generated>
    <#lt>@javax.annotation.processing.Generated(value = {"fmpp", "${pp.sourceFile}", "FMPP ${pp.version}", "FreeMarker ${.version}"}, date = "${.now?datetime?iso_utc}")
</#macro>

<#macro javaFile c>
    <@pp.changeOutputFile name=c + ".java" />
</#macro>
