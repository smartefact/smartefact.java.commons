<#ftl strip_text=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#include "util.ftl">
<#include "java.ftl">

<#function emptyPrimitiveArrayConstant p>
    <#return "EMPTY_" + p?upper_case + "_ARRAY">
</#function>

<#function emptyWrapperArrayConstant w>
    <#return "EMPTY_" + w?upper_case + "_OBJECT_ARRAY">
</#function>

<#function consumer t>
    <#if (isPrimitive(t))>
        <#return capitalize(t) + "Consumer">
    </#if>
    <#return "Consumer<? super " + t + ">">
</#function>

<#function indexedConsumer t>
    <#if (isPrimitive(t))>
        <#return "Indexed" + capitalize(t) + "Consumer">
    </#if>
    <#return "IndexedConsumer<? super " + t + ">">
</#function>

<#function predicate t>
    <#if (isPrimitive(t))>
        <#return capitalize(t) + "Predicate">
    </#if>
    <#return "Predicate<? super " + t + ">">
</#function>

<#function indexedPredicate t>
    <#if (isPrimitive(t))>
        <#return "Indexed" + capitalize(t) + "Predicate">
    </#if>
    <#return "IndexedPredicate<? super " + t + ">">
</#function>

<#function func t r>
    <#if (isPrimitive(t))>
        <#if (isPrimitive(r))>
            <#return capitalize(t) + "To" + capitalize(r) + "Function">
        </#if>
        <#return capitalize(t) + "Function<? extends " + r + ">">
    </#if>
    <#if (isPrimitive(r))>
        <#return "To" + capitalize(r) + "Function<? extends " + r + ">">
    </#if>
    <#return "Function<? super " + t + ", ? extends " + r + ">">
</#function>

<#function indexedFunc t r>
    <#if (isPrimitive(t))>
        <#return "Indexed" + capitalize(t) + "Function<? extends " + r + ">">
    </#if>
    <#return "IndexedFunction<? super " + t + ", ? extends " + r + ">">
</#function>

<#function equals t v1 v2>
    <#if (isPrimitive(t))>
        <#return v1 + " == " + v2>
    </#if>
    <#return "Objects.equals(" + v1 + ", " + v2 + ")">
</#function>

<#function toString t>
    <#if (isPrimitive(t))>
        <#return "String.valueOf">
    </#if>
    <#return "Objects.toString">
</#function>
