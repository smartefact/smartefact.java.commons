<#ftl strip_whitespace=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#include "/smartefact.ftl">

<@pp.dropOutputFile />

<#list primitives as p>

<#assign cp = capitalize(p)>
<#assign w = wrapper(p)>
<#assign class = "Indexed" + cp>

<@javaFile class />

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.ToStringBuilder.toStringBuilder;

/**
 * {@code ${p}} value and its index.
 *
 * @author Laurent Pireyn
 */
<@generated />
public final class ${class} {
    private final int index;
    private final ${p} value;

    public ${class}(int index, ${p} value) {
        this.index = index;
        this.value = value;
    }

    @Contract(pure = true)
    public int index() {
        return index;
    }

    @Contract(pure = true)
    public ${p} value() {
        return value;
    }

    @Contract(value = "-> new", pure = true)
    public IndexedValue<${w}> asIndexedValue() {
        return new IndexedValue<>(index, value);
    }

    @Override
    @Contract(pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ${class})) {
            return false;
        }
        final ${class} other = (${class}) object;
        return index == other.index
            && value == other.value;
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCodeBuilder()
            .property(index)
            .property(value)
            .build();
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("index", index)
            .property("value", value)
            .build();
    }
}

</#list>
