<#ftl strip_whitespace=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#include "/smartefact.ftl">

<@pp.dropOutputFile />

<#list primitives as p>

<#assign cp = capitalize(p)>
<#assign iface = "Indexed" + cp + "Predicate">

<@javaFile iface />

package smartefact.util.function;

import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Predicate on {@code ${p}} and its index.
 *
 * @author Laurent Pireyn
 */
@FunctionalInterface
<@generated />
public interface ${iface} {
    @Contract(value = "_ -> new", pure = true)
    static ${iface} not(${iface} predicate) {
        requireNotNull(predicate);
        return predicate.negate();
    }

    boolean test(int index, ${p} value);

    default ${iface} negate() {
        return (index, value) -> !test(index, value);
    }

    default ${iface} and(${iface} other) {
        requireNotNull(other);
        return (index, value) -> test(index, value) && other.test(index, value);
    }

    default ${iface} or(${iface} other) {
        requireNotNull(other);
        return (index, value) -> test(index, value) || other.test(index, value);
    }
}

</#list>
