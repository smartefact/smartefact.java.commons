<#ftl strip_whitespace=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#include "/smartefact.ftl">

<@pp.dropOutputFile />

<#list primitives as p>

<#assign cp = capitalize(p)>
<#assign w = wrapper(p)>

<#assign iface = cp + "BinaryOperator">

<#if (stdFunctionalInterfaces?seq_contains(iface))>
    <#continue>
</#if>

<@javaFile iface />

package smartefact.util.function;

/**
 * Binary {@code ${p}} operator.
 *
 * @author Laurent Pireyn
 */
@FunctionalInterface
<@generated />
public interface ${iface} {
    /**
     * Applies this operator to the given operands.
     *
     * @param left the left operand
     * @param right the right operand
     * @return the result
     */
    ${p} applyAs${cp}(${p} left, ${p} right);
}

</#list>
