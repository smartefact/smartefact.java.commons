<#ftl strip_whitespace=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#include "/smartefact.ftl">

<@pp.dropOutputFile />

<#list primitives as p>

<#assign cp = capitalize(p)>

<#list primitives as p2>

<#assign cp2 = capitalize(p2)>

<#assign iface = cp + "To" + cp2 + "Function">

<#if (stdFunctionalInterfaces?seq_contains(iface))>
    <#continue>
</#if>

<#if (p2 == "boolean")>
    <#-- Skip XToBoolean functions, as they are essentially XPredicates -->
    <#continue>
</#if>

<@javaFile iface />

package smartefact.util.function;

/**
* Function of {@code ${p}} to {@code ${p2}}.
*
* @author Laurent Pireyn
*/
@FunctionalInterface
<@generated />
public interface ${iface} {
    <#if (p == p2)>
        static ${iface} identity() {
            return value -> value;
        }
    </#if>

    /**
     * Applies this function to the given argument.
     *
     * @param value the argument
     * @return the result
     */
    ${p2} applyAs${cp2}(${p} value);
}

</#list>

</#list>
