<#ftl strip_whitespace=true>

<#--
 - Copyright 2023-2024 Smartefact
 -
 - Licensed under the Apache License, Version 2.0 (the "License");
 - you may not use this file except in compliance with the License.
 - You may obtain a copy of the License at
 -
 -     http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS,
 - WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 - See the License for the specific language governing permissions and
 - limitations under the License.
 -->

<#include "/smartefact.ftl">

<#assign primitivesAndE = primitives + [ "E" ]>

<#macro typeVars t vars...>
    <#if (!isPrimitive(t))><#t>
        <#local vars = [t] + vars><#t>
    </#if>
    <#if (vars?has_content)><#t>
        <${vars?join(", ")}><#t>
    </#if>
</#macro>

package smartefact.util;

import java.io.IOException;
import static java.lang.Math.*;
import java.util.*;
import static java.util.Collections.*;
import java.util.function.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.*;
import smartefact.util.function.*;

/**
 * Utilities for arrays.
 *
 * @author Laurent Pireyn
 */
<@generated />
@SuppressWarnings("MixedMutabilityReturnType")
public final class Arrays {
    <#list primitives as t>
        /**
         * Empty {@code ${t}} array.
         */
        public static final ${t}[] ${emptyPrimitiveArrayConstant(t)} = {};
    </#list>

    /**
     * Empty array of {@link Object}s.
     */
    public static final Object[] EMPTY_OBJECT_ARRAY = {};

    <#list wrappers as t>
        /**
         * Empty array of {@link ${t}}s.
         */
        public static final ${t}[] ${emptyWrapperArrayConstant(t)} = {};
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> boolean all(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (final ${t} element : array) {
                if (!predicate.test(element)) {
                    return false;
                }
            }
            return true;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> boolean any(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (final ${t} element : array) {
                if (predicate.test(element)) {
                    return true;
                }
            }
            return false;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "K", "V" /> Map<K, V> associate(
            ${t}[] array,
            ${func(t, "Pair<? extends K, ? extends V>")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            if (isEmpty(array)) {
                return emptyMap();
            }
            return associateTo(
                array,
                new HashMap<>(array.length),
                transform
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "K" /> Map<K, ${wrapper(t)}> associateBy(
            ${t}[] array,
            ${func(t, "K")} keySelector
        ) {
            requireNotNull(array);
            requireNotNull(keySelector);
            if (isEmpty(array)) {
                return emptyMap();
            }
            return associateByTo(
                array,
                new HashMap<>(array.length),
                keySelector
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "K", "M extends Map<? super K, ? super ${wrapper(t)}>" /> M associateByTo(
            ${t}[] array,
            M destination,
            ${func(t, "K")} keySelector
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(keySelector);
            for (final ${t} element : array) {
                final K key = keySelector.apply(element);
                if (key != null) {
                    destination.put(key, element);
                }
            }
            return destination;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "K", "V", "M extends Map<? super K, ? super V>" /> M associateTo(
            ${t}[] array,
            M destination,
            ${func(t, "Pair<? extends K, ? extends V>")} transform
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(transform);
            for (final ${t} element : array) {
                final Pair<? extends K, ? extends V> pair = transform.apply(element);
                if (pair != null) {
                    destination.put(pair.first(), pair.second());
                }
            }
            return destination;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "V" /> Map<${wrapper(t)}, V> associateWith(
            ${t}[] array,
            ${func(t, "V")} valueSelector
        ) {
            requireNotNull(array);
            requireNotNull(valueSelector);
            if (isEmpty(array)) {
                return emptyMap();
            }
            return associateWithTo(
                array,
                new HashMap<>(array.length),
                valueSelector
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "V", "M extends Map<? super ${wrapper(t)}, ? super V>" /> M associateWithTo(
            ${t}[] array,
            M destination,
            ${func(t, "V")} valueSelector
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(valueSelector);
            for (final ${t} element : array) {
                destination.put(element, valueSelector.apply(element));
            }
            return destination;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> List<List<${wrapper(t)}>> chunked(
            ${t}[] array,
            int size
        ) {
            return chunked(
                array,
                size,
                Function.identity()
            );
        }
    </#list>

    <#list primitivesAndE as t>
        <#assign w = wrapper(t)>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> List<R> chunked(
            ${t}[] array,
            int size,
            Function<? super List<${wrapper(t)}>, ? extends R> transform
        ) {
            requireNotNull(array);
            requireGt0(size);
            requireNotNull(transform);
            if (isEmpty(array)) {
                return emptyList();
            }
            final List<R> result = new ArrayList<>(array.length / size + 1);
            List<${w}> slice = null;
            for (int i = 0; i < array.length; ++i) {
                if (slice == null) {
                    slice = new ArrayList<>(size);
                }
                slice.add(array[i]);
                if (slice.size() == size) {
                    result.add(transform.apply(slice));
                    slice = null;
                }
            }
            if (slice != null) {
                result.add(transform.apply(slice));
            }
            return result;
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract(pure = true)
        public static boolean contains(
            ${t}[] array,
            ${t} value
        ) {
            requireNotNull(array);
            for (final ${t} element : array) {
                if (${equals(t, "element", "value")}) {
                    return true;
                }
            }
            return false;
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract(pure = true)
        public static boolean containsAll(
            ${t}[] array,
            ${t}[] values
        ) {
            requireNotNull(array);
            requireNotNull(values);
            if (isEmpty(array)) {
                return false;
            }
            if (isEmpty(values)) {
                return true;
            }
            loop_values: for (final ${t} value : values) {
                for (final ${t} element : array) {
                    if (${equals(t, "value", "element")}) {
                        continue loop_values;
                    }
                }
                return false;
            }
            return true;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> int count(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            int result = 0;
            for (final ${t} element : array) {
                if (predicate.test(element)) {
                    ++result;
                }
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        <#if (isPrimitive(t))>
            <#assign cp = capitalize(t)>
            <#if (["double", "int", "long"]?seq_contains(t))>
                <#assign identity = cp + "Functions.identity()">
            <#else>
                <#assign identity = cp + "Function.identity()">
            </#if>
        <#else>
            <#assign identity = "Function.identity()">
        </#if>
        @Contract(pure = true)
        public static <@typeVars t /> List<${wrapper(t)}> distinct(${t}[] array) {
            return distinctBy(
                array,
                ${identity}
            );
        }
    </#list>

    <#list primitivesAndE as t>
        <#assign w = wrapper(t)>
        @Contract(pure = true)
        public static <@typeVars t, "K" /> List<${w}> distinctBy(
            ${t}[] array,
            ${func(t, "K")} selector
        ) {
            requireNotNull(array);
            requireNotNull(selector);
            final List<${w}> result = new ArrayList<>(array.length);
            final Set<K> set = new HashSet<>(array.length);
            for (final ${t} element : array) {
                if (set.add(selector.apply(element))) {
                    result.add(element);
                }
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> List<${wrapper(t)}> drop(
            ${t}[] array,
            int n
        ) {
            requireNotNull(array);
            requireGe0(n);
            if (n >= array.length) {
                return emptyList();
            }
            return toCollection(
                array,
                new ArrayList<>(array.length - n),
                n,
                array.length
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> List<${wrapper(t)}> dropLast(
            ${t}[] array,
            int n
        ) {
            requireNotNull(array);
            requireGe0(n);
            if (n >= array.length) {
                return emptyList();
            }
            return toCollection(
                array,
                new ArrayList<>(array.length - n),
                0,
                array.length - n
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> ${t} first(${t}[] array) {
            if (isEmpty(array)) {
                throw new NoSuchElementException();
            }
            return array[0];
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> ${t} first(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (final ${t} element : array) {
                if (predicate.test(element)) {
                    return element;
                }
            }
            throw new NoSuchElementException();
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> R firstNotNullOf(
            ${t}[] array,
            ${func(t, "R")} transform
        ) {
            final R result = firstNotNullOfOrNull(
                array,
                transform
            );
            if (result == null) {
                throw new NoSuchElementException();
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> @Nullable R firstNotNullOfOrNull(
            ${t}[] array,
            ${func(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            for (final ${t} element : array) {
                final R result = transform.apply(element);
                if (result != null) {
                    return result;
                }
            }
            return null;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> @Nullable ${wrapper(t)} firstOrNull(${t}[] array) {
            if (isEmpty(array)) {
                return null;
            }
            return array[0];
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> @Nullable ${wrapper(t)} firstOrNull(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (final ${t} element : array) {
                if (predicate.test(element)) {
                    return element;
                }
            }
            return null;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> R fold(
            ${t}[] array,
            R initial,
            BiFunction<? super R, ? super ${wrapper(t)}, ? extends R> operation
        ) {
            requireNotNull(array);
            requireNotNull(operation);
            R result = initial;
            for (final ${t} element : array) {
                result = operation.apply(result, element);
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> R foldIndexed(
            ${t}[] array,
            R initial,
            IndexedBiFunction<? super R, ? super ${wrapper(t)}, ? extends R> operation
        ) {
            requireNotNull(array);
            requireNotNull(operation);
            R result = initial;
            for (int i = 0; i < array.length; ++i) {
                result = operation.apply(i, result, array[i]);
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> R foldRight(
            ${t}[] array,
            R initial,
            BiFunction<? super R, ? super ${wrapper(t)}, ? extends R> operation
        ) {
            requireNotNull(array);
            requireNotNull(operation);
            R result = initial;
            for (int i = array.length - 1; i >= 0; --i) {
                result = operation.apply(result, array[i]);
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> R foldRightIndexed(
            ${t}[] array,
            R initial,
            IndexedBiFunction<? super R, ? super ${wrapper(t)}, ? extends R> operation
        ) {
            requireNotNull(array);
            requireNotNull(operation);
            R result = initial;
            for (int i = array.length - 1; i >= 0; --i) {
                result = operation.apply(i, result, array[i]);
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> void forEach(
            ${t}[] array,
            ${consumer(t)} action
        ) {
            requireNotNull(array);
            requireNotNull(action);
            for (final ${t} element : array) {
                action.accept(element);
            }
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> void forEachIndexed(
            ${t}[] array,
            ${indexedConsumer(t)} action
        ) {
            requireNotNull(array);
            requireNotNull(action);
            for (int i = 0; i < array.length; ++i) {
                action.accept(i, array[i]);
            }
        }
    </#list>

    <#list primitives as p>
        @Contract(pure = true)
        public static int indexOf(
            ${p}[] array,
            ${p} value
        ) {
            requireNotNull(array);
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == value) {
                    return i;
                }
            }
            return -1;
        }
    </#list>

    @Contract(pure = true)
    public static int indexOf(
        Object[] array,
        Object value
    ) {
        requireNotNull(array);
        for (int i = 0; i < array.length; ++i) {
            if (Objects.equals(array[i], value)) {
                return i;
            }
        }
        return -1;
    }

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> int indexOfFirst(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (int i = 0; i < array.length; ++i) {
                if (predicate.test(array[i])) {
                    return i;
                }
            }
            return -1;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> int indexOfLast(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (int i = array.length - 1; i >= 0; --i) {
                if (predicate.test(array[i])) {
                    return i;
                }
            }
            return -1;
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract(pure = true)
        public static boolean isEmpty(${t}[] array) {
            requireNotNull(array);
            return array.length == 0;
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract(pure = true)
        public static boolean isNullOrEmpty(${t} @Nullable [] array) {
            return array == null || array.length == 0;
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract("_, _, _ -> param2")
        public static <A extends Appendable> A joinTo(
            ${t}[] array,
            A buffer,
            JoinToString config
        ) throws IOException {
            return joinTo(
                array,
                buffer,
                config,
                element -> ${toString(t)}(element)
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _, _ -> param2")
        public static <@typeVars t, "A extends Appendable" /> A joinTo(
            ${t}[] array,
            A buffer,
            JoinToString config,
            ${func(t, "@Nullable CharSequence")} transform
        ) throws IOException {
            requireNotNull(array);
            requireNotNull(buffer);
            requireNotNull(config);
            requireNotNull(transform);
            buffer.append(config.prefix);
            int count = 0;
            for (final ${t} element : array) {
                final @Nullable CharSequence value = transform.apply(element);
                if (value == null && config.skipNulls) {
                    continue;
                }
                if (count > 0) {
                    buffer.append(config.separator);
                }
                if (count == config.limit) {
                    buffer.append(config.truncated);
                    break;
                }
                buffer.append(value);
                ++count;
            }
            buffer.append(config.postfix);
            return buffer;
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract("_, _, _ -> param2")
        public static StringBuilder joinTo(
            ${t}[] array,
            StringBuilder buffer,
            JoinToString config
        ) {
            return joinTo(
                array,
                buffer,
                config,
                element -> ${toString(t)}(element)
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _, _ -> param2")
        public static <@typeVars t /> StringBuilder joinTo(
            ${t}[] array,
            StringBuilder buffer,
            JoinToString config,
            ${func(t, "@Nullable CharSequence")} transform
        ) {
            try {
                return (StringBuilder) joinTo(
                    array,
                    (Appendable) buffer,
                    config,
                    transform
                );
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract(pure = true)
        public static String joinToString(
            ${t}[] array,
            JoinToString config
        ) {
            return joinToString(
                array,
                config,
                element -> ${toString(t)}(element)
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> String joinToString(
            ${t}[] array,
            JoinToString config,
            ${func(t, "@Nullable CharSequence")} transform
        ) {
            return joinTo(
                array,
                new StringBuilder(),
                config,
                transform
            )
                .toString();
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> ${t} last(${t}[] array) {
            if (isEmpty(array)) {
                throw new NoSuchElementException();
            }
            return array[lastIndex(array)];
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> ${t} last(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (int i = array.length - 1; i >= 0; --i) {
                final ${t} element = array[i];
                if (predicate.test(element)) {
                    return element;
                }
            }
            throw new NoSuchElementException();
        }
    </#list>

    <#list primitivesAndObject as t>
        @Contract(pure = true)
        public static int lastIndex(${t}[] array) {
            requireNotNull(array);
            return array.length - 1;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> R lastNotNullOf(
            ${t}[] array,
            ${func(t, "R")} transform
        ) {
            final R result = lastNotNullOfOrNull(
                array,
                transform
            );
            if (result == null) {
                throw new NoSuchElementException();
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> @Nullable R lastNotNullOfOrNull(
            ${t}[] array,
            ${func(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            for (int i = array.length - 1; i >= 0; --i) {
                final R result = transform.apply(array[i]);
                if (result != null) {
                    return result;
                }
            }
            return null;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> @Nullable ${wrapper(t)} lastOrNull(${t}[] array) {
            if (isEmpty(array)) {
                return null;
            }
            return array[lastIndex(array)];
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> @Nullable ${wrapper(t)} lastOrNull(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (int i = array.length - 1; i >= 0; --i) {
                final ${t} element = array[i];
                if (predicate.test(element)) {
                    return element;
                }
            }
            return null;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> List<R> map(
            ${t}[] array,
            ${func(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            if (isEmpty(array)) {
                return emptyList();
            }
            return mapTo(
                array,
                new ArrayList<>(array.length),
                transform
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> List<R> mapIndexed(
            ${t}[] array,
            ${indexedFunc(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            if (isEmpty(array)) {
                return emptyList();
            }
            return mapIndexedTo(
                array,
                new ArrayList<>(array.length),
                transform
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> List<R> mapIndexedNotNull(
            ${t}[] array,
            ${indexedFunc(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            if (isEmpty(array)) {
                return emptyList();
            }
            return mapIndexedNotNullTo(
                array,
                new ArrayList<>(array.length),
                transform
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "R", "C extends Collection<? super R>" /> C mapIndexedNotNullTo(
            ${t}[] array,
            C destination,
            ${indexedFunc(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(transform);
            for (int i = 0; i < array.length; ++i) {
                final R value = transform.apply(i, array[i]);
                if (value != null) {
                    destination.add(value);
                }
            }
            return destination;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "R", "C extends Collection<? super R>" /> C mapIndexedTo(
            ${t}[] array,
            C destination,
            ${indexedFunc(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(transform);
            for (int i = 0; i < array.length; ++i) {
                destination.add(transform.apply(i, array[i]));
            }
            return destination;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t, "R" /> List<R> mapNotNull(
            ${t}[] array,
            ${func(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(transform);
            if (isEmpty(array)) {
                return emptyList();
            }
            return mapNotNullTo(
                array,
                new ArrayList<>(array.length),
                transform
            );
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "R", "C extends Collection<? super R>" /> C mapNotNullTo(
            ${t}[] array,
            C destination,
            ${func(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(transform);
            for (final ${t} element : array) {
                final R value = transform.apply(element);
                if (value != null) {
                    destination.add(value);
                }
            }
            return destination;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract("_, _, _ -> param2")
        public static <@typeVars t, "R", "C extends Collection<? super R>" /> C mapTo(
            ${t}[] array,
            C destination,
            ${func(t, "R")} transform
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            requireNotNull(transform);
            for (final ${t} element : array) {
                destination.add(transform.apply(element));
            }
            return destination;
        }
    </#list>


    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> boolean none(
            ${t}[] array,
            ${predicate(t)} predicate
        ) {
            requireNotNull(array);
            requireNotNull(predicate);
            for (final ${t} element : array) {
                if (predicate.test(element)) {
                    return false;
                }
            }
            return true;
        }
    </#list>

    <#list primitives as p>
        <#assign cp = capitalize(p)>
        @Contract(pure = true)
        public static ${p} reduce(
            ${p}[] array,
            ${cp}BinaryOperator operation
        ) {
            requireNotNull(array);
            requireNotNull(operation);
            if (isEmpty(array)) {
                throw new NoSuchElementException();
            }
            ${p} result = array[0];
            for (int i = 1; i < array.length; ++i) {
                result = operation.applyAs${cp}(result, array[i]);
            }
            return result;
        }
    </#list>

    <#list primitives as p>
        <#assign cp = capitalize(p)>
        @Contract(pure = true)
        public static ${p} reduceRight(
            ${p}[] array,
            ${cp}BinaryOperator operation
        ) {
            requireNotNull(array);
            requireNotNull(operation);
            if (isEmpty(array)) {
                throw new NoSuchElementException();
            }
            ${p} result = array[array.length - 1];
            for (int i = array.length - 2; i >= 0; --i) {
                result = operation.applyAs${cp}(result, array[i]);
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> ${t} single(${t}[] array) {
            if (isEmpty(array)) {
                throw new NoSuchElementException();
            }
            if (array.length > 1) {
                throw new IllegalArgumentException();
            }
            return array[0];
        }
    </#list>

    <#list primitivesAndE as t>
        @Contract(pure = true)
        public static <@typeVars t /> @Nullable ${wrapper(t)} singleOrNull(${t}[] array) {
            if (isEmpty(array) || array.length > 1) {
                return null;
            }
            return array[0];
        }
    </#list>

    <#list primitives as p>
        <#assign w = wrapper(p)>
        @Contract(value = "_ -> new", pure = true)
        public static ${w}[] to${w}ObjectArray(${p}[] array) {
            requireNotNull(array);
            final ${w}[] result = new ${w}[array.length];
            for (int i = 0; i < array.length; ++i) {
                result[i] = array[i];
            }
            return result;
        }
    </#list>

    <#list primitives as p>
        <#assign cp = capitalize(p)>
        <#assign w = wrapper(p)>
        @Contract(value = "_ -> new", pure = true)
        public static ${p}[] to${cp}Array(${w}[] array) {
            requireNotNull(array);
            final ${p}[] result = new ${p}[array.length];
            for (int i = 0; i < array.length; ++i) {
                final ${w} element = array[i];
                // Inlined requireNotNull
                if (element == null) {
                    throw new IllegalArgumentException("The element at index <" + i + "> in the array is null");
                }
                result[i] = element;
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        <#assign w = wrapper(t)>
        @Contract(value = "_ -> new", pure = true)
        @SuppressWarnings("NonApiType")
        public static <@typeVars t /> ArrayList<${w}> toArrayList(${t}[] array) {
            requireNotNull(array);
            final ArrayList<${w}> result = new ArrayList<>(array.length);
            for (final ${t} element : array) {
                result.add(element);
            }
            return result;
        }
    </#list>

    <#list primitivesAndE as t>
        <#assign w = wrapper(t)>
        @Contract("_, _ -> param2")
        public static <@typeVars t, "C extends Collection<? super ${w}>" /> C toCollection(
            ${t}[] array,
            C destination
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            return toCollection(
                array,
                destination,
                0,
                array.length
            );
        }
    </#list>

    <#list primitivesAndE as t>
        <#assign w = wrapper(t)>
        @Contract("_, _, _, _ -> param2")
        public static <@typeVars t, "C extends Collection<? super ${w}>" /> C toCollection(
            ${t}[] array,
            C destination,
            int startIndex,
            int endIndex
        ) {
            requireNotNull(array);
            requireNotNull(destination);
            final int actualStartIndex = max(startIndex, 0);
            final int actualEndIndex = min(endIndex, array.length);
            for (int i = actualStartIndex; i < actualEndIndex; ++i) {
                destination.add(array[i]);
            }
            return destination;
        }
    </#list>

    <#list primitives as p>
        <#assign w = wrapper(p)>
        @Contract(value = "_ -> new", pure = true)
        public static List<${w}> toList(${p}[] array) {
            if (isEmpty(array)) {
                return emptyList();
            }
            return toArrayList(array);
        }
    </#list>

    @Contract(value = "_ -> new", pure = true)
    public static <E> List<E> toList(E[] array) {
        if (isEmpty(array)) {
            return emptyList();
        }
        return java.util.Arrays.asList(array);
    }

    private Arrays() {}
}
