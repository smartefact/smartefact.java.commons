/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.nio;

import java.nio.ByteBuffer;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link ByteBuffer}s.
 *
 * @author Laurent Pireyn
 */
public final class ByteBuffers {
    /**
     * Writes as much of the given buffer as possible into the given buffer.
     * <p>
     * Unlike {@link ByteBuffer#put(ByteBuffer)}, this method never throws a {@link java.nio.BufferOverflowException}.
     * </p>
     * <p><b>Implementation notes:</b></p>
     * <p>
     * This method does not create a {@linkplain ByteBuffer#duplicate() duplicate} buffer.
     * </p>
     *
     * @param buffer the buffer to write the bytes into, considered ready for writing
     * @param from the buffer to read the bytes from, considered ready for reading
     * @return the number of bytes of {@code from} actually written into {@code buffer}
     */
    public static int putAsMuchAsPossible(
        ByteBuffer buffer,
        ByteBuffer from
    ) {
        requireNotNull(buffer);
        requireNotNull(from);
        final var fromRemaining = from.remaining();
        final var bufferRemaining = buffer.remaining();
        if (fromRemaining <= bufferRemaining) {
            // All remaining of from can be written to buffer
            buffer.put(from);
            assert !from.hasRemaining();
            return fromRemaining;
        }
        // Not all remaining of from can be written to buffer
        // Temporarily change limit of from to avoid creating duplicate
        final var oldLimit = from.limit();
        try {
            from.limit(from.position() + bufferRemaining);
            buffer.put(from);
            assert !buffer.hasRemaining();
        } finally {
            from.limit(oldLimit);
        }
        return bufferRemaining;
    }

    private ByteBuffers() {}
}
