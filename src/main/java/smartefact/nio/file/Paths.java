/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.nio.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;
import smartefact.util.Strings;

/**
 * Utilities for {@link Path}s.
 *
 * @author Laurent Pireyn
 */
public final class Paths {
    private static final FileVisitor<Path> FILE_VISITOR_DELETE_RECURSIVELY = new SimpleFileVisitor<>() {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            if (exc != null) {
                throw exc;
            }
            Files.delete(dir);
            return FileVisitResult.CONTINUE;
        }
    };

    public static void deleteRecursively(Path path) throws IOException {
        requireNotNull(path);
        Files.walkFileTree(path, FILE_VISITOR_DELETE_RECURSIVELY);
    }

    @Contract(pure = true)
    public static String extension(Path path) {
        requireNotNull(path);
        final Path fileName = path.getFileName();
        return fileName != null
            ? Strings.substringAfterLast(fileName.toString(), '.', Strings.EMPTY)
            : Strings.EMPTY;
    }

    @Contract(pure = true)
    public static String nameWithoutExtension(Path path) {
        requireNotNull(path);
        final Path fileName = path.getFileName();
        if (fileName == null) {
            return Strings.EMPTY;
        }
        final String name = fileName.toString();
        return Strings.substringBeforeLast(name, '.', name);
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static BufferedReader toBufferedReader(
        Path path,
        Charset charset
    ) throws IOException {
        requireNotNull(path);
        requireNotNull(charset);
        return Files.newBufferedReader(path, charset);
    }

    // TODO: Is this method pure?
    @Contract("_ -> new")
    public static BufferedReader toBufferedReaderUtf8(Path path) throws IOException {
        return toBufferedReader(
            path,
            StandardCharsets.UTF_8
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _, _ -> new")
    public static BufferedWriter toBufferedWriter(
        Path path,
        Charset charset,
        OpenOption... options
    ) throws IOException {
        requireNotNull(path);
        requireNotNull(charset);
        return Files.newBufferedWriter(path, charset, options);
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static BufferedWriter toBufferedWriterUtf8(
        Path path,
        OpenOption... options
    ) throws IOException {
        return toBufferedWriter(
            path,
            StandardCharsets.UTF_8,
            options
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static InputStream toInputStream(
        Path path,
        OpenOption... options
    ) throws IOException {
        requireNotNull(path);
        requireNotNull(options);
        return Files.newInputStream(path, options);
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static OutputStream toOutputStream(
        Path path,
        OpenOption... options
    ) throws IOException {
        requireNotNull(path);
        requireNotNull(options);
        return Files.newOutputStream(path, options);
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static Reader toReader(
        Path path,
        Charset charset
    ) throws IOException {
        return toBufferedReader(
            path,
            charset
        );
    }

    // TODO: Is this method pure?
    @Contract("_ -> new")
    public static Reader toReaderUtf8(Path path) throws IOException {
        return toReader(
            path,
            StandardCharsets.UTF_8
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _, _ -> new")
    public static Writer toWriter(
        Path path,
        Charset charset,
        OpenOption... options
    ) throws IOException {
        return toBufferedWriter(
            path,
            charset,
            options
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static Writer toWriterUtf8(
        Path path,
        OpenOption... options
    ) throws IOException {
        return toWriter(
            path,
            StandardCharsets.UTF_8,
            options
        );
    }

    private Paths() {}
}
