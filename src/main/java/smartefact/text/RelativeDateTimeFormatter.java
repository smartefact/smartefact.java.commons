/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import smartefact.util.Objects;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Relative date-time formatter.
 *
 * @author Laurent Pireyn
 */
public final class RelativeDateTimeFormatter {
    private enum Scale {
        SECONDS("seconds") {
            @Override
            long getSeconds() {
                return 1L;
            }
        },

        MINUTES("minutes") {
            @Override
            long getSeconds() {
                return SECONDS_IN_MINUTE;
            }
        },

        HOURS("hours") {
            @Override
            long getSeconds() {
                return SECONDS_IN_MINUTE * MINUTES_IN_HOUR;
            }
        },

        DAYS("days") {
            @Override
            long getSeconds() {
                return SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY;
            }
        },

        WEEKS("weeks") {
            @Override
            long getSeconds() {
                return SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_WEEK;
            }
        },

        MONTHS("months") {
            @Override
            long getSeconds() {
                return SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_MONTH;
            }
        },

        YEARS("years") {
            @Override
            long getSeconds() {
                return SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_YEAR;
            }
        };

        private static final long SECONDS_IN_MINUTE = 60L;
        private static final long MINUTES_IN_HOUR = 60L;
        private static final long HOURS_IN_DAY = 24L;
        private static final long DAYS_IN_WEEK = 7L;
        private static final long DAYS_IN_MONTH = 30L;
        private static final long DAYS_IN_YEAR = 365L;

        private static final Scale[] VALUES = values();

        @Contract(pure = true)
        static Scale fromSeconds(long seconds) {
            for (int i = 1; i < VALUES.length; ++i) {
                if (seconds < VALUES[i].getSeconds()) {
                    return VALUES[i - 1];
                }
            }
            return YEARS;
        }

        final String key;

        Scale(String key) {
            this.key = key;
        }

        @Contract(pure = true)
        abstract long getSeconds();
    }

    private static final class MessageFormatKey {
        private final Locale locale;
        private final String key;
        private final int hashCode;

        MessageFormatKey(
            Locale locale,
            String key
        ) {
            this.locale = locale;
            this.key = key;
            hashCode = hashCodeBuilder()
                .property(locale)
                .property(key)
                .build();
        }

        @Override
        @Contract(value = "null -> fail", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            assert object instanceof MessageFormatKey;
            final MessageFormatKey other = (MessageFormatKey) object;
            return locale.equals(other.locale)
                && key.equals(other.key);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return hashCode;
        }
    }

    private static final ConcurrentMap<MessageFormatKey, MessageFormat> messageFormats = new ConcurrentHashMap<>();

    private final Locale locale;
    private final ResourceBundle bundle;
    private final Supplier<Temporal> nowSupplier;

    public RelativeDateTimeFormatter(
        @Nullable Locale locale,
        @Nullable Supplier<Temporal> nowSupplier
    ) {
        this.locale = Objects.orElse(locale, Locale.getDefault());
        try {
            bundle = ResourceBundle.getBundle(RelativeDateTimeFormatter.class.getName(), this.locale);
        } catch (MissingResourceException e) {
            throw new AssertionError(e);
        }
        this.nowSupplier = Objects.orElse(nowSupplier, Instant::now);
    }

    public RelativeDateTimeFormatter(@Nullable Locale locale) {
        this(locale, null);
    }

    public RelativeDateTimeFormatter(@Nullable Supplier<Temporal> nowSupplier) {
        this(null, nowSupplier);
    }

    public RelativeDateTimeFormatter() {
        this(null, null);
    }

    @Contract(pure = true)
    public Locale getLocale() {
        return locale;
    }

    @Contract(pure = true)
    private MessageFormat createMessageFormat(String key) {
        final String pattern;
        try {
            pattern = bundle.getString(key);
        } catch (MissingResourceException e) {
            throw new AssertionError(e);
        }
        return new MessageFormat(pattern, locale);
    }

    @Contract(pure = true)
    private MessageFormat getMessageFormat(String key) {
        return messageFormats.computeIfAbsent(
            new MessageFormatKey(locale, key),
            _key -> createMessageFormat(key)
        );
    }

    @Contract(pure = true)
    public String format(Temporal temporal) {
        requireNotNull(temporal);
        long seconds = nowSupplier.get().until(temporal, ChronoUnit.SECONDS);
        final StringBuilder keyBuilder = new StringBuilder(50);
        if (seconds > 0) {
            keyBuilder.append("future");
        } else {
            keyBuilder.append("past");
            seconds = -seconds;
        }
        keyBuilder.append('-');
        final Scale scale = Scale.fromSeconds(seconds);
        keyBuilder.append(scale.key);
        final String key = keyBuilder.toString();
        final long amount = seconds / scale.getSeconds();
        return getMessageFormat(key).format(new Object[] {amount});
    }
}
