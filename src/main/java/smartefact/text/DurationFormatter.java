/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.text.MessageFormat;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import smartefact.util.Objects;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link java.time.Duration} formatter.
 *
 * @author Laurent Pireyn
 */
public final class DurationFormatter {
    private static final class MessageFormatKey {
        private final Locale locale;
        private final String key;
        private final int hashCode;

        MessageFormatKey(
            Locale locale,
            String key
        ) {
            this.locale = locale;
            this.key = key;
            hashCode = hashCodeBuilder()
                .property(locale)
                .property(key)
                .build();
        }

        @Override
        @Contract(value = "null -> fail", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            assert object instanceof MessageFormatKey;
            final MessageFormatKey other = (MessageFormatKey) object;
            return locale.equals(other.locale)
                && key.equals(other.key);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return hashCode;
        }
    }

    private static final ConcurrentMap<MessageFormatKey, MessageFormat> messageFormats = new ConcurrentHashMap<>();

    private final Locale locale;
    private final ResourceBundle bundle;

    public DurationFormatter(@Nullable Locale locale) {
        this.locale = Objects.orElse(locale, Locale.getDefault());
        try {
            bundle = ResourceBundle.getBundle(DurationFormatter.class.getName(), this.locale);
        } catch (MissingResourceException e) {
            throw new AssertionError(e);
        }
    }

    public DurationFormatter() {
        this(null);
    }

    @Contract(pure = true)
    public Locale getLocale() {
        return locale;
    }

    @Contract(pure = true)
    private MessageFormat createMessageFormat(String key) {
        final String pattern;
        try {
            pattern = bundle.getString(key);
        } catch (MissingResourceException e) {
            throw new AssertionError(e);
        }
        return new MessageFormat(pattern, locale);
    }

    @Contract(pure = true)
    private MessageFormat getMessageFormat(String key) {
        return messageFormats.computeIfAbsent(
            new MessageFormatKey(locale, key),
            _key -> createMessageFormat(key)
        );
    }

    @Contract(pure = true)
    public String format(TemporalAmount duration) {
        requireNotNull(duration);
        long remainingSeconds = duration.get(ChronoUnit.SECONDS);
        final long days = TimeUnit.SECONDS.toDays(remainingSeconds);
        if (days > 0) {
            remainingSeconds %= TimeUnit.DAYS.toSeconds(days);
        }
        final long hours = TimeUnit.SECONDS.toHours(remainingSeconds);
        if (hours > 0) {
            remainingSeconds %= TimeUnit.HOURS.toSeconds(hours);
        }
        final long minutes = TimeUnit.SECONDS.toMinutes(remainingSeconds);
        if (minutes > 0) {
            remainingSeconds %= TimeUnit.MINUTES.toSeconds(minutes);
        }
        final long seconds = remainingSeconds;
        final long millis = TimeUnit.NANOSECONDS.toMillis(duration.get(ChronoUnit.NANOS));
        final StringBuilder builder = new StringBuilder(100);
        appendUnit(builder, "days", days);
        appendUnit(builder, "hours", hours);
        appendUnit(builder, "minutes", minutes);
        appendUnit(builder, "seconds", seconds);
        appendUnit(builder, "millis", millis);
        return builder.toString();
    }

    private void appendUnit(StringBuilder builder, String key, long value) {
        final String string = getMessageFormat(key).format(new Object[] {value});
        if (!string.isEmpty() && builder.length() > 0) {
            builder.append(' ');
        }
        builder.append(string);
    }
}
