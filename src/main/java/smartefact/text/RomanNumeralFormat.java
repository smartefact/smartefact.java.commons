/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.math.BigInteger;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import smartefact.util.Arrays;
import smartefact.util.Integers;
import smartefact.util.Longs;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link Format} for <a href="https://en.wikipedia.org/wiki/Roman_numerals">Roman numerals</a>.
 * <p>
 * This class does not extend {@link java.text.NumberFormat} because it only supports integer numbers.
 * </p>
 *
 * @author Laurent Pireyn
 */
public final class RomanNumeralFormat extends Format {
    /**
     * Minimum value supported by this format.
     */
    public static final int MIN_VALUE = 1;

    /**
     * Maximum value supported by this format.
     */
    public static final int MAX_VALUE = 9999;

    private static final char[] DIGITS_UPPER_CASE = {'M', 'D', 'C', 'L', 'X', 'V', 'I'};
    private static final char[] DIGITS_LOWER_CASE = {'m', 'd', 'c', 'l', 'x', 'v', 'i'};
    private static final int[] DIGIT_VALUES = {1000, 500, 100, 50, 10, 5, 1};

    private static final RomanNumeralFormat INSTANCE_UPPER_CASE = new RomanNumeralFormat(false);
    private static final RomanNumeralFormat INSTANCE_LOWER_CASE = new RomanNumeralFormat(true);

    private static final long serialVersionUID = 1L;

    @Contract(pure = true)
    private static int digitIndex(char digit) {
        final int index = Arrays.indexOf(DIGITS_UPPER_CASE, digit);
        return index != -1 ? index : Arrays.indexOf(DIGITS_LOWER_CASE, digit);
    }

    @Contract(pure = true)
    private static int digitValue(char digit) {
        final int digitIndex = digitIndex(digit);
        return digitIndex != -1 ? DIGIT_VALUES[digitIndex] : -1;
    }

    @Contract(pure = true)
    public static int parseRomanNumeral(String string) throws ParseException {
        final ParsePosition pos = new ParsePosition(0);
        final Integer result = parseRomanNumeral(string, pos);
        if (result == null) {
            throw new ParseException("The string <" + string + "> is not a legal Roman numeral", pos.getErrorIndex());
        }
        return result;
    }

    public static @Nullable Integer parseRomanNumeral(
        String source,
        ParsePosition pos
    ) {
        requireNotNull(source);
        // The superclass mandates NullPointerException
        Objects.requireNonNull(pos);
        final int length = source.length();
        if (length == 0) {
            return null;
        }
        final int[] values = new int[length];
        for (int i = pos.getIndex(); i < length; ++i) {
            final int value = digitValue(source.charAt(i));
            if (value == -1) {
                pos.setErrorIndex(i);
                return null;
            }
            values[i] = value;
        }
        int result = 0;
        for (int i = 0; i < values.length; ++i) {
            final int value = values[i];
            final int nextIndex = i + 1;
            if (nextIndex == length || values[nextIndex] <= value) {
                result += value;
            } else {
                result -= value;
            }
        }
        pos.setIndex(length);
        return result;
    }

    @Contract(pure = true)
    public static RomanNumeralFormat getInstance(boolean lowerCase) {
        return lowerCase ? INSTANCE_LOWER_CASE : INSTANCE_UPPER_CASE;
    }

    @Contract(pure = true)
    public static RomanNumeralFormat getUpperCaseInstance() {
        return getInstance(false);
    }

    @Contract(pure = true)
    public static RomanNumeralFormat getLowerCaseInstance() {
        return getInstance(true);
    }

    private final boolean lowerCase;
    private final char[] digits;

    /**
     * Creates a {@code RomanNumeralFormat} with the given case.
     *
     * @param lowerCase whether the new format is lower case
     */
    public RomanNumeralFormat(boolean lowerCase) {
        this.lowerCase = lowerCase;
        digits = lowerCase ? DIGITS_LOWER_CASE : DIGITS_UPPER_CASE;
    }

    /**
     * Creates an upper case {@code RomanNumeralFormat}.
     *
     * @see #RomanNumeralFormat(boolean)
     */
    public RomanNumeralFormat() {
        this(false);
    }

    /**
     * Returns whether this format is lower case.
     *
     * @return {@code true} if this format is lower case, {@code false} if it is upper case
     */
    @Contract(pure = true)
    public boolean isLowerCase() {
        return lowerCase;
    }

    @Override
    public StringBuffer format(
        Object object,
        StringBuffer toAppendTo,
        FieldPosition pos
    ) {
        requireNotNull(object);
        // The superclass mandates NullPointerException
        Objects.requireNonNull(toAppendTo);
        Objects.requireNonNull(pos);
        if (object instanceof Integer
            || object instanceof Short
            || object instanceof AtomicInteger
        ) {
            return format(((Number) object).intValue(), toAppendTo, pos);
        }
        if (object instanceof Long
            || object instanceof AtomicLong
            || object instanceof BigInteger && ((BigInteger) object).bitLength() < 64
        ) {
            return format(((Number) object).longValue(), toAppendTo, pos);
        }
        throw new IllegalArgumentException("The class " + object.getClass().getName() +
            " is not supported by " + RomanNumeralFormat.class.getSimpleName()
        );
    }

    @Contract("_, _, _ -> param2")
    public StringBuffer format(
        int number,
        StringBuffer toAppendTo,
        FieldPosition pos
    ) {
        require(
            Integers.in(number, MIN_VALUE, MAX_VALUE),
            () -> "The number <" + number + "> is out of the supported range <" + MIN_VALUE + "> to <" + MAX_VALUE + '>'
        );
        // The superclass mandates NullPointerException
        Objects.requireNonNull(toAppendTo);
        Objects.requireNonNull(pos);
        pos.setBeginIndex(toAppendTo.length());
        int v = number;
        int index = 0;
        while (index < digits.length) {
            final int n = v / DIGIT_VALUES[index];
            assert n < 10;
            int u = n;
            if (index > 0) {
                switch (n) {
                    case 4:
                        toAppendTo.append(digits[index]).append(digits[index - 1]);
                        u = 0;
                        break;
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        toAppendTo.append(digits[index - 1]);
                        u = n - 5;
                        break;
                    case 9:
                        toAppendTo.append(digits[index]).append(digits[index - 2]);
                        u = 0;
                        break;
                }
            }
            for (int i = 0; i < u; ++i) {
                toAppendTo.append(digits[index]);
            }
            v %= DIGIT_VALUES[index];
            index += 2;
        }
        pos.setEndIndex(toAppendTo.length());
        return toAppendTo;
    }

    @SuppressWarnings("JdkObsolete")
    public String format(int number) {
        return format(
            number,
            new StringBuffer(),
            new FieldPosition(0)
        )
            .toString();
    }

    public StringBuffer format(
        long number,
        StringBuffer toAppendTo,
        FieldPosition pos
    ) {
        require(
            Longs.in(number, MIN_VALUE, MAX_VALUE),
            () -> "The number <" + number + "> is out of the supported range <" + MIN_VALUE + "> to <" + MAX_VALUE + '>'
        );
        return format(
            (int) number,
            toAppendTo,
            pos
        );
    }

    @SuppressWarnings("JdkObsolete")
    public String format(long number) {
        return format(
            number,
            new StringBuffer(),
            new FieldPosition(0)
        )
            .toString();
    }

    @Override
    public @Nullable Integer parseObject(
        String source,
        ParsePosition pos
    ) {
        return parseRomanNumeral(source, pos);
    }

    @Override
    public RomanNumeralFormat clone() {
        return (RomanNumeralFormat) super.clone();
    }
}
