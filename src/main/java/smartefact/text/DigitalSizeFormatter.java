/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.text;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Digital size formatter.
 *
 * @author Laurent Pireyn
 * @see <a href="https://en.wikipedia.org/wiki/Units_of_information">Units of information</a>
 */
public final class DigitalSizeFormatter {
    /**
     * Prefix.
     */
    public enum Prefix {
        /**
         * Metric prefix.
         *
         * @see <a href="https://en.wikipedia.org/wiki/Metric_prefix">Metric prefix</a>
         */
        METRIC(1000, "metric"),

        /**
         * Binary prefix.
         *
         * @see <a href="https://en.wikipedia.org/wiki/Binary_prefix">Binary prefix</a>
         */
        BINARY(1024, "binary");

        final int unitFactor;
        final String messageKeyPart;

        @Contract(pure = true)
        Prefix(int unitFactor, String messageKeyPart) {
            this.unitFactor = unitFactor;
            this.messageKeyPart = messageKeyPart;
        }
    }

    /**
     * Style.
     */
    public enum Style {
        /**
         * Full.
         */
        FULL("full"),

        /**
         * Symbol.
         */
        SYMBOL("symbol");

        final String messageKeyPart;

        @Contract(pure = true)
        Style(String messageKeyPart) {
            this.messageKeyPart = messageKeyPart;
        }
    }

    /**
     * {@link DigitalSizeFormatter} builder.
     */
    public static final class Builder {
        @Nullable Locale locale;
        String numberPattern = "###,###0.###";
        DigitalSizeFormatter.Prefix prefix = Prefix.METRIC;
        Style style = Style.FULL;
        float unitThreshold = 1.0f;

        @Contract("_ -> this")
        public Builder locale(@Nullable Locale locale) {
            this.locale = locale;
            return this;
        }

        @Contract("-> this")
        public Builder defaultLocale() {
            return locale(null);
        }

        @Contract("_ -> this")
        public Builder numberPattern(String numberPattern) {
            this.numberPattern = requireNotNull(numberPattern);
            return this;
        }

        @Contract("_ -> this")
        public Builder prefix(DigitalSizeFormatter.Prefix prefix) {
            this.prefix = requireNotNull(prefix);
            return this;
        }

        @Contract("-> this")
        public Builder metric() {
            return prefix(Prefix.METRIC);
        }

        @Contract("-> this")
        public Builder binary() {
            return prefix(Prefix.BINARY);
        }

        @Contract("_ -> this")
        public Builder style(Style style) {
            this.style = requireNotNull(style);
            return this;
        }

        @Contract("-> this")
        public Builder full() {
            return style(Style.FULL);
        }

        @Contract("-> this")
        public Builder symbol() {
            return style(Style.SYMBOL);
        }

        @Contract("_ -> this")
        public Builder unitThreshold(float unitThreshold) {
            require(
                unitThreshold > 0.0f && unitThreshold <= 1.0f,
                () -> "The unit threshold <" + unitThreshold + "> is not in the range ]0.0, 1.0]"
            );
            this.unitThreshold = unitThreshold;
            return this;
        }

        @Contract("-> this")
        public Builder wholeUnitThreshold() {
            return unitThreshold(1.0f);
        }

        @Contract("-> new")
        public DigitalSizeFormatter build() {
            return new DigitalSizeFormatter(this);
        }
    }

    private static final class MessageFormatKey {
        private final Locale locale;
        private final String key;
        private final int hashCode;

        MessageFormatKey(
            Locale locale,
            String key
        ) {
            this.locale = locale;
            this.key = key;
            hashCode = hashCodeBuilder()
                .property(locale)
                .property(key)
                .build();
        }

        @Override
        @Contract(value = "null -> fail", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            assert object instanceof MessageFormatKey;
            final MessageFormatKey other = (MessageFormatKey) object;
            return locale.equals(other.locale)
                && key.equals(other.key);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return hashCode;
        }
    }

    private static final int UNIT_COUNT = 9;
    private static final int BIG_DECIMAL_SCALE = 3;
    private static final BigInteger BIG_INTEGER_FACTOR = BigInteger.TEN.pow(BIG_DECIMAL_SCALE);
    private static final ConcurrentMap<MessageFormatKey, MessageFormat> messageFormats = new ConcurrentHashMap<>();

    @Contract("-> new")
    public static Builder builder() {
        return new Builder();
    }

    private final Locale locale;
    private final DecimalFormat numberFormat;
    private final ResourceBundle bundle;
    private final DigitalSizeFormatter.Prefix prefix;
    private final Style style;
    private final float unitThreshold;

    private DigitalSizeFormatter(Builder builder) {
        locale = builder.locale != null ? builder.locale : Locale.getDefault();
        numberFormat = new DecimalFormat(builder.numberPattern, DecimalFormatSymbols.getInstance(locale));
        try {
            bundle = ResourceBundle.getBundle(DigitalSizeFormatter.class.getName(), locale);
        } catch (MissingResourceException e) {
            throw new AssertionError(e);
        }
        prefix = builder.prefix;
        style = builder.style;
        unitThreshold = builder.unitThreshold;
    }

    @Contract(pure = true)
    public Locale getLocale() {
        return locale;
    }

    @Contract(pure = true)
    public DigitalSizeFormatter.Prefix getPrefix() {
        return prefix;
    }

    @Contract(pure = true)
    public Style getStyle() {
        return style;
    }

    @Contract(pure = true)
    public float getUnitThreshold() {
        return unitThreshold;
    }

    @Contract("_ -> new")
    private MessageFormat createMessageFormat(String key) {
        final String pattern;
        try {
            pattern = bundle.getString(key);
        } catch (MissingResourceException e) {
            throw new AssertionError(e);
        }
        return new MessageFormat(pattern, locale);
    }

    @Contract(pure = true)
    private MessageFormat getMessageFormat(String key) {
        return messageFormats.computeIfAbsent(
            new MessageFormatKey(locale, key),
            _key -> createMessageFormat(key)
        );
    }

    @Contract(pure = true)
    private String format(Object number, int unitIndex) {
        return getMessageFormat(prefix.messageKeyPart + '-' + style.messageKeyPart + '-' + unitIndex)
            // TODO: DecimalFormat is not thread-safe
            .format(new Object[] {numberFormat.format(number), number});
    }

    @Contract(pure = true)
    public String format(long bytes) {
        double number = bytes;
        int unitIndex = 0;
        while (unitIndex < UNIT_COUNT - 1 && number >= prefix.unitFactor * unitThreshold) {
            number /= prefix.unitFactor;
            ++unitIndex;
        }
        return format(number, unitIndex);
    }

    @Contract(pure = true)
    public String format(BigInteger bytes) {
        requireNotNull(bytes);
        BigDecimal number = new BigDecimal(bytes.multiply(BIG_INTEGER_FACTOR), BIG_DECIMAL_SCALE);
        int unitIndex = 0;
        while (unitIndex < UNIT_COUNT - 1 && number.compareTo(BigDecimal.valueOf(prefix.unitFactor * unitThreshold)) >= 0) {
            number = number.divide(BigDecimal.valueOf(prefix.unitFactor), RoundingMode.HALF_EVEN);
            ++unitIndex;
        }
        return format(number, unitIndex);
    }
}
