/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link Reader}s.
 *
 * @author Laurent Pireyn
 */
public final class Readers {
    private static final int DEFAULT_BUFFER_SIZE = 5 * 1024;

    public static String readText(Reader reader) throws IOException {
        requireNotNull(reader);
        final var stringWriter = new StringWriter(DEFAULT_BUFFER_SIZE);
        reader.transferTo(stringWriter);
        return stringWriter.toString();
    }

    public static long skipEffectively(Reader reader, long n) throws IOException {
        requireNotNull(reader);
        if (n <= 0L) {
            return 0L;
        }
        long remaining = n;
        while (remaining > 0L) {
            final long count = reader.skip(remaining);
            if (count == 0L) {
                // Assume EOF
                break;
            }
            remaining -= count;
        }
        return n - remaining;
    }

    @Contract(pure = true)
    public static BufferedReader toBufferedReader(Reader reader) {
        return toBufferedReader(
            reader,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(pure = true)
    public static BufferedReader toBufferedReader(
        Reader reader,
        int bufferSize
    ) {
        requireNotNull(reader);
        requireGt0(bufferSize);
        if (reader instanceof BufferedReader) {
            return (BufferedReader) reader;
        }
        return new BufferedReader(reader, bufferSize);
    }

    private Readers() {}
}
