/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.BufferedWriter;
import java.io.Writer;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link Writer}s.
 *
 * @author Laurent Pireyn
 */
public final class Writers {
    private static final int DEFAULT_BUFFER_SIZE = 5 * 1024;

    @Contract(pure = true)
    public static BufferedWriter toBufferedWriter(Writer writer) {
        return toBufferedWriter(
            writer,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(pure = true)
    public static BufferedWriter toBufferedWriter(
        Writer writer,
        int bufferSize
    ) {
        requireNotNull(writer);
        requireGt0(bufferSize);
        if (writer instanceof BufferedWriter) {
            return (BufferedWriter) writer;
        }
        return new BufferedWriter(writer, bufferSize);
    }

    private Writers() {}
}
