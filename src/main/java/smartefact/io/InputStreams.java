/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.jetbrains.annotations.Contract;
import smartefact.util.Longs;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link InputStream}s.
 *
 * @author Laurent Pireyn
 */
public final class InputStreams {
    private static final int DEFAULT_BUFFER_SIZE = 5 * 1024;

    /**
     * Discards <i>n</i> bytes from the given {@link InputStream}
     * by repeatedly reading from it into a buffer.
     *
     * @param input the input stream
     * @param n the number of bytes to discard from {@code input}
     * @return the number of bytes actually discarded from {@code input}
     * @see #discard(InputStream, long, int)
     */
    public static long discard(InputStream input, long n) throws IOException {
        return discard(input, n, DEFAULT_BUFFER_SIZE);
    }

    /**
     * Discards <i>n</i> bytes from the given {@link InputStream}
     * by repeatedly reading from it into a buffer.
     *
     * @param input the input stream
     * @param n the number of bytes to discard from {@code input}
     * @param bufferSize the size of the buffer used
     * @return the number of bytes actually discarded from {@code input}
     */
    public static long discard(
        InputStream input,
        long n,
        int bufferSize
    ) throws IOException {
        requireNotNull(input);
        requireGt0(bufferSize);
        if (n <= 0L) {
            return 0L;
        }
        long remaining = n;
        final var buffer = new byte[(int) Longs.coerceAtMost(bufferSize, n)];
        while (remaining > 0L) {
            final var count = input.read(buffer, 0, (int) Longs.coerceAtMost(remaining, buffer.length));
            if (count == -1) {
                // EOF
                break;
            }
            remaining -= count;
        }
        return n - remaining;
    }

    public static long skipEffectively(InputStream input, long n) throws IOException {
        requireNotNull(input);
        if (n <= 0L) {
            return 0L;
        }
        long remaining = n;
        while (remaining > 0L) {
            final long count = input.skip(remaining);
            if (count == 0L) {
                // Assume EOF
                break;
            }
            remaining -= count;
        }
        return n - remaining;
    }

    @Contract(pure = true)
    public static BufferedInputStream toBufferedInputStream(InputStream input) {
        return toBufferedInputStream(
            input,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(pure = true)
    public static BufferedInputStream toBufferedInputStream(
        InputStream input,
        int bufferSize
    ) {
        requireNotNull(input);
        requireGt0(bufferSize);
        if (input instanceof BufferedInputStream) {
            return (BufferedInputStream) input;
        }
        return new BufferedInputStream(input, bufferSize);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static BufferedReader toBufferedReader(
        InputStream input,
        Charset charset
    ) {
        return toBufferedReader(
            input,
            charset,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public static BufferedReader toBufferedReader(
        InputStream input,
        Charset charset,
        int bufferSize
    ) {
        return Readers.toBufferedReader(toReader(input, charset), bufferSize);
    }

    @Contract(value = "_ -> new", pure = true)
    public static BufferedReader toBufferedReaderUtf8(InputStream input) {
        return toBufferedReaderUtf8(
            input,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static BufferedReader toBufferedReaderUtf8(
        InputStream input,
        int bufferSize
    ) {
        return toBufferedReader(
            input,
            StandardCharsets.UTF_8,
            bufferSize
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static Reader toReader(
        InputStream input,
        Charset charset
    ) {
        requireNotNull(input);
        requireNotNull(charset);
        return new InputStreamReader(input, charset);
    }

    @Contract(value = "_ -> new", pure = true)
    public static Reader toReaderUtf8(InputStream input) {
        return toReader(
            input,
            StandardCharsets.UTF_8
        );
    }

    private InputStreams() {}
}
