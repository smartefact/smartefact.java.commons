/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.IOException;
import java.io.InputStream;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link InputStream} wrapper.
 * <p>
 * This class is a better alternative to {@link java.io.FilterInputStream}.
 * </p>
 *
 * @author Laurent Pireyn
 */
public abstract class InputStreamWrapper extends AbstractInputStream {
    protected final InputStream input;

    protected InputStreamWrapper(InputStream input) {
        this.input = requireNotNull(input);
    }

    @Override
    public int available() throws IOException {
        return input.available();
    }

    @Override
    public int read() throws IOException {
        return input.read();
    }

    @Override
    public int read(byte[] array, int offset, int length) throws IOException {
        return input.read(array, offset, length);
    }

    @Override
    public long skip(long count) throws IOException {
        return input.skip(count);
    }

    @Override
    public boolean markSupported() {
        return input.markSupported();
    }

    @Override
    public void mark(int readLimit) {
        input.mark(readLimit);
    }

    @Override
    public void reset() throws IOException {
        input.reset();
    }

    @Override
    public void close() throws IOException {
        input.close();
    }
}
