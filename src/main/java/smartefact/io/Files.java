/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.OpenOption;
import org.jetbrains.annotations.Contract;
import smartefact.nio.file.Paths;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link File}s.
 * <p>
 * The methods in this class delegate to the corresponding methods in {@link Paths}.
 * </p>
 *
 * @author Laurent Pireyn
 * @see Paths
 */
public final class Files {
    public static void deleteRecursively(File file) throws IOException {
        requireNotNull(file);
        Paths.deleteRecursively(file.toPath());
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static BufferedReader toBufferedReader(
        File file,
        Charset charset
    ) throws IOException {
        requireNotNull(file);
        requireNotNull(charset);
        return Paths.toBufferedReader(file.toPath(), charset);
    }

    // TODO: Is this method pure?
    @Contract("_ -> new")
    public static BufferedReader toBufferedReaderUtf8(File file) throws IOException {
        return toBufferedReader(
            file,
            StandardCharsets.UTF_8
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static BufferedWriter toBufferedWriter(
        File file,
        Charset charset
    ) throws IOException {
        requireNotNull(file);
        requireNotNull(charset);
        return Paths.toBufferedWriter(file.toPath(), charset);
    }

    // TODO: Is this method pure?
    @Contract("_ -> new")
    public static BufferedWriter toBufferedWriterUtf8(File file) throws IOException {
        return toBufferedWriter(
            file,
            StandardCharsets.UTF_8
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static InputStream toInputStream(
        File file,
        OpenOption... options
    ) throws IOException {
        requireNotNull(file);
        requireNotNull(options);
        return Paths.toInputStream(file.toPath(), options);
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static OutputStream toOutputStream(
        File file,
        OpenOption... options
    ) throws IOException {
        requireNotNull(file);
        requireNotNull(options);
        return Paths.toOutputStream(file.toPath(), options);
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static Reader toReader(
        File file,
        Charset charset
    ) throws IOException {
        requireNotNull(file);
        requireNotNull(charset);
        return Paths.toReader(file.toPath(), charset);
    }

    // TODO: Is this method pure?
    @Contract("_ -> new")
    public static Reader toReaderUtf8(File file) throws IOException {
        return toReader(
            file,
            StandardCharsets.UTF_8
        );
    }

    // TODO: Is this method pure?
    @Contract("_, _ -> new")
    public static Writer toWriter(
        File file,
        Charset charset
    ) throws IOException {
        requireNotNull(file);
        requireNotNull(charset);
        return Paths.toWriter(file.toPath(), charset);
    }

    // TODO: Is this method pure?
    @Contract("_ -> new")
    public static Writer toWriterUtf8(File file) throws IOException {
        return toWriter(
            file,
            StandardCharsets.UTF_8
        );
    }

    private Files() {}
}
