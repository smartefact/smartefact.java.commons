/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.IOException;
import java.io.OutputStream;
import org.jetbrains.annotations.Contract;

/**
 * {@link OutputStreamWrapper} that counts the bytes written.
 *
 * @author Laurent Pireyn
 */
public final class CountingOutputStream extends OutputStreamWrapper {
    private long byteCount;

    public CountingOutputStream(OutputStream output) {
        this(output, 0L);
    }

    public CountingOutputStream(OutputStream output, long initialByteCount) {
        super(output);

        byteCount = initialByteCount;
    }

    @Contract(pure = true)
    public long getByteCount() {
        return byteCount;
    }

    @Override
    public void write(int b) throws IOException {
        output.write(b);
        ++byteCount;
    }

    @Override
    public void write(byte[] array, int offset, int length) throws IOException {
        output.write(array, offset, length);
        byteCount += length;
    }
}
