/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.IOException;
import java.io.InputStream;
import org.jetbrains.annotations.Contract;

/**
 * {@link InputStreamWrapper} that counts the bytes read and skipped.
 * <p>
 * This class does not support reset.
 * </p>
 *
 * @author Laurent Pireyn
 */
public final class CountingInputStream extends InputStreamWrapper {
    private long byteCount;

    public CountingInputStream(InputStream input) {
        this(input, 0L);
    }

    public CountingInputStream(InputStream input, long initialByteCount) {
        super(input);

        byteCount = initialByteCount;
    }

    @Contract(pure = true)
    public long getByteCount() {
        return byteCount;
    }

    @Override
    public int read() throws IOException {
        final int b = input.read();
        if (b != -1) {
            ++byteCount;
        }
        return b;
    }

    @Override
    public int read(byte[] array, int offset, int length) throws IOException {
        final int count = input.read(array, offset, length);
        if (count > 0) {
            byteCount += count;
        }
        return count;
    }

    @Override
    public long skip(long count) throws IOException {
        final long actualCount = input.skip(count);
        byteCount += actualCount;
        return actualCount;
    }

    @Override
    @Contract(pure = true)
    public boolean markSupported() {
        return false;
    }

    @Override
    @Contract(pure = true)
    public void mark(int readLimit) {}

    @Override
    @Contract(value = "-> fail", pure = true)
    public void reset() throws IOException {
        throw new IOException("The reset method is not supported");
    }
}
