/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Abstract {@link InputStream}.
 * <p>
 * This class is a better alternative to {@code InputStream} when used as superclass:
 * </p>
 * <ul>
 *     <li>It overrides as final a method that should not be overridden</li>
 * </ul>
 *
 * @author Laurent Pireyn
 */
public abstract class AbstractInputStream extends InputStream {
    @Override
    public final int read(byte[] array) throws IOException {
        return super.read(array);
    }
}
