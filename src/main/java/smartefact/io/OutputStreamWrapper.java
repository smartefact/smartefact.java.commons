/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.IOException;
import java.io.OutputStream;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link OutputStream} wrapper.
 * <p>
 * This class is a better alternative to {@link java.io.FilterOutputStream}.
 * </p>
 *
 * @author Laurent Pireyn
 */
public class OutputStreamWrapper extends AbstractOutputStream {
    protected final OutputStream output;

    public OutputStreamWrapper(OutputStream output) {
        this.output = requireNotNull(output);
    }

    @Override
    public void write(int b) throws IOException {
        output.write(b);
    }

    @Override
    public void write(byte[] array, int offset, int length) throws IOException {
        output.write(array, offset, length);
    }

    @Override
    public void flush() throws IOException {
        output.flush();
    }

    @Override
    public void close() throws IOException {
        output.close();
    }
}
