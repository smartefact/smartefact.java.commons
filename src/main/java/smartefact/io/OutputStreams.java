/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.io;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link OutputStream}s.
 *
 * @author Laurent Pireyn
 */
public final class OutputStreams {
    private static final int DEFAULT_BUFFER_SIZE = 5 * 1024;

    @Contract(pure = true)
    public static BufferedOutputStream toBufferedOutputStream(OutputStream output) {
        return toBufferedOutputStream(
            output,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(pure = true)
    public static BufferedOutputStream toBufferedOutputStream(
        OutputStream output,
        int bufferSize
    ) {
        requireNotNull(output);
        requireGt0(bufferSize);
        if (output instanceof BufferedOutputStream) {
            return (BufferedOutputStream) output;
        }
        return new BufferedOutputStream(output, bufferSize);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static BufferedWriter toBufferedWriter(
        OutputStream output,
        Charset charset
    ) {
        return toBufferedWriter(
            output,
            charset,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public static BufferedWriter toBufferedWriter(
        OutputStream output,
        Charset charset,
        int bufferSize
    ) {
        return Writers.toBufferedWriter(toWriter(output, charset), bufferSize);
    }

    @Contract(value = "_ -> new", pure = true)
    public static BufferedWriter toBufferedWriterUtf8(OutputStream output) {
        return toBufferedWriterUtf8(
            output,
            DEFAULT_BUFFER_SIZE
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static BufferedWriter toBufferedWriterUtf8(
        OutputStream output,
        int bufferSize
    ) {
        return toBufferedWriter(
            output,
            StandardCharsets.UTF_8,
            bufferSize
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static Writer toWriter(
        OutputStream output,
        Charset charset
    ) {
        requireNotNull(output);
        requireNotNull(charset);
        return new OutputStreamWriter(output, charset);
    }

    @Contract(value = "_ -> new", pure = true)
    public static Writer toWriterUtf8(OutputStream output) {
        return toWriter(
            output,
            StandardCharsets.UTF_8
        );
    }

    private OutputStreams() {}
}
