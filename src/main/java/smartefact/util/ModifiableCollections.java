/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;
import static smartefact.util.Preconditions.requireGe0;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for modifiable {@link Collection}s and {@link Iterable}s.
 *
 * @author Laurent Pireyn
 */
public final class ModifiableCollections {
    /**
     * Adds the elements in the given iterable to the given collection,
     * and returns whether it was modified.
     *
     * @param collection the collection
     * @param other the iterable
     * @return whether {@code collection} was modified
     * @param <E> the type of elements in {@code collection} and {@code other}
     */
    public static <E> boolean addAll(
        Collection<? super E> collection,
        Iterable<? extends E> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        boolean result = false;
        for (final E element : other) {
            result |= collection.add(element);
        }
        return result;
    }

    /**
     * Adds the elements in the given collection to the given collection,
     * and returns whether it was modified.
     *
     * @param collection the collection
     * @param other the other collection
     * @return whether {@code collection} was modified
     * @param <E> the type of elements in {@code collection} and {@code other}
     * @see Collection#addAll(Collection)
     */
    public static <E> boolean addAll(
        Collection<? super E> collection,
        Collection<? extends E> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        return collection.addAll(other);
    }

    /**
     * Adds the elements to the given collection,
     * and returns whether it was modified.
     *
     * @param collection the collection
     * @param elements the elements
     * @return whether {@code collection} was modified
     * @param <E> the type of elements in {@code collection} and {@code elements}
     * @see java.util.Collections#addAll(Collection, Object[])
     */
    public static <E> boolean addAll(
        Collection<? super E> collection,
        E... elements
    ) {
        requireNotNull(collection);
        requireNotNull(elements);
        boolean result = false;
        for (final E element : elements) {
            result |= collection.add(element);
        }
        return result;
    }

    /**
     * Removes all elements in the given iterable.
     *
     * @param iterable the iterable
     * @see Iterator#remove()
     */
    public static void clear(Iterable<?> iterable) {
        requireNotNull(iterable);
        for (final Iterator<?> iterator = iterable.iterator(); iterator.hasNext(); ) {
            iterator.next();
            iterator.remove();
        }
    }

    /**
     * Removes all elements in the given collection.
     *
     * @param collection the collection
     * @see Collection#clear()
     */
    public static void clear(Collection<?> collection) {
        requireNotNull(collection);
        collection.clear();
    }

    /**
     * Removes all elements in the given iterable from the given iterable,
     * and returns whether it was modified.
     *
     * @param iterable the iterable to remove elements from
     * @param other the iterable of elements to remove from {@code iterable}
     * @return whether {@code iterable} was modified
     */
    public static boolean removeAll(
        Iterable<?> iterable,
        Iterable<?> other
    ) {
        return removeAll(
            iterable,
            Collections.toSet(other)
        );
    }

    /**
     * Removes all elements in the given collection from the given iterable,
     * and returns whether it was modified.
     *
     * @param iterable the iterable to remove elements from
     * @param other the collection of elements to remove from {@code iterable}
     * @return whether {@code iterable} was modified
     */
    public static boolean removeAll(
        Iterable<?> iterable,
        Collection<?> other
    ) {
        requireNotNull(iterable);
        requireNotNull(other);
        boolean result = false;
        for (final Iterator<?> iterator = iterable.iterator(); iterator.hasNext(); ) {
            if (other.contains(iterator.next())) {
                iterator.remove();
                result = true;
            }
        }
        return result;
    }

    /**
     * Removes all elements in the given iterable from the given collection,
     * and returns whether it was modified.
     *
     * @param collection the collection to remove elements from
     * @param other the iterable of elements to remove from {@code collection}
     * @return whether {@code collection} was modified
     */
    public static boolean removeAll(
        Collection<?> collection,
        Iterable<?> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        boolean result = false;
        for (final Object element : other) {
            result |= collection.remove(element);
        }
        return result;
    }

    /**
     * Removes all elements in the given collection from the given collection,
     * and returns whether it was modified.
     *
     * @param collection the collection to remove elements from
     * @param other the collection of elements to remove from {@code collection}
     * @return whether {@code collection} was modified
     * @see Collection#removeAll(Collection)
     */
    public static boolean removeAll(
        Collection<?> collection,
        Collection<?> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        return collection.removeAll(other);
    }

    /**
     * Removes the first element from the given list,
     * and returns whether it was modified.
     *
     * @param list the list
     * @return whether {@code list} was modified
     */
    public static boolean removeFirst(List<?> list) {
        return removeFirst(
            list,
            1
        );
    }

    /**
     * Removes the first <i>n</i> elements from the given list,
     * and returns whether it was modified.
     *
     * @param list the list
     * @param n the number of first elements to remove
     * @return whether {@code list} was modified
     */
    public static boolean removeFirst(
        List<?> list,
        int n
    ) {
        requireNotNull(list);
        requireGe0(n);
        if (n == 0 || list.isEmpty()) {
            return false;
        }
        final int size = list.size();
        if (n >= size) {
            list.clear();
        } else {
            int count = 0;
            for (final Iterator<?> iterator = list.iterator(); count < n && iterator.hasNext(); ) {
                iterator.next();
                iterator.remove();
                ++count;
            }
        }
        return true;
    }

    /**
     * Removes the first elements from the given list,
     * while they match the given predicate,
     * and returns whether the list was modified.
     *
     * @param list the list
     * @param predicate the predicate
     * @return whether {@code list} was modified
     */
    public static <E> boolean removeFirstWhile(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        requireNotNull(list);
        requireNotNull(predicate);
        boolean result = false;
        if (!list.isEmpty()) {
            for (final Iterator<? extends E> iterator = list.iterator(); iterator.hasNext(); ) {
                if (!predicate.test(iterator.next())) {
                    break;
                }
                iterator.remove();
                result = true;
            }
        }
        return result;
    }

    /**
     * Removes from the given iterable the elements that match the given predicate,
     * and returns whether the iterable was modified.
     *
     * @param iterable the iterable
     * @param predicate the predicate
     * @return whether {@code iterable} was modified
     */
    public static <E> boolean removeIf(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        boolean result = false;
        for (final Iterator<? extends E> iterator = iterable.iterator(); iterator.hasNext(); ) {
            final E element = iterator.next();
            if (predicate.test(element)) {
                iterator.remove();
                result = true;
            }
        }
        return result;
    }

    /**
     * Removes from the given collection the elements that match the given predicate,
     * and returns whether the collection was modified.
     *
     * @param collection the collection
     * @param predicate the predicate
     * @return whether {@code collection} was modified
     * @see Collection#removeIf(Predicate)
     */
    public static <E> boolean removeIf(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        return collection.removeIf(predicate);
    }

    /**
     * Removes the last element from the given list,
     * and returns whether it was modified.
     *
     * @param list the list
     * @return whether {@code list} was modified
     */
    public static boolean removeLast(List<?> list) {
        return removeLast(
            list,
            1
        );
    }

    /**
     * Removes the last <i>n</i> elements from the given list,
     * and returns whether it was modified.
     *
     * @param list the list
     * @param n the number of last elements to remove
     * @return whether {@code list} was modified
     */
    public static boolean removeLast(
        List<?> list,
        int n
    ) {
        requireNotNull(list);
        requireGe0(n);
        if (n == 0 || list.isEmpty()) {
            return false;
        }
        final int size = list.size();
        if (n >= size) {
            list.clear();
        } else {
            int count = 0;
            for (final ListIterator<?> iterator = list.listIterator(size); count < n && iterator.hasPrevious(); ) {
                iterator.previous();
                iterator.remove();
                ++count;
            }
        }
        return true;
    }

    /**
     * Removes the last elements from the given list,
     * while they match the given predicate,
     * and returns whether the list was modified.
     *
     * @param list the list
     * @param predicate the predicate
     * @return whether {@code list} was modified
     */
    public static <E> boolean removeLastWhile(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        requireNotNull(list);
        requireNotNull(predicate);
        boolean result = false;
        if (!list.isEmpty()) {
            for (final ListIterator<? extends E> iterator = list.listIterator(list.size()); iterator.hasPrevious(); ) {
                if (!predicate.test(iterator.previous())) {
                    break;
                }
                iterator.remove();
                result = true;
            }
        }
        return result;
    }

    public static boolean retainAll(
        Iterable<?> iterable,
        Iterable<?> other
    ) {
        return retainAll(
            iterable,
            Collections.toSet(other)
        );
    }

    public static boolean retainAll(
        Iterable<?> iterable,
        Collection<?> other
    ) {
        requireNotNull(iterable);
        requireNotNull(other);
        boolean result = false;
        for (final Iterator<?> iterator = iterable.iterator(); iterator.hasNext(); ) {
            if (!other.contains(iterator.next())) {
                iterator.remove();
                result = true;
            }
        }
        return result;
    }

    public static boolean retainAll(
        Collection<?> collection,
        Iterable<?> other
    ) {
        return retainAll(
            collection,
            Collections.toSet(other)
        );
    }

    public static boolean retainAll(
        Collection<?> collection,
        Collection<?> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        return collection.retainAll(other);
    }

    /**
     * Reverses the given list.
     *
     * @param list the list
     * @see java.util.Collections#reverse(List)
     */
    public static void reverse(List<?> list) {
        requireNotNull(list);
        java.util.Collections.reverse(list);
    }

    /**
     * Shuffles the given list
     * using the global random number generator.
     *
     * @param list the list
     * @see #shuffle(List, Random)
     * @see GlobalRandom
     */
    public static void shuffle(List<?> list) {
        shuffle(
            list,
            GlobalRandom.INSTANCE
        );
    }

    /**
     * Shuffles the given list
     * using the given random number generator.
     *
     * @param list the list
     * @param random the random number generator
     * @see java.util.Collections#shuffle(List, Random)
     */
    public static void shuffle(
        List<?> list,
        Random random
    ) {
        requireNotNull(list);
        requireNotNull(random);
        java.util.Collections.shuffle(list, random);
    }

    public static <E extends Comparable<? super E>> void sort(List<E> list) {
        requireNotNull(list);
        list.sort(null);
    }

    public static <E, R extends Comparable<? super R>> void sortBy(
        List<? extends E> list,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(list);
        requireNotNull(selector);
        sortWith(
            list,
            Comparator.comparing(selector)
        );
    }

    public static <E, R extends Comparable<? super R>> void sortByDescending(
        List<? extends E> list,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(list);
        requireNotNull(selector);
        sortWith(
            list,
            Comparator.comparing(selector).reversed()
        );
    }

    public static <E extends Comparable<? super E>> void sortDescending(List<? extends E> list) {
        sortWith(
            list,
            Comparator.reverseOrder()
        );
    }

    public static <E> void sortWith(
        List<? extends E> list,
        Comparator<? super E> comparator
    ) {
        requireNotNull(list);
        requireNotNull(comparator);
        list.sort(comparator);
    }

    private ModifiableCollections() {}
}
