/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireGe0;

/**
 * Utilities for {@code char}s and {@link Character}s.
 *
 * @author Laurent Pireyn
 */
public final class Characters {
    /**
     * Returns the given {@code char} coerced to a minimum character.
     *
     * @param c the character
     * @param min the minimum character
     * @return {@code c} if it is at least {@code min},
     * {@code min} otherwise
     */
    @Contract(pure = true)
    public static char coerceAtLeast(
        char c,
        char min
    ) {
        return c >= min ? c : min;
    }

    /**
     * Returns the given {@code char} coerced to a maximum character.
     *
     * @param c the character
     * @param max the maximum character
     * @return {@code c} if it is at most {@code max},
     * {@code max} otherwise
     */
    @Contract(pure = true)
    public static char coerceAtMost(
        char c,
        char max
    ) {
        return c <= max ? c : max;
    }

    /**
     * Returns the given {@code char} coerced to a character range.
     *
     * @param c the character
     * @param min the minimum character
     * @param max the maximum character
     * @return {@code c} coerced between {@code min} and {@code max}
     */
    @Contract(pure = true)
    public static char coerceIn(
        char c,
        char min,
        char max
    ) {
        require(
            min <= max,
            () -> "The minimum character <" + min + "> is greater than the maximum character <" + max + '>'
        );
        if (c < min) {
            return min;
        }
        if (c > max) {
            return max;
        }
        return c;
    }

    /**
     * Returns the decimal digit represented by the given {@code char}.
     *
     * @param c the character
     * @return the decimal digit represented by {@code c},
     * or -1 if {@code c} does not represent a decimal digit
     */
    @Contract(pure = true)
    public static int digitToInt(char c) {
        return digitToInt(
            c,
            10
        );
    }

    /**
     * Returns the digit represented by the given {@code char} with the given radix.
     *
     * @param c the character
     * @param radix the radix
     * @return the digit represented by {@code c} with {@code radix},
     * or -1 if {@code c} does not represent a digit
     */
    @Contract(pure = true)
    public static int digitToInt(
        char c,
        int radix
    ) {
        return Character.digit(c, radix);
    }

    /**
     * Returns the decimal digit represented by the given {@code char}.
     *
     * @param c the character
     * @return the decimal digit represented by {@code c},
     * or {@code null} if {@code c} does not represent a decimal digit
     */
    @Contract(pure = true)
    public static @Nullable Integer digitToIntOrNull(char c) {
        return digitToIntOrNull(
            c,
            10
        );
    }

    /**
     * Returns the digit represented by the given {@code char} with the given radix.
     *
     * @param c the character
     * @param radix the radix
     * @return the digit represented by {@code c} with {@code radix},
     * or {@code null} if {@code c} does not represent a digit
     */
    @Contract(pure = true)
    public static @Nullable Integer digitToIntOrNull(
        char c,
        int radix
    ) {
        final int result = digitToInt(c, radix);
        return result != -1 ? result : null;
    }

    /**
     * Returns whether the given {@code char} is equal to the given character.
     *
     * @param c the character
     * @param other the other character
     * @return {@code true} if {@code c} and {@code other} are equal,
     * {@code false} otherwise
     */
    @Contract(pure = true)
    public static boolean equals(
        char c,
        char other
    ) {
        return c == other;
    }

    /**
     * Returns whether the given {@code char} is equal to the given character, possibly ignoring case.
     *
     * @param c the character
     * @param other the other character
     * @param ignoreCase whether the case should be ignored
     * @return {@code true} if {@code c} and {@code other} are equal
     * (ignoring case if {@code ignoreCase} is {@code true}),
     * {@code false} otherwise
     * @see Character#toLowerCase(char)
     */
    @Contract(pure = true)
    public static boolean equals(
        char c,
        char other,
        boolean ignoreCase
    ) {
        return ignoreCase
            ? Character.toLowerCase(c) == Character.toLowerCase(other)
            : c == other;
    }

    @Contract(pure = true)
    public static boolean in(
        char c,
        char first,
        char last
    ) {
        return c >= first && c <= last;
    }

    @Contract(pure = true)
    public static boolean isAlphanumeric(char c) {
        return isLetter(c) || isDecimalDigit(c);
    }

    @Contract(pure = true)
    public static boolean isDecimalDigit(char c) {
        return in(c, '0', '9');
    }

    @Contract(pure = true)
    public static boolean isHexadecimalDigit(char c) {
        return isDecimalDigit(c) || in(c, 'a', 'f') || in(c, 'A', 'F');
    }

    @Contract(pure = true)
    public static boolean isLetter(char c) {
        return isLowerCaseLetter(c) || isUpperCaseLetter(c);
    }

    @Contract(pure = true)
    public static boolean isLowerCaseLetter(char c) {
        return in(c, 'a', 'z');
    }

    @Contract(pure = true)
    public static boolean isOctalDigit(char c) {
        return in(c, '0', '7');
    }

    @Contract(pure = true)
    public static boolean isUpperCaseLetter(char c) {
        return in(c, 'A', 'Z');
    }

    @Contract(pure = true)
    public static String javaEscaped(char c) {
        switch (c) {
            case '\\':
                return "\\\\";
            case '"':
                return "\\\"";
            case '\b':
                return "\\b";
            case '\t':
                return "\\t";
            case '\n':
                return "\\n";
            case '\f':
                return "\\f";
            case '\r':
                return "\\r";
        }
        if (in(c, ' ', '~')) {
            return String.valueOf(c);
        }
        // TODO: Improve implementation
        return "\\u" + Strings.takeLast("0000" + Integers.toString(c, 16), 4);
    }

    @Contract(pure = true)
    public static char orElse(
        @Nullable Character c,
        char defaultValue
    ) {
        return c != null ? c : defaultValue;
    }

    @Contract(pure = true)
    public static String repeat(
        char c,
        int n
    ) {
        requireGe0(n, "n");
        // String#repeat is much faster than appending to a StringBuilder in a loop
        return String.valueOf(c).repeat(n);
    }

    private Characters() {}
}
