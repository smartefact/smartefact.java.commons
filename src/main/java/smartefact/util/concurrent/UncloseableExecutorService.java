/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import java.util.List;
import java.util.concurrent.ExecutorService;
import org.jetbrains.annotations.Contract;

/**
 * {@link ExecutorServiceWrapper} that cannot be closed.
 *
 * @author Laurent Pireyn
 */
final class UncloseableExecutorService extends ExecutorServiceWrapper {
    @Contract(value = "-> new", pure = true)
    private static UnsupportedOperationException createUnsupportedShutdownException() {
        return new UnsupportedOperationException("This executor service cannot be shut down");
    }

    UncloseableExecutorService(ExecutorService service) {
        super(service);
    }

    @Override
    @Contract(value = "-> fail", pure = true)
    public void shutdown() {
        throw createUnsupportedShutdownException();
    }

    @Override
    @Contract(value = "-> fail", pure = true)
    public List<Runnable> shutdownNow() {
        throw createUnsupportedShutdownException();
    }
}
