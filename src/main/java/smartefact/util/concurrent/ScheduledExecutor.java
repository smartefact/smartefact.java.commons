/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.Contract;

/**
 * Scheduled {@link Executor}.
 * <p>
 * This interface defines the same task-related operations as {@link ScheduledExecutorService},
 * but not the methods related to its life cycle (e.g. `shutdown`).
 * </p>
 *
 * @author Laurent Pireyn
 */
public interface ScheduledExecutor extends CompleteExecutor {
    @Contract(value = "_ -> new", pure = true)
    static ScheduledExecutor on(ScheduledExecutorService service) {
        return new ScheduledExecutorServiceAsScheduledExecutor(service);
    }

    <V> ScheduledFuture<V> schedule(
        Callable<V> task,
        long delay,
        TimeUnit unit
    );

    ScheduledFuture<?> schedule(
        Runnable task,
        long delay,
        TimeUnit unit
    );

    ScheduledFuture<?> scheduleAtFixedRate(
        Runnable task,
        long initialDelay,
        long period,
        TimeUnit unit
    );

    ScheduledFuture<?> scheduleWithFixedDelay(
        Runnable task,
        long initialDelay,
        long delay,
        TimeUnit unit
    );
}
