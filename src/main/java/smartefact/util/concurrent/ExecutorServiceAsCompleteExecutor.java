/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link CompleteExecutor} on an {@link ExecutorService}.
 *
 * @author Laurent Pireyn
 */
class ExecutorServiceAsCompleteExecutor<S extends ExecutorService> implements CompleteExecutor {
    final S service;

    ExecutorServiceAsCompleteExecutor(S service) {
        this.service = requireNotNull(service);
    }

    @Override
    public final void execute(Runnable task) {
        requireNotNull(task);
        service.execute(task);
    }

    @Override
    public final <T> Future<T> submit(Callable<T> task) {
        requireNotNull(task);
        return service.submit(task);
    }

    @Override
    public final <T> Future<T> submit(Runnable task, T result) {
        requireNotNull(task);
        return service.submit(task, result);
    }

    @Override
    public final Future<?> submit(Runnable task) {
        requireNotNull(task);
        return service.submit(task);
    }

    @Override
    public final <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        requireNotNull(tasks);
        return service.invokeAll(tasks);
    }

    @Override
    public final <T> List<Future<T>> invokeAll(
        Collection<? extends Callable<T>> tasks,
        long timeout,
        TimeUnit unit
    ) throws InterruptedException {
        requireNotNull(tasks);
        requireNotNull(unit);
        return service.invokeAll(tasks, timeout, unit);
    }

    @Override
    public final <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws ExecutionException, InterruptedException {
        requireNotNull(tasks);
        return service.invokeAny(tasks);
    }

    @Override
    public final <T> T invokeAny(
        Collection<? extends Callable<T>> tasks,
        long timeout,
        TimeUnit unit
    ) throws ExecutionException, TimeoutException, InterruptedException {
        requireNotNull(tasks);
        requireNotNull(unit);
        return service.invokeAny(tasks, timeout, unit);
    }
}
