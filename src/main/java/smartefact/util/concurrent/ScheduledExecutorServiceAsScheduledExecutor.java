/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link ScheduledExecutor} on a {@link ScheduledExecutorService}.
 *
 * @author Laurent Pireyn
 */
class ScheduledExecutorServiceAsScheduledExecutor extends ExecutorServiceAsCompleteExecutor<ScheduledExecutorService> implements ScheduledExecutor {
    ScheduledExecutorServiceAsScheduledExecutor(ScheduledExecutorService service) {
        super(service);
    }

    @Override
    public <V> ScheduledFuture<V> schedule(
        Callable<V> task,
        long delay,
        TimeUnit unit
    ) {
        requireNotNull(task);
        requireNotNull(unit);
        return service.schedule(task, delay, unit);
    }

    @Override
    public ScheduledFuture<?> schedule(
        Runnable task,
        long delay,
        TimeUnit unit
    ) {
        requireNotNull(task);
        requireNotNull(unit);
        return service.schedule(task, delay, unit);
    }

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(
        Runnable task,
        long initialDelay,
        long period,
        TimeUnit unit
    ) {
        requireNotNull(task);
        requireNotNull(unit);
        return service.scheduleAtFixedRate(task, initialDelay, period, unit);
    }

    @Override
    public ScheduledFuture<?> scheduleWithFixedDelay(
        Runnable task,
        long initialDelay,
        long delay,
        TimeUnit unit
    ) {
        requireNotNull(task);
        requireNotNull(unit);
        return service.scheduleWithFixedDelay(task, initialDelay, delay, unit);
    }
}
