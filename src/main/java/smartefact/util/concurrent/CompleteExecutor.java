/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.jetbrains.annotations.Contract;

/**
 * Complete {@link Executor}.
 * <p>
 * This interface defines the same task-related operations as {@link ExecutorService},
 * but not the methods related to its life cycle (e.g. `shutdown`).
 * </p>
 *
 * @author Laurent Pireyn
 */
public interface CompleteExecutor extends Executor {
    @Contract(value = "_ -> new", pure = true)
    static CompleteExecutor on(ExecutorService service) {
        return new ExecutorServiceAsCompleteExecutor<>(service);
    }

    <T> Future<T> submit(Callable<T> task);

    <T> Future<T> submit(Runnable task, T result);

    Future<?> submit(Runnable task);

    <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException;

    <T> List<Future<T>> invokeAll(
        Collection<? extends Callable<T>> tasks,
        long timeout,
        TimeUnit unit
    ) throws InterruptedException;

    <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws ExecutionException, InterruptedException;

    <T> T invokeAny(
        Collection<? extends Callable<T>> tasks,
        long timeout,
        TimeUnit unit
    ) throws ExecutionException, TimeoutException, InterruptedException;
}
