/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link ExecutorService}s and {@link ScheduledExecutorService}s.
 *
 * @author Laurent Pireyn
 */
public final class ExecutorServices {
    @Contract(pure = true)
    public static ExecutorService uncloseableExecutorService(ExecutorService service) {
        requireNotNull(service);
        return service instanceof UncloseableExecutorService
            ? service
            : new UncloseableExecutorService(service);
    }

    @Contract(pure = true)
    public static ScheduledExecutorService uncloseableScheduledExecutorService(ScheduledExecutorService service) {
        requireNotNull(service);
        return service instanceof UncloseableScheduledExecutorService
            ? service
            : new UncloseableScheduledExecutorService(service);
    }

    private ExecutorServices() {}
}
