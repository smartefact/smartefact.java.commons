/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.concurrent;

import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.check;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link java.util.concurrent.ExecutorService} that executes submitted tasks in the same thread as the caller.
 *
 * @author Laurent Pireyn
 */
public final class SameThreadExecutorService extends AbstractExecutorService {
    private volatile boolean terminated;

    @Override
    @Contract(pure = true)
    public boolean isShutdown() {
        return terminated;
    }

    @Override
    @Contract(pure = true)
    public boolean isTerminated() {
        return terminated;
    }

    @Override
    public void execute(Runnable command) {
        // Executor mandates NullPointerException
        Objects.requireNonNull(command);
        check(!terminated, "This executor service has been shut down");
        command.run();
    }

    @Override
    public void shutdown() {
        terminated = true;
    }

    @Override
    public List<Runnable> shutdownNow() {
        shutdown();
        return emptyList();
    }

    @Override
    @Contract(pure = true)
    public boolean awaitTermination(long timeout, TimeUnit unit) {
        requireNotNull(unit);
        check(terminated, "This executor service has not been shut down");
        return true;
    }
}
