/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.ProtectionDomain;
import java.util.Enumeration;
import org.jetbrains.annotations.Nullable;

/**
 * Abstract {@link ClassLoader}.
 * <p>
 * This class is a better alternative to {@code ClassLoader} when used as superclass:
 * </p>
 * <ul>
 *     <li>It overrides as final some methods that should not be overridden</li>
 *     <li>It overrides as abstract some methods that should be overridden together</li>
 * </ul>
 *
 * @author Laurent Pireyn
 */
public abstract class AbstractClassLoader extends ClassLoader {
    protected AbstractClassLoader() {}

    protected AbstractClassLoader(@Nullable ClassLoader parent) {
        super(parent);
    }

    protected AbstractClassLoader(@Nullable String name, @Nullable ClassLoader parent) {
        super(name, parent);
    }

    @Override
    public final @Nullable String getName() {
        return super.getName();
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method is final, override {@link #loadClass(String, boolean)} instead.
     * </p>
     */
    @Override
    public final Class<?> loadClass(String name) throws ClassNotFoundException {
        return super.loadClass(name);
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method is abstract to ensure a consistent implementation in subclasses.
     * </p>
     */
    @Override
    protected abstract Class<?> findClass(String name) throws ClassNotFoundException;

    /**
     * {@inheritDoc}
     * <p>
     * This method is abstract to ensure a consistent implementation in subclasses.
     * </p>
     */
    @Override
    protected abstract @Nullable Class<?> findClass(@Nullable String moduleName, String name);

    /**
     * Shortcut method.
     *
     * @see #defineClass(String, byte[], int, int)
     */
    protected final Class<?> defineClass(
        @Nullable String name,
        byte[] array
    ) throws ClassFormatError {
        return defineClass(name, array, null);
    }

    /**
     * Shortcut method.
     *
     * @see #defineClass(String, byte[], int, int, ProtectionDomain)
     */
    protected final Class<?> defineClass(
        @Nullable String name,
        byte[] array,
        @Nullable ProtectionDomain protectionDomain
    ) throws ClassFormatError {
        return defineClass(name, array, 0, array.length, protectionDomain);
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method is final, override {@link #getResource(String)} instead.
     * </p>
     */
    @Override
    public final @Nullable InputStream getResourceAsStream(String name) {
        return super.getResourceAsStream(name);
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method is abstract to ensure a consistent implementation in subclasses.
     * </p>
     */
    @Override
    protected abstract @Nullable URL findResource(String name);

    /**
     * {@inheritDoc}
     * <p>
     * This method is abstract to ensure a consistent implementation in subclasses.
     * </p>
     */
    @Override
    protected abstract @Nullable URL findResource(@Nullable String moduleName, String name) throws IOException;

    /**
     * {@inheritDoc}
     * <p>
     * This method is abstract to ensure a consistent implementation in subclasses.
     * </p>
     */
    @Override
    protected abstract Enumeration<URL> findResources(String name) throws IOException;
}
