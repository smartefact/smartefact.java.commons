/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Splitting {@link Iterator}.
 * <p>
 * Not to be confused with {@link java.util.Spliterator}.
 * </p>
 *
 * @author Laurent Pireyn
 */
abstract class SplittingIterator implements Iterator<String> {
    final CharSequence string;
    private final int stringLength;
    private final int limit;
    private int startIndex;
    private int count;

    SplittingIterator(CharSequence string, int limit) {
        this.string = string;
        stringLength = string.length();
        this.limit = limit;
    }

    @Override
    public final boolean hasNext() {
        return startIndex <= stringLength;
    }

    @Override
    public final String next() {
        if (startIndex == stringLength + 1) {
            // End of string reached
            throw new NoSuchElementException();
        }
        // End of string not reached yet
        // Determine next element
        final CharSequence result;
        if (limit >= 0 && count == limit) {
            // Limit reached
            // Return remaining string as last element
            result = consumeRemainingString();
        } else {
            // No limit, or limit not reached yet
            // Find next delimiter
            final int delimiterFirstIndex = getDelimiterFirstIndex(startIndex);
            if (delimiterFirstIndex == -1) {
                // No delimiter in remaining string
                // Return remaining string as last element
                result = consumeRemainingString();
            } else {
                // Delimiter found
                // Return substring as next element
                result = string.subSequence(startIndex, delimiterFirstIndex);
                startIndex = getDelimiterLastIndex();
            }
            ++count;
        }
        ++startIndex;
        return result.toString();
    }

    private CharSequence consumeRemainingString() {
        final CharSequence result = string.subSequence(startIndex, stringLength);
        startIndex = stringLength;
        return result;
    }

    abstract int getDelimiterFirstIndex(int startIndex);

    abstract int getDelimiterLastIndex();
}
