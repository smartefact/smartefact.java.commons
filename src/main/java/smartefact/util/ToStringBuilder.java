/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Builder that creates string representations of objects meant for their {@link Object#toString()} method.
 * <p>
 * The built strings follow the format of the {@code toString} method of the Kotlin data classes.
 * </p>
 *
 * @author Laurent Pireyn
 */
public final class ToStringBuilder {
    @Contract("_ -> new")
    public static ToStringBuilder toStringBuilder(Class<?> cls) {
        requireNotNull(cls);
        return new ToStringBuilder(cls);
    }

    @Contract("_ -> new")
    public static ToStringBuilder toStringBuilder(Object object) {
        requireNotNull(object);
        return toStringBuilder(object.getClass());
    }

    private final StringBuilder stringBuilder = new StringBuilder();
    private boolean hasProperties;
    private boolean closed;

    private ToStringBuilder(Class<?> type) {
        stringBuilder.append(type.getSimpleName());
    }

    private void ensureOpen() {
        if (closed) {
            if (hasProperties) {
                stringBuilder.setLength(stringBuilder.length() - 1);
            }
            closed = false;
        }
    }

    private void ensureClosed() {
        if (!closed) {
            if (hasProperties) {
                stringBuilder.append(')');
            }
            closed = true;
        }
    }

    private void beforeProperty() {
        ensureOpen();
        if (!hasProperties) {
            stringBuilder.append('(');
        } else {
            stringBuilder.append(", ");
        }
    }

    private void afterProperty() {
        hasProperties = true;
    }

    @Contract("_ -> this")
    public ToStringBuilder inherited(String inheritedString) {
        requireNotNull(inheritedString);
        final int length = inheritedString.length();
        if (length > 0
            && inheritedString.charAt(length - 1) == ')'
        ) {
            final int index = inheritedString.indexOf('(');
            if (index != -1) {
                // Assume inheritedString has the same format and has properties
                beforeProperty();
                stringBuilder.append(inheritedString, index + 1, length - 1);
                afterProperty();
            }
        }
        return this;
    }

    @Contract("_, _ -> this")
    private ToStringBuilder append(String name, @Nullable String value) {
        requireNotNull(name);
        beforeProperty();
        stringBuilder.append(name).append('=').append(value);
        afterProperty();
        return this;
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, boolean value) {
        return append(name, Boolean.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, byte value) {
        return append(name, Byte.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, char value) {
        return append(name, Character.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, double value) {
        return append(name, Double.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, float value) {
        return append(name, Float.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, int value) {
        return append(name, Integer.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, long value) {
        return append(name, Long.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, short value) {
        return append(name, Short.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, @Nullable Object value) {
        return append(name, Objects.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, boolean[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, byte[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, char[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, double[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, float[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, int[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, long[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, short[] value) {
        return append(name, Arrays.toString(value));
    }

    @Contract("_, _ -> this")
    public ToStringBuilder property(String name, @Nullable Object[] value) {
        return append(name, Arrays.deepToString(value));
    }

    public String build() {
        ensureClosed();
        return stringBuilder.toString();
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("stringBuilder", stringBuilder)
            .property("hasProperties", hasProperties)
            .property("closed", closed)
            .build();
    }
}
