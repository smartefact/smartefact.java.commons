/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link SplittingIterator} based on a {@link Pattern}.
 *
 * @author Laurent Pireyn
 */
final class PatternSplittingIterator extends SplittingIterator {
    private final Matcher matcher;

    PatternSplittingIterator(
        CharSequence string,
        int limit,
        Pattern pattern
    ) {
        super(string, limit);

        matcher = pattern.matcher(string);
    }

    @Override
    int getDelimiterFirstIndex(int startIndex) {
        if (!matcher.find(startIndex)) {
            return -1;
        }
        try {
            return matcher.start();
        } catch (IllegalStateException e) {
            throw new AssertionError(e);
        }
    }

    @Override
    int getDelimiterLastIndex() {
        try {
            return matcher.end() - 1;
        } catch (IllegalStateException e) {
            throw new AssertionError(e);
        }
    }
}
