/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Arrays;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.ToStringBuilder.toStringBuilder;

/**
 * Builder that creates hash codes of objects meant for their {@link Object#hashCode()} method.
 * <p>
 * The algorithm implemented by this class is the same as the one used by {@link Arrays#hashCode(Object[])}
 * (and {@link java.util.Objects#hash(Object...)}).
 * However, it should be more efficient because it allocates fewer objects and makes fewer boxing operations.
 * </p>
 *
 * @author Laurent Pireyn
 */
public final class HashCodeBuilder {
    @Contract(value = "-> new", pure = true)
    public static HashCodeBuilder hashCodeBuilder() {
        return new HashCodeBuilder();
    }

    private int hashCode = 1;

    @Contract(pure = true)
    private HashCodeBuilder() {}

    @Contract("_ -> this")
    private HashCodeBuilder append(int hashCode) {
        this.hashCode = this.hashCode * 31 + hashCode;
        return this;
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(boolean value) {
        return append(Boolean.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(byte value) {
        return append(Byte.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(char value) {
        return append(Character.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(double value) {
        return append(Double.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(float value) {
        return append(Float.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(int value) {
        return append(Integer.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(long value) {
        return append(Long.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(short value) {
        return append(Short.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(@Nullable Object value) {
        return append(Objects.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(boolean @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(byte @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(char @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(double @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(float @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(int @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(long @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(short @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract("_ -> this")
    public HashCodeBuilder property(@Nullable Object @Nullable [] value) {
        return append(Arrays.hashCode(value));
    }

    @Contract(pure = true)
    public int build() {
        return hashCode;
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("hashCode", hashCode)
            .build();
    }
}
