/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.function.Predicate;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link Object}s and {@link Comparable}s.
 *
 * @author Laurent Pireyn
 */
public final class Objects {
    public static <T extends Comparable<T>> T coerceAtLeast(
        T value,
        T minValue
    ) {
        requireNotNull(value);
        requireNotNull(minValue);
        return value.compareTo(minValue) < 0 ? minValue : value;
    }

    public static <T extends Comparable<T>> T coerceAtMost(
        T value,
        T maxValue
    ) {
        requireNotNull(value);
        requireNotNull(maxValue);
        return value.compareTo(maxValue) > 0 ? maxValue : value;
    }

    public static <T extends Comparable<T>> T coerceIn(
        T value,
        T minValue,
        T maxValue
    ) {
        requireNotNull(value);
        requireNotNull(minValue);
        requireNotNull(maxValue);
        require(
            minValue.compareTo(maxValue) <= 0,
            () -> "The minimum value <" + minValue + "> is greater than the maximum value <" + maxValue + '>'
        );
        if (value.compareTo(minValue) < 0) {
            return minValue;
        }
        if (value.compareTo(maxValue) > 0) {
            return maxValue;
        }
        return value;
    }

    /**
     * Returns whether the given object is equal to the other given object,
     * or they are both {@code null}.
     * <p>
     * This method behaves like {@link java.util.Objects#equals(Object, Object)}.
     * </p>
     *
     * @param object the object
     * @param other the other object
     * @return {@code true} if {@code object} is equal to {@code other},
     * {@code false} otherwise
     * @see Object#equals(Object)
     */
    @Contract(value = "null, null -> true; !null, null -> false; null, !null -> false", pure = true)
    public static boolean equals(
        @Nullable Object object,
        @Nullable Object other
    ) {
        return object == other
            || object != null && object.equals(other);
    }

    /**
     * Returns the hash code of the given nullable object.
     * <p>
     * This method behaves like {@link java.util.Objects#hashCode(Object)}.
     * </p>
     *
     * @param object the object
     * @return the hash code of {@code object},
     * or 0 if it is {@code null}
     * @see Object#hashCode()
     */
    @Contract(pure = true)
    public static int hashCode(@Nullable Object object) {
        return object != null ? object.hashCode() : 0;
    }

    /**
     * Returns the given object or a default value if it is {@code null}.
     *
     * @param object the object
     * @param defaultValue the default value
     * @param <R> the return type
     * @param <T> the type of {@code object}
     * @return {@code object} if it is not {@code null},
     * {@code defaultValue} otherwise
     */
    @Contract(value = "!null, _ -> param1; null, _ -> param2", pure = true)
    public static <R, T extends R> R orElse(
        @Nullable T object,
        R defaultValue
    ) {
        return object != null ? object : defaultValue;
    }

    /**
     * Returns the given object if it matches a predicate,
     * or {@code null} if it does not.
     *
     * @param object the object
     * @param predicate the predicate
     * @param <T> the type of {@code object}
     * @return {@code object} if it matches {@code predicate},
     * {@code null} otherwise
     */
    @Contract(pure = true)
    public static <T> @Nullable T takeIf(
        @Nullable T object,
        Predicate<? super T> predicate
    ) {
        requireNotNull(predicate);
        return predicate.test(object) ? object : null;
    }

    /**
     * Returns the given object if it does not match a predicate,
     * or {@code null} if it does.
     *
     * @param object the object
     * @param predicate the predicate
     * @param <T> the type of {@code object}
     * @return {@code object} if it does not match {@code predicate},
     * {@code null} otherwise
     */
    @Contract(pure = true)
    public static <T> @Nullable T takeUnless(
        @Nullable T object,
        Predicate<? super T> predicate
    ) {
        requireNotNull(predicate);
        return !predicate.test(object) ? object : null;
    }

    /**
     * Returns the string representation of the given nullable object.
     * <p>
     * Unlike the {@link java.util.Objects#toString(Object)} method,
     * this method returns {@code null} if the given object is {@code null},
     * rather than the string {@code "null"}.
     * </p>
     *
     * @param object the object
     * @return the string representation of {@code object},
     * or {@code null} if it is {@code null}
     * @see Object#toString()
     */
    @Contract(value = "null -> null", pure = true)
    public static @Nullable String toString(@Nullable Object object) {
        return object != null ? object.toString() : null;
    }

    private Objects() {}
}
