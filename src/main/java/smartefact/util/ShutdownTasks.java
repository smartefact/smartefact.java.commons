/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Shutdown tasks.
 * <p>
 * This class makes it possible to register tasks to run on JVM shutdown
 * without having to manipulate {@link Thread}s.
 * </p>
 *
 * @author Laurent Pireyn
 */
public final class ShutdownTasks {
    /**
     * Registration of a shutdown task.
     */
    public static final class Registration {
        private final Thread thread;

        Registration(Thread thread) {
            this.thread = thread;
        }

        /**
         * Deregisters the shutdown task.
         *
         * @return {@code true} if the shutdown task was successfully deregistered,
         * {@code false} otherwise
         */
        public boolean deregister() {
            return Runtime.getRuntime().removeShutdownHook(thread);
        }
    }

    private static final class ShutdownTaskThread extends Thread {
        ShutdownTaskThread(Runnable task) {
            super(task, "Shutdown task");
            setUncaughtExceptionHandler(ShutdownTasks::uncaughtException);
        }
    }

    /**
     * Registers a shutdown task.
     *
     * @param task the task to run on JVM shutdown
     * @return the registration for {@code task} (never {@code null})
     * @throws IllegalStateException the JVM is already shutting down
     */
    @Contract("_ -> new")
    public static Registration registerShutdownTask(Runnable task) {
        requireNotNull(task);
        final var thread = new ShutdownTaskThread(task);
        try {
            Runtime.getRuntime().addShutdownHook(thread);
        } catch (IllegalStateException e) {
            throw new IllegalStateException("The JVM is already shutting down", e);
        } catch (IllegalArgumentException e) {
            // It's impossible to add the same thread twice
            throw new AssertionError(e);
        }
        return new Registration(thread);
    }

    private static void uncaughtException(Thread thread, Throwable e) {
        // Do nothing
    }

    private ShutdownTasks() {}
}
