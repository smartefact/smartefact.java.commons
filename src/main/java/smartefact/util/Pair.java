/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;

/**
 * Pair of values.
 *
 * @author Laurent Pireyn
 * @param <A> the type of the first value
 * @param <B> the type of the second value
 */
public final class Pair<A, B> {
    private final A first;
    private final B second;

    @Contract(pure = true)
    public Pair(A first, B second) {
        this.first = first;
        this.second = second;
    }

    @Contract(pure = true)
    public A first() {
        return first;
    }

    @Contract(pure = true)
    public B second() {
        return second;
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Pair)) {
            return false;
        }
        final Pair<?, ?> other = (Pair<?, ?>) object;
        return Objects.equals(first, other.first)
            && Objects.equals(second, other.second);
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCodeBuilder()
            .property(first)
            .property(second)
            .build();
    }

    @Override
    @Contract(value = "-> new", pure = true)
    public String toString() {
        return "(" + first + ", " + second + ')';
    }
}
