/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.IOException;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link RuntimeException} wrapping an {@link IOException}.
 *
 * @author Laurent Pireyn
 */
public class RuntimeIoException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public RuntimeIoException(IOException exception) {
        super(requireNotNull(exception).getMessage(), exception);
    }

    @Override
    public final synchronized IOException getCause() {
        final var cause = (IOException) super.getCause();
        assert cause != null;
        return cause;
    }
}
