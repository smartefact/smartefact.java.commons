/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Alphanumeric string.
 * <p>
 * An alphanumeric string is split into a sequence of <em>alphabetic</em> and <em>numeric</em> elements
 * used to order alphanumeric strings.
 * </p>
 * <p>
 * <b>Example:</b>
 * </p>
 * <p>
 * Ordered strings:
 * </p>
 * <ul>
 *     <li>file1.txt</li>
 *     <li>file10.txt</li>
 *     <li>file2.txt</li>
 * </ul>
 * <p>
 * Ordered alphanumeric strings:
 * </p>
 * <ul>
 *     <li>file1.txt</li>
 *     <li>file2.txt</li>
 *     <li>file10.txt</li>
 * </ul>
 *
 * @author Laurent Pireyn
 * @see AlphanumericStringComparator
 */
public final class AlphanumericString implements Comparable<AlphanumericString>, Serializable {
    private abstract static class Element implements Comparable<Element>, Serializable {
        private static final long serialVersionUID = 1L;

        final String stringValue;

        Element(String stringValue) {
            this.stringValue = stringValue;
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public final boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Element)) {
                return false;
            }
            final Element other = (Element) object;
            return stringValue.equals(other.stringValue);
        }

        @Override
        @Contract(pure = true)
        public final int hashCode() {
            return stringValue.hashCode();
        }

        @Override
        @Contract(pure = true)
        public int compareTo(Element other) {
            return stringValue.compareTo(other.stringValue);
        }
    }

    private static final class AlphabeticElement extends Element {
        private static final long serialVersionUID = 1L;

        AlphabeticElement(String stringValue) {
            super(stringValue);
        }
    }

    private static final class NumericElement extends Element {
        private static final long serialVersionUID = 1L;

        private final long longValue;

        NumericElement(String stringValue) {
            super(stringValue);

            longValue = Strings.toLong(stringValue);
        }

        @Override
        @Contract(pure = true)
        public int compareTo(Element other) {
            return other instanceof NumericElement
                ? Long.compare(longValue, ((NumericElement) other).longValue)
                : super.compareTo(other);
        }
    }

    private enum Category {
        ALPHABETIC {
            @Override
            @Contract(pure = true)
            Element createElement(String string) {
                return new AlphabeticElement(string);
            }
        },

        NUMERIC {
            @Override
            @Contract(pure = true)
            Element createElement(String string) {
                return string.length() <= MAX_NUMERIC_LENGTH
                    ? new NumericElement(string)
                    : new AlphabeticElement(string);
            }
        };

        private static final int MAX_NUMERIC_LENGTH = Long.toString(Long.MAX_VALUE).length() - 1;

        @Contract(pure = true)
        static Category forChar(char c) {
            return Characters.isDecimalDigit(c) ? NUMERIC : ALPHABETIC;
        }

        abstract Element createElement(String string);
    }

    private static final long serialVersionUID = 1L;

    @Contract(pure = true)
    private static List<Element> parseElements(String string) {
        requireNotNull(string);
        final List<Element> elements = new ArrayList<>();
        final int length = string.length();
        int start = 0;
        int end = start;
        Category prevCategory = null;
        while (end < length) {
            final Category category = Category.forChar(string.charAt(end));
            if (category != prevCategory) {
                if (prevCategory != null) {
                    elements.add(
                        prevCategory.createElement(string.substring(start, end))
                    );
                }
                start = end;
                prevCategory = category;
            }
            ++end;
        }
        if (prevCategory != null) {
            elements.add(
                prevCategory.createElement(string.substring(start))
            );
        }
        return elements;
    }

    private final String value;
    private final List<Element> elements;

    public AlphanumericString(String value) {
        this.value = requireNotNull(value);
        elements = parseElements(value);
    }

    @Override
    @Contract(pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AlphanumericString)) {
            return false;
        }
        final AlphanumericString other = (AlphanumericString) object;
        return value.equals(other.value);
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    @Contract(pure = true)
    public int compareTo(AlphanumericString other) {
        // Comparable mandates NullPointerException
        Objects.requireNonNull(other);
        final int elementCount = elements.size();
        final int otherElementCount = other.elements.size();
        int index = 0;
        while (true) {
            if (index >= elementCount) {
                // No more elements in this
                if (index >= otherElementCount) {
                    // No more elements in other; this equals other
                    return 0;
                }
                // This comes before other
                return -1;
            }
            if (index >= otherElementCount) {
                // No more elements in other; this comes after other
                return 1;
            }
            final int result = elements.get(index).compareTo(other.elements.get(index));
            if (result != 0) {
                // Element in this is different from element in other; return that result
                return result;
            }
            // Element in this equals element in other; continue with next element
            ++index;
        }
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return value;
    }
}
