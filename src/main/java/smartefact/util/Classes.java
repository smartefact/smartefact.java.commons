/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link Class}es.
 *
 * @author Laurent Pireyn
 */
public final class Classes {
    @Contract(pure = true)
    public static Iterable<Class<?>> classAndSuperclasses(Class<?> cls) {
        requireNotNull(cls);
        return superclassesIterable(cls);
    }

    @Contract(pure = true)
    public static <T> @Nullable Constructor<T> getConstructorOrNull(Class<T> cls, Class<?>... parameterTypes) {
        requireNotNull(cls);
        requireNotNull(parameterTypes);
        try {
            return cls.getConstructor(parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static <T> @Nullable Constructor<T> getDeclaredConstructorOrNull(Class<T> cls, Class<?>... parameterTypes) {
        requireNotNull(cls);
        requireNotNull(parameterTypes);
        try {
            return cls.getDeclaredConstructor(parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Field getDeclaredFieldOrNull(Class<?> cls, String name) {
        requireNotNull(cls);
        requireNotNull(name);
        try {
            return cls.getDeclaredField(name);
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Method getDeclaredMethodOrNull(Class<?> cls, String name, Class<?>... parameterTypes) {
        requireNotNull(cls);
        requireNotNull(name);
        requireNotNull(parameterTypes);
        try {
            return cls.getDeclaredMethod(name, parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Field getFieldOrNull(Class<?> cls, String name) {
        requireNotNull(cls);
        requireNotNull(name);
        try {
            return cls.getField(name);
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Method getMethodOrNull(Class<?> cls, String name, Class<?>... parameterTypes) {
        requireNotNull(cls);
        requireNotNull(name);
        requireNotNull(parameterTypes);
        try {
            return cls.getMethod(name, parameterTypes);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static Iterable<Class<?>> superclasses(Class<?> cls) {
        requireNotNull(cls);
        return superclassesIterable(cls.getSuperclass());
    }

    @Contract(pure = true)
    private static Iterable<Class<?>> superclassesIterable(@Nullable Class<?> initialClass) {
        return () -> new Iterator<>() {
            private @Nullable Class<?> next = initialClass;

            @Override
            public boolean hasNext() {
                return next != null;
            }

            @Override
            public Class<?> next() {
                if (next == null) {
                    throw new NoSuchElementException();
                }
                final Class<?> result = next;
                next = next.getSuperclass();
                return result;
            }
        };
    }

    private Classes() {}
}
