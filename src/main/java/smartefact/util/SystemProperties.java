/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotEmpty;

/**
 * Utilities for system properties.
 *
 * @author Laurent Pireyn
 * @see System#getProperties() Standard system properties
 */
public final class SystemProperties {
    public static final String FILE_SEPARATOR = "file.separator";
    public static final String JAVA_CLASS_PATH = "java.class.path";
    public static final String JAVA_CLASS_VERSION = "java.class.version";
    public static final String JAVA_COMPILER = "java.compiler";
    public static final String JAVA_HOME = "java.home";
    public static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
    public static final String JAVA_LIBRARY_PATH = "java.library.path";
    public static final String JAVA_SPECIFICATION_NAME = "java.specification.name";
    public static final String JAVA_SPECIFICATION_VENDOR = "java.specification.vendor";
    public static final String JAVA_SPECIFICATION_VERSION = "java.specification.version";
    public static final String JAVA_VENDOR = "java.vendor";
    public static final String JAVA_VENDOR_URL = "java.vendor.url";
    public static final String JAVA_VENDOR_VERSION = "java.vendor.version";
    public static final String JAVA_VERSION = "java.version";
    public static final String JAVA_VERSION_DATE = "java.version.date";
    public static final String JAVA_VM_SPECIFICATION_NAME = "java.vm.specification.name";
    public static final String JAVA_VM_SPECIFICATION_VENDOR = "java.vm.specification.vendor";
    public static final String JAVA_VM_SPECIFICATION_VERSION = "java.vm.specification.version";
    public static final String JAVA_VM_NAME = "java.vm.name";
    public static final String JAVA_VM_VENDOR = "java.vm.vendor";
    public static final String JAVA_VM_VERSION = "java.vm.version";
    public static final String LINE_SEPARATOR = "line.separator";
    public static final String OS_ARCH = "os.arch";
    public static final String OS_NAME = "os.name";
    public static final String OS_VERSION = "os.version";
    public static final String PATH_SEPARATOR = "path.separator";
    public static final String USER_DIR = "user.dir";
    public static final String USER_HOME = "user.home";
    public static final String USER_NAME = "user.name";

    /**
     * Returns the value of the system property with the given key.
     *
     * @param key the key
     * @return the value of the system property {@code key},
     * or {@code null} if it is not defined
     * or not accessible
     */
    public static @Nullable String getProperty(String key) {
        requireNotEmpty(key);
        try {
            return System.getProperty(key);
        } catch (SecurityException e) {
            return null;
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    /**
     * Returns the required value of the system property with the given key.
     *
     * @param key the key
     * @return the value of the system property {@code key} (never {@code null})
     * @throws IllegalStateException if the system property {@code key} is not defined or not accessible
     */
    public static String getRequiredProperty(String key) {
        final String value = getProperty(key);
        if (value == null) {
            throw new IllegalStateException("The required system property <" + key + "> is not set");
        }
        return value;
    }

    public static String getFileSeparator() {
        return getRequiredProperty(FILE_SEPARATOR);
    }

    public static char getFileSeparatorAsChar() {
        return Strings.single(getFileSeparator());
    }

    public static String getJavaClassPath() {
        return getRequiredProperty(JAVA_CLASS_PATH);
    }

    public static List<String> getJavaClassPathAsList() {
        return Collections.toList(Strings.split(getJavaClassPath(), getPathSeparatorAsChar()));
    }

    public static String getJavaClassVersion() {
        return getRequiredProperty(JAVA_CLASS_VERSION);
    }

    public static @Nullable String getJavaCompiler() {
        return getProperty(JAVA_COMPILER);
    }

    public static String getJavaHome() {
        return getRequiredProperty(JAVA_HOME);
    }

    public static File getJavaHomeAsFile() {
        return new File(getJavaHome());
    }

    public static Path getJavaHomeAsPath() {
        return Paths.get(getJavaHome());
    }

    public static String getJavaIoTmpdir() {
        return getRequiredProperty(JAVA_IO_TMPDIR);
    }

    public static File getJavaIoTmpdirAsFile() {
        return new File(getJavaIoTmpdir());
    }

    public static Path getJavaIoTmpdirAsPath() {
        return Paths.get(getJavaIoTmpdir());
    }

    public static String getJavaLibraryPath() {
        return getRequiredProperty(JAVA_LIBRARY_PATH);
    }

    public static List<String> getJavaLibraryPathAsList() {
        return Collections.toList(Strings.split(getJavaLibraryPath(), getPathSeparatorAsChar()));
    }

    public static String getJavaSpecificationName() {
        return getRequiredProperty(JAVA_SPECIFICATION_NAME);
    }

    public static String getJavaSpecificationVendor() {
        return getRequiredProperty(JAVA_SPECIFICATION_VENDOR);
    }

    public static String getJavaSpecificationVersion() {
        return getRequiredProperty(JAVA_SPECIFICATION_VERSION);
    }

    public static int getJavaSpecificationVersionAsInt() {
        final String value = getJavaSpecificationVersion();
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("The system property <" + JAVA_SPECIFICATION_VERSION + "> has the illegal value <" + value + '>', e);
        } catch (NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    public static String getJavaVendor() {
        return getRequiredProperty(JAVA_VENDOR);
    }

    public static String getJavaVendorUrl() {
        return getRequiredProperty(JAVA_VENDOR_URL);
    }

    public static @Nullable String getJavaVendorVersion() {
        return getProperty(JAVA_VENDOR_VERSION);
    }

    public static String getJavaVersion() {
        return getRequiredProperty(JAVA_VERSION);
    }

    public static Runtime.Version getJavaVersionAsVersion() {
        final String value = getJavaVersion();
        try {
            return Runtime.Version.parse(value);
        } catch (IllegalArgumentException e) {
            throw new IllegalStateException("The system property <" + JAVA_VERSION + "> has the illegal value <" + value + '>', e);
        } catch (NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    public static String getJavaVersionDate() {
        return getRequiredProperty(JAVA_VERSION_DATE);
    }

    public static LocalDate getJavaVersionDateAsLocalDate() {
        final String value = getJavaVersionDate();
        try {
            return LocalDate.parse(value);
        } catch (DateTimeParseException e) {
            throw new IllegalStateException("The system property <" + JAVA_VERSION_DATE + "> has the illegal value <" + value + '>', e);
        } catch (NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    public static String getJavaVmName() {
        return getRequiredProperty(JAVA_VM_NAME);
    }

    public static String getJavaVmSpecificationName() {
        return getRequiredProperty(JAVA_VM_SPECIFICATION_NAME);
    }

    public static String getJavaVmSpecificationVendor() {
        return getRequiredProperty(JAVA_VM_SPECIFICATION_VENDOR);
    }

    public static String getJavaVmSpecificationVersion() {
        return getRequiredProperty(JAVA_VM_SPECIFICATION_VERSION);
    }

    public static int getJavaVmSpecificationVersionAsInt() {
        final String value = getJavaVmSpecificationVersion();
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("The system property <" + JAVA_VM_SPECIFICATION_VERSION + "> has the illegal value <" + value + '>', e);
        } catch (NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    public static String getJavaVmVendor() {
        return getRequiredProperty(JAVA_VM_VENDOR);
    }

    public static String getJavaVmVersion() {
        return getRequiredProperty(JAVA_VM_VERSION);
    }

    public static Runtime.Version getJavaVmVersionAsVersion() {
        final String value = getJavaVmVersion();
        try {
            return Runtime.Version.parse(value);
        } catch (IllegalArgumentException e) {
            throw new IllegalStateException("The system property <" + JAVA_VM_VERSION + "> has the illegal value <" + value + '>', e);
        } catch (NullPointerException e) {
            throw new AssertionError(e);
        }
    }

    public static String getLineSeparator() {
        return System.lineSeparator();
    }

    public static LineSeparator getLineSeparatorAsLineSeparator() {
        return LineSeparator.forString(getLineSeparator());
    }

    public static String getOsArch() {
        return getRequiredProperty(OS_ARCH);
    }

    public static OsFamily getOsFamily() {
        return OsFamily.fromOsName(getOsName());
    }

    public static String getOsName() {
        return getRequiredProperty(OS_NAME);
    }

    public static String getOsVersion() {
        return getRequiredProperty(OS_VERSION);
    }

    public static String getPathSeparator() {
        return getRequiredProperty(PATH_SEPARATOR);
    }

    public static char getPathSeparatorAsChar() {
        return Strings.single(getPathSeparator());
    }

    public static String getUserDir() {
        return getRequiredProperty(USER_DIR);
    }

    public static File getUserDirAsFile() {
        return new File(getUserDir());
    }

    public static Path getUserDirAsPath() {
        return Paths.get(getUserDir());
    }

    public static String getUserHome() {
        return getRequiredProperty(USER_HOME);
    }

    public static File getUserHomeAsFile() {
        return new File(getUserHome());
    }

    public static Path getUserHomeAsPath() {
        return Paths.get(getUserHome());
    }

    public static String getUserName() {
        return getRequiredProperty(USER_NAME);
    }

    private SystemProperties() {}
}
