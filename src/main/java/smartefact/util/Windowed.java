/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.ToStringBuilder.toStringBuilder;

/**
 * Configuration for {@code windowed} methods.
 *
 * @author Laurent Pireyn
 */
public final class Windowed {
    final int size;
    int step = 1;
    boolean partialWindows;

    public Windowed(int size) {
        this.size = requireGt0(size);
    }

    @Contract("_ -> this")
    public Windowed step(int step) {
        this.step = requireGt0(step);
        return this;
    }

    @Contract("_ -> this")
    public Windowed partialWindows(boolean partialWindows) {
        this.partialWindows = partialWindows;
        return this;
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Windowed)) {
            return false;
        }
        final Windowed other = (Windowed) object;
        return size == other.size
            && step == other.step
            && partialWindows == other.partialWindows;
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCodeBuilder()
            .property(size)
            .property(step)
            .property(partialWindows)
            .build();
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("size", size)
            .property("step", step)
            .property("partialWindows", partialWindows)
            .build();
    }
}
