/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

/**
 * {@link SplittingIterator} based on an array of delimiters.
 *
 * @author Laurent Pireyn
 */
final class DelimitersSplittingIterator extends SplittingIterator {
    private final CharSequence[] delimiters;
    private final boolean ignoreCase;
    private int delimiterLastIndex = -1;

    DelimitersSplittingIterator(
        CharSequence string,
        int limit,
        CharSequence[] delimiters,
        boolean ignoreCase
    ) {
        super(string, limit);

        this.delimiters = delimiters;
        this.ignoreCase = ignoreCase;
    }

    @Override
    int getDelimiterFirstIndex(int startIndex) {
        int delimiterIndex = -1;
        int delimiterLength = -1;
        for (final CharSequence candidateDelimiter : delimiters) {
            final int candidateDelimiterLength = candidateDelimiter.length();
            if (candidateDelimiterLength == 0) {
                // Ignore empty delimiter
                continue;
            }
            final int candidateDelimiterIndex = Strings.indexOf(string, candidateDelimiter, startIndex, ignoreCase);
            if (candidateDelimiterIndex != -1
                && (delimiterIndex == -1 || candidateDelimiterIndex < delimiterIndex)
            ) {
                // Delimiter found (first or at a smaller index)
                delimiterIndex = candidateDelimiterIndex;
                delimiterLength = candidateDelimiterLength;
            }
        }
        delimiterLastIndex = delimiterLength != -1 ? delimiterIndex + delimiterLength - 1 : -1;
        return delimiterIndex;
    }

    @Override
    int getDelimiterLastIndex() {
        return delimiterLastIndex;
    }
}
