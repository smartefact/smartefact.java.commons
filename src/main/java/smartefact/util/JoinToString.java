/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.Preconditions.requireNotNull;
import static smartefact.util.ToStringBuilder.toStringBuilder;

/**
 * Configuration for {@code joinToString} methods.
 *
 * @author Laurent Pireyn
 */
public final class JoinToString {
    // This instance must not be modified
    static final JoinToString DEFAULT = new JoinToString();

    String separator = Strings.EMPTY;
    String prefix = Strings.EMPTY;
    String postfix = Strings.EMPTY;
    boolean skipNulls;
    int limit = -1;
    String truncated = "...";

    @Contract("_ -> this")
    public JoinToString separator(String separator) {
        this.separator = requireNotNull(separator);
        return this;
    }

    @Contract("_ -> this")
    public JoinToString separator(char separator) {
        return separator(String.valueOf(separator));
    }

    @Contract("_ -> this")
    public JoinToString prefix(String prefix) {
        this.prefix = requireNotNull(prefix);
        return this;
    }

    @Contract("_ -> this")
    public JoinToString prefix(char prefix) {
        return prefix(String.valueOf(prefix));
    }

    @Contract("_ -> this")
    public JoinToString postfix(String postfix) {
        this.postfix = requireNotNull(postfix);
        return this;
    }

    @Contract("_ -> this")
    public JoinToString postfix(char postfix) {
        return postfix(String.valueOf(postfix));
    }

    @Contract("_ -> this")
    public JoinToString skipNulls(boolean skipNulls) {
        this.skipNulls = skipNulls;
        return this;
    }

    @Contract("_ -> this")
    public JoinToString limit(int limit) {
        this.limit = limit;
        return this;
    }

    @Contract("_ -> this")
    public JoinToString truncated(String truncated) {
        this.truncated = requireNotNull(truncated);
        return this;
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof JoinToString)) {
            return false;
        }
        final JoinToString other = (JoinToString) object;
        return separator.equals(other.separator)
            && prefix.equals(other.prefix)
            && postfix.equals(other.postfix)
            && skipNulls == other.skipNulls
            && limit == other.limit
            && truncated.equals(other.truncated);
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCodeBuilder()
            .property(separator)
            .property(prefix)
            .property(postfix)
            .property(skipNulls)
            .property(limit)
            .property(truncated)
            .build();
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("separator", separator)
            .property("prefix", prefix)
            .property("postfix", postfix)
            .property("skipNulls", skipNulls)
            .property("limit", limit)
            .property("truncated", truncated)
            .build();
    }
}
