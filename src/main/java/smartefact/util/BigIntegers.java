/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.math.BigInteger;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link BigInteger}s.
 *
 * @author Laurent Pireyn
 */
public final class BigIntegers {
    @Contract(pure = true)
    public static @Nullable Byte toByteExactOrNull(BigInteger value) {
        requireNotNull(value);
        try {
            return value.byteValueExact();
        } catch (ArithmeticException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Integer toIntExactOrNull(BigInteger value) {
        requireNotNull(value);
        try {
            return value.intValueExact();
        } catch (ArithmeticException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Long toLongExactOrNull(BigInteger value) {
        requireNotNull(value);
        try {
            return value.longValueExact();
        } catch (ArithmeticException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static @Nullable Short toShortExactOrNull(BigInteger value) {
        requireNotNull(value);
        try {
            return value.shortValueExact();
        } catch (ArithmeticException e) {
            return null;
        }
    }

    private BigIntegers() {}
}
