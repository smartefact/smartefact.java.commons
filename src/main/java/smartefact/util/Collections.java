/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.IOException;
import static java.lang.Math.min;
import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static java.util.Collections.emptySortedSet;
import static java.util.Collections.singletonList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.checkNotNull;
import static smartefact.util.Preconditions.requireGe0;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.Preconditions.requireNotNull;
import smartefact.util.function.IndexedBiFunction;
import smartefact.util.function.IndexedConsumer;
import smartefact.util.function.IndexedFunction;
import smartefact.util.function.IndexedPredicate;

/**
 * Utilities for {@link Collection}s and {@link Iterable}s.
 *
 * @author Laurent Pireyn
 */
@SuppressWarnings("MixedMutabilityReturnType")
public final class Collections {
    @Contract(pure = true)
    public static <E> boolean all(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        for (final E element : iterable) {
            if (!predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    @Contract(pure = true)
    public static <E> boolean all(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return true;
        }
        return all(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> boolean any(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        for (final E element : iterable) {
            if (predicate.test(element)) {
                return true;
            }
        }
        return false;
    }

    @Contract(pure = true)
    public static <E> boolean any(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return false;
        }
        return any(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E, K, V> Map<K, V> associate(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends Pair<? extends K, ? extends V>> transform
    ) {
        return associateTo(
            iterable,
            new HashMap<>(),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, K, V> Map<K, V> associate(
        Collection<? extends E> collection,
        Function<? super E, ? extends Pair<? extends K, ? extends V>> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyMap();
        }
        return associateTo(
            collection,
            new HashMap<>(collection.size()),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, K> Map<K, E> associateBy(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends K> keySelector
    ) {
        return associateByTo(
            iterable,
            new HashMap<>(),
            keySelector
        );
    }

    @Contract(pure = true)
    public static <E, K> Map<K, E> associateBy(
        Collection<? extends E> collection,
        Function<? super E, ? extends K> keySelector
    ) {
        requireNotNull(collection);
        requireNotNull(keySelector);
        if (collection.isEmpty()) {
            return emptyMap();
        }
        return associateByTo(
            collection,
            new HashMap<>(collection.size()),
            keySelector
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, K, M extends Map<? super K, ? super E>> M associateByTo(
        Iterable<? extends E> iterable,
        M destination,
        Function<? super E, ? extends K> keySelector
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(keySelector);
        for (final E element : iterable) {
            final K key = keySelector.apply(element);
            if (key != null) {
                destination.put(key, element);
            }
        }
        return destination;
    }

    @Contract("_, _, _ -> param2")
    public static <E, K, V, M extends Map<? super K, ? super V>> M associateTo(
        Iterable<? extends E> iterable,
        M destination,
        Function<? super E, ? extends Pair<? extends K, ? extends V>> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        for (final E element : iterable) {
            final Pair<? extends K, ? extends V> pair = transform.apply(element);
            if (pair != null) {
                destination.put(pair.first(), pair.second());
            }
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E, V> Map<E, V> associateWith(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends V> valueSelector
    ) {
        return associateWithTo(
            iterable,
            new HashMap<>(),
            valueSelector
        );
    }

    @Contract(pure = true)
    public static <E, V> Map<E, V> associateWith(
        Collection<? extends E> collection,
        Function<? super E, ? extends V> valueSelector
    ) {
        requireNotNull(collection);
        requireNotNull(valueSelector);
        if (collection.isEmpty()) {
            return emptyMap();
        }
        return associateWithTo(
            collection,
            new HashMap<>(collection.size()),
            valueSelector
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, V, M extends Map<? super E, ? super V>> M associateWithTo(
        Iterable<? extends E> iterable,
        M destination,
        Function<? super E, ? extends V> valueSelector
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(valueSelector);
        for (final E element : iterable) {
            destination.put(element, valueSelector.apply(element));
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> List<List<E>> chunked(
        Iterable<? extends E> iterable,
        int size
    ) {
        return chunked(
            iterable,
            size,
            Function.identity()
        );
    }

    @Contract(pure = true)
    public static <E> List<List<E>> chunked(
        Collection<? extends E> collection,
        int size
    ) {
        return chunked(
            collection,
            size,
            Function.identity()
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> chunked(
        Iterable<? extends E> iterable,
        int size,
        Function<? super List<E>, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireGt0(size);
        requireNotNull(transform);
        return chunkedTo(
            iterable,
            new ArrayList<>(),
            size,
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> chunked(
        Collection<? extends E> collection,
        int size,
        Function<? super List<E>, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireGt0(size);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return chunkedTo(
            collection,
            new ArrayList<>(collection.size() / size + 1),
            size,
            transform
        );
    }

    @Contract("_, _, _, _ -> param2")
    private static <E, R, C extends Collection<R>> C chunkedTo(
        Iterable<? extends E> iterable,
        C destination,
        int size,
        Function<? super List<E>, ? extends R> transform
    ) {
        final Iterator<? extends E> iterator = iterable.iterator();
        List<E> slice = null;
        while (iterator.hasNext()) {
            if (slice == null) {
                slice = new ArrayList<>(size);
            }
            slice.add(iterator.next());
            if (slice.size() == size) {
                destination.add(transform.apply(slice));
                slice = null;
            }
        }
        if (slice != null) {
            destination.add(transform.apply(slice));
        }
        return destination;
    }

    @Contract(pure = true)
    public static boolean contains(
        Iterable<?> iterable,
        @Nullable Object value
    ) {
        requireNotNull(iterable);
        for (final Object element : iterable) {
            if (Objects.equals(element, value)) {
                return true;
            }
        }
        return false;
    }

    @Contract(pure = true)
    public static boolean contains(
        Collection<?> collection,
        @Nullable Object value
    ) {
        requireNotNull(collection);
        return collection.contains(value);
    }

    @Contract(pure = true)
    public static boolean containsAll(
        Iterable<?> iterable,
        Iterable<?> values
    ) {
        return containsAll(
            toSet(iterable),
            toSet(values)
        );
    }

    @Contract(pure = true)
    public static boolean containsAll(
        Iterable<?> iterable,
        Collection<?> values
    ) {
        return containsAll(
            toSet(iterable),
            toSet(values)
        );
    }

    @Contract(pure = true)
    public static boolean containsAll(
        Collection<?> collection,
        Iterable<?> values
    ) {
        return containsAll(
            toSet(collection),
            toSet(values)
        );
    }

    @Contract(pure = true)
    public static boolean containsAll(
        Collection<?> collection,
        Collection<?> values
    ) {
        requireNotNull(collection);
        requireNotNull(values);
        return collection.containsAll(values);
    }

    @Contract(pure = true)
    public static <E> int count(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        int result = 0;
        for (final E element : iterable) {
            if (predicate.test(element)) {
                ++result;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> int count(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return 0;
        }
        return count(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> List<E> distinct(Iterable<? extends E> iterable) {
        return distinctBy(
            iterable,
            Function.identity()
        );
    }

    @Contract(pure = true)
    public static <E, K> List<E> distinctBy(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends K> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        final List<E> result = new ArrayList<>();
        final Set<K> set = new HashSet<>();
        for (final E element : iterable) {
            if (set.add(selector.apply(element))) {
                result.add(element);
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> drop(
        Iterable<? extends E> iterable,
        int n
    ) {
        requireNotNull(iterable);
        requireGe0(n);
        final Iterator<? extends E> iterator = iterable.iterator();
        Iterators.skip(iterator, n);
        return Iterators.toList(iterator);
    }

    @Contract(pure = true)
    public static <E> List<E> drop(
        Collection<? extends E> collection,
        int n
    ) {
        requireNotNull(collection);
        requireGe0(n);
        final int size = collection.size();
        if (n >= size) {
            return emptyList();
        }
        final Iterator<? extends E> iterator = collection.iterator();
        Iterators.skip(iterator, n);
        return Iterators.toCollection(
            iterator,
            new ArrayList<>(size - n)
        );
    }

    @Contract(pure = true)
    public static <E> List<E> drop(
        List<? extends E> list,
        int n
    ) {
        requireNotNull(list);
        requireGe0(n);
        final int size = list.size();
        if (n >= size) {
            return emptyList();
        }
        return Iterators.toCollection(
            list.listIterator(n),
            new ArrayList<>(size - n)
        );
    }

    @Contract(pure = true)
    public static <E> List<E> dropLast(
        Iterable<? extends E> iterable,
        int n
    ) {
        final List<E> result = toArrayList(iterable);
        ModifiableCollections.removeLast(result, n);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> dropLast(
        Collection<? extends E> collection,
        int n
    ) {
        requireNotNull(collection);
        requireGe0(n);
        if (collection.size() <= n) {
            return emptyList();
        }
        final List<E> result = toArrayList(collection);
        ModifiableCollections.removeLast(result, n);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> dropLastWhile(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        final List<E> result = toArrayList(iterable);
        ModifiableCollections.removeLastWhile(result, predicate);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> dropLastWhile(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        final List<E> result = toArrayList(collection);
        ModifiableCollections.removeLastWhile(result, predicate);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> dropWhile(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        List<E> result = null;
        final Iterator<? extends E> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            final E element = iterator.next();
            if (!predicate.test(element)) {
                result = new ArrayList<>();
                result.add(element);
                break;
            }
        }
        if (result == null) {
            return emptyList();
        }
        return Iterators.toCollection(iterator, result);
    }

    @Contract(pure = true)
    public static <E> List<E> dropWhile(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return dropWhile(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E elementAt(
        Iterable<? extends E> iterable,
        int index
    ) {
        requireNotNull(iterable);
        if (index < 0) {
            throw new IndexOutOfBoundsException(index);
        }
        final Iterator<? extends E> iterator = iterable.iterator();
        int i = 0;
        E result = null;
        while (i <= index) {
            if (!iterator.hasNext()) {
                throw new IndexOutOfBoundsException(index);
            }
            result = iterator.next();
            ++i;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E elementAt(
        Collection<? extends E> collection,
        int index
    ) {
        requireNotNull(collection);
        if (index < 0 || index >= collection.size()) {
            throw new IndexOutOfBoundsException(index);
        }
        final Iterator<? extends E> iterator = collection.iterator();
        int i = 0;
        E result = null;
        while (i <= index) {
            assert iterator.hasNext();
            result = iterator.next();
            ++i;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> E elementAt(
        List<? extends E> list,
        int index
    ) {
        requireNotNull(list);
        return list.get(index);
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E elementAtOrElse(
        Iterable<? extends E> iterable,
        int index,
        IntFunction<E> defaultValue
    ) {
        requireNotNull(iterable);
        requireNotNull(defaultValue);
        if (index < 0) {
            return defaultValue.apply(index);
        }
        final Iterator<? extends E> iterator = iterable.iterator();
        int i = 0;
        E result = null;
        while (i <= index) {
            if (!iterator.hasNext()) {
                return defaultValue.apply(index);
            }
            result = iterator.next();
            ++i;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E elementAtOrElse(
        Collection<? extends E> collection,
        int index,
        IntFunction<E> defaultValue
    ) {
        requireNotNull(collection);
        requireNotNull(defaultValue);
        if (index < 0 || index >= collection.size()) {
            return defaultValue.apply(index);
        }
        final Iterator<? extends E> iterator = collection.iterator();
        int i = 0;
        E result = null;
        while (i <= index) {
            assert iterator.hasNext();
            result = iterator.next();
            ++i;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E elementAtOrElse(
        List<? extends E> list,
        int index,
        IntFunction<E> defaultValue
    ) {
        requireNotNull(list);
        requireNotNull(defaultValue);
        if (index < 0 || index >= list.size()) {
            return defaultValue.apply(index);
        }
        return list.get(index);
    }

    @Contract(pure = true)
    public static <E> @Nullable E elementAtOrNull(
        Iterable<? extends E> iterable,
        int index
    ) {
        requireNotNull(iterable);
        if (index < 0) {
            return null;
        }
        final Iterator<? extends E> iterator = iterable.iterator();
        int i = 0;
        E result = null;
        while (i <= index) {
            if (!iterator.hasNext()) {
                return null;
            }
            result = iterator.next();
            ++i;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E elementAtOrNull(
        Collection<? extends E> collection,
        int index
    ) {
        requireNotNull(collection);
        if (index < 0 || index >= collection.size()) {
            return null;
        }
        final Iterator<? extends E> iterator = collection.iterator();
        int i = 0;
        E result = null;
        while (i <= index) {
            assert iterator.hasNext();
            result = iterator.next();
            ++i;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E elementAtOrNull(
        List<? extends E> list,
        int index
    ) {
        requireNotNull(list);
        if (index < 0 || index >= list.size()) {
            return null;
        }
        return list.get(index);
    }

    @Contract(pure = true)
    public static <E> List<E> filter(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        return filterTo(
            iterable,
            new ArrayList<>(),
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> List<E> filter(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return filterTo(
            collection,
            new ArrayList<>(collection.size()),
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> List<E> filterIndexed(
        Iterable<? extends E> iterable,
        IndexedPredicate<? super E> predicate
    ) {
        return filterIndexedTo(
            iterable,
            new ArrayList<>(),
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> List<E> filterIndexed(
        Collection<? extends E> collection,
        IndexedPredicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return filterIndexedTo(
            collection,
            new ArrayList<>(collection.size()),
            predicate
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, C extends Collection<? super E>> C filterIndexedTo(
        Iterable<? extends E> iterable,
        C destination,
        IndexedPredicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(predicate);
        int index = 0;
        for (final E element : iterable) {
            if (predicate.test(index, element)) {
                destination.add(element);
            }
            ++index;
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> List<E> filterIsInstance(
        Iterable<?> iterable,
        Class<? extends E> type
    ) {
        return filterIsInstanceTo(
            iterable,
            new ArrayList<>(),
            type
        );
    }

    @Contract(pure = true)
    public static <E> List<E> filterIsInstance(
        Collection<?> collection,
        Class<? extends E> type
    ) {
        requireNotNull(collection);
        requireNotNull(type);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return filterIsInstanceTo(
            collection,
            new ArrayList<>(collection.size()),
            type
        );
    }

    @Contract("_, _, _ -> param2")
    public static <R, C extends Collection<? super R>> C filterIsInstanceTo(
        Iterable<?> iterable,
        C destination,
        Class<R> type
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(type);
        for (final Object element : iterable) {
            if (element == null || type.isAssignableFrom(element.getClass())) {
                destination.add(type.cast(element));
            }
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> List<E> filterNotNull(Iterable<? extends E> iterable) {
        return filterNotNullTo(
            iterable,
            new ArrayList<>()
        );
    }

    @Contract(pure = true)
    public static <E> List<E> filterNotNull(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return filterNotNullTo(
            collection,
            new ArrayList<>(collection.size())
        );
    }

    @Contract("_, _ -> param2")
    public static <E, C extends Collection<? super E>> C filterNotNullTo(
        Iterable<? extends E> iterable,
        C destination
    ) {
        return filterTo(
            iterable,
            destination,
            java.util.Objects::nonNull
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, C extends Collection<? super E>> C filterTo(
        Iterable<? extends E> iterable,
        C destination,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(predicate);
        for (final E element : iterable) {
            if (predicate.test(element)) {
                destination.add(element);
            }
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> @Nullable E find(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        return firstOrNull(
            iterable,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E find(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        return firstOrNull(
            collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E findLast(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        return lastOrNull(
            iterable,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E findLast(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        return lastOrNull(
            collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E findLast(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        return lastOrNull(
            list,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> E first(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        return iterator.next();
    }

    @Contract(pure = true)
    public static <E> E first(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        return collection.iterator().next();
    }

    @Contract(pure = true)
    public static <E> E first(List<? extends E> list) {
        requireNotNull(list);
        if (list.isEmpty()) {
            throw new NoSuchElementException();
        }
        return list.get(0);
    }

    @Contract(pure = true)
    public static <E> E first(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        for (final E element : iterable) {
            if (predicate.test(element)) {
                return element;
            }
        }
        throw new NoSuchElementException();
    }

    @Contract(pure = true)
    public static <E> E first(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        return first(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E, R> R firstNotNullOf(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> transform
    ) {
        final R result = firstNotNullOfOrNull(
            iterable,
            transform
        );
        if (result == null) {
            throw new NoSuchElementException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R firstNotNullOf(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> transform
    ) {
        final R result = firstNotNullOfOrNull(
            collection,
            transform
        );
        if (result == null) {
            throw new NoSuchElementException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R firstNotNullOfOrNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(transform);
        for (final E element : iterable) {
            final R result = transform.apply(element);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R firstNotNullOfOrNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (!collection.isEmpty()) {
            for (final E element : collection) {
                final R result = transform.apply(element);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E firstOrNull(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final Iterator<? extends E> iterator = iterable.iterator();
        return iterator.hasNext() ? iterator.next() : null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E firstOrNull(Collection<? extends E> collection) {
        requireNotNull(collection);
        return !collection.isEmpty() ? collection.iterator().next() : null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E firstOrNull(List<? extends E> list) {
        requireNotNull(list);
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E firstOrNull(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        for (final E element : iterable) {
            if (predicate.test(element)) {
                return element;
            }
        }
        return null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E firstOrNull(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return null;
        }
        return firstOrNull(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E, R> Iterable<R> flatMap(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends Iterable<R>> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(transform);
        return () -> new Iterator<>() {
            private final Iterator<? extends E> iterator = iterable.iterator();
            private @Nullable Iterator<R> subIterator;

            @Override
            public boolean hasNext() {
                while (subIterator == null || !subIterator.hasNext()) {
                    if (!iterator.hasNext()) {
                        return false;
                    }
                    subIterator = transform.apply(iterator.next()).iterator();
                }
                return true;
            }

            @Override
            public R next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                assert subIterator != null;
                return subIterator.next();
            }
        };
    }

    @Contract(pure = true)
    public static <E, R> Iterable<R> flatMap(
        Collection<? extends E> collection,
        Function<? super E, ? extends Iterable<R>> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return flatMap(
            (Iterable<? extends E>) collection,
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> Iterable<R> flatMapIndexed(
        Iterable<? extends E> iterable,
        IndexedFunction<? super E, ? extends Iterable<R>> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(transform);
        return () -> new Iterator<>() {
            private final Iterator<? extends E> iterator = iterable.iterator();
            private int index;
            private @Nullable Iterator<R> subIterator;

            @Override
            public boolean hasNext() {
                while (subIterator == null || !subIterator.hasNext()) {
                    if (!iterator.hasNext()) {
                        return false;
                    }
                    subIterator = transform.apply(index, iterator.next()).iterator();
                    ++index;
                }
                return true;
            }

            @Override
            public R next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                assert subIterator != null;
                return subIterator.next();
            }
        };
    }

    @Contract(pure = true)
    public static <E, R> Iterable<R> flatMapIndexed(
        Collection<? extends E> collection,
        IndexedFunction<? super E, ? extends Iterable<R>> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return flatMapIndexed(
            (Iterable<? extends E>) collection,
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, R, C extends Collection<? super R>> C flatMapIndexedTo(
        Iterable<? extends E> iterable,
        C destination,
        IndexedFunction<? super E, ? extends Iterable<R>> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        int index = 0;
        for (final E element : iterable) {
            ModifiableCollections.addAll(destination, transform.apply(index, element));
            ++index;
        }
        return destination;
    }

    @Contract("_, _, _ -> param2")
    public static <E, R, C extends Collection<? super R>> C flatMapTo(
        Iterable<? extends E> iterable,
        C destination,
        Function<? super E, ? extends Iterable<R>> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        for (final E element : iterable) {
            ModifiableCollections.addAll(destination, transform.apply(element));
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> List<E> flatten(Iterable<? extends Iterable<E>> iterable) {
        return flatMapTo(
            iterable,
            new ArrayList<>(),
            Function.identity()
        );
    }

    @Contract(pure = true)
    public static <E, R> R fold(
        Iterable<? extends E> iterable,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        R result = initial;
        for (final E element : iterable) {
            result = operation.apply(result, element);
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R fold(
        Collection<? extends E> collection,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(collection);
        requireNotNull(operation);
        if (collection.isEmpty()) {
            return initial;
        }
        return fold(
            (Iterable<? extends E>) collection,
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> R foldIndexed(
        Iterable<? extends E> iterable,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        R result = initial;
        int index = 0;
        for (final E element : iterable) {
            result = operation.apply(index, result, element);
            ++index;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R foldIndexed(
        Collection<? extends E> collection,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(collection);
        requireNotNull(operation);
        if (collection.isEmpty()) {
            return initial;
        }
        return foldIndexed(
            (Iterable<? extends E>) collection,
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> R foldRight(
        Iterable<? extends E> iterable,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return foldRight(
            toList(iterable),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> R foldRight(
        Collection<? extends E> collection,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return foldRight(
            toList(collection),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> R foldRight(
        List<? extends E> list,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(list);
        requireNotNull(operation);
        R result = initial;
        for (final ListIterator<? extends E> iterator = list.listIterator(list.size()); iterator.hasPrevious(); ) {
            result = operation.apply(result, iterator.previous());
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R foldRightIndexed(
        Iterable<? extends E> iterable,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return foldRightIndexed(
            toList(iterable),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> R foldRightIndexed(
        Collection<? extends E> collection,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return foldRightIndexed(
            toList(collection),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> R foldRightIndexed(
        List<? extends E> list,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(list);
        requireNotNull(operation);
        R result = initial;
        for (final ListIterator<? extends E> iterator = list.listIterator(list.size()); iterator.hasPrevious(); ) {
            result = operation.apply(iterator.previousIndex(), result, iterator.previous());
        }
        return result;
    }

    /**
     * Performs the given action on each element of the given iterable.
     *
     * @param iterable the iterable
     * @param action the action to perform on each element of {@code iterable}
     * @param <E> the type of elements in {@code iterable}
     * @see #forEachIndexed(Iterable, IndexedConsumer)
     * @see #onEach(Iterable, Consumer)
     */
    @Contract(pure = true)
    public static <E> void forEach(
        Iterable<? extends E> iterable,
        Consumer<? super E> action
    ) {
        requireNotNull(iterable);
        requireNotNull(action);
        iterable.forEach(action);
    }

    /**
     * Performs the given action on each element of the given iterable.
     *
     * @param iterable the iterable
     * @param action the action to perform on each element of {@code iterable}
     * @param <E> the type of elements in {@code iterable}
     * @see #forEach(Iterable, Consumer)
     */
    @Contract(pure = true)
    public static <E> void forEachIndexed(
        Iterable<? extends E> iterable,
        IndexedConsumer<? super E> action
    ) {
        requireNotNull(iterable);
        requireNotNull(action);
        int index = 0;
        for (final E element : iterable) {
            action.accept(index, element);
            ++index;
        }
    }

    /**
     * Performs the given action on each element of the given collection.
     *
     * @param collection the collection
     * @param action the action to perform on each element of {@code collection}
     * @param <E> the type of elements in {@code collection}
     */
    @Contract(pure = true)
    public static <E> void forEachIndexed(
        Collection<? extends E> collection,
        IndexedConsumer<? super E> action
    ) {
        requireNotNull(collection);
        requireNotNull(action);
        if (!collection.isEmpty()) {
            forEachIndexed(
                (Iterable<? extends E>) collection,
                action
            );
        }
    }

    @Contract(pure = true)
    public static <E, K> Map<K, List<E>> groupBy(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends K> keySelector
    ) {
        return groupBy(
            iterable,
            keySelector,
            Function.identity()
        );
    }

    @Contract(pure = true)
    public static <E, K, V> Map<K, List<V>> groupBy(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends K> keySelector,
        Function<? super E, ? extends V> valueTransform
    ) {
        return groupByTo(
            iterable,
            new HashMap<>(),
            keySelector,
            valueTransform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, K, M extends Map<? super K, List<E>>> M groupByTo(
        Iterable<? extends E> iterable,
        M destination,
        Function<? super E, ? extends K> keySelector
    ) {
        return groupByTo(
            iterable,
            destination,
            keySelector,
            Function.identity()
        );
    }

    @Contract("_, _, _, _ -> param2")
    public static <E, K, V, M extends Map<? super K, List<V>>> M groupByTo(
        Iterable<? extends E> iterable,
        M destination,
        Function<? super E, ? extends K> keySelector,
        Function<? super E, ? extends V> valueTransform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(keySelector);
        requireNotNull(valueTransform);
        for (final E element : iterable) {
            final K key = keySelector.apply(element);
            destination.computeIfAbsent(key, _key -> new ArrayList<>())
                .add(valueTransform.apply(element));
        }
        return destination;
    }

    @Contract(pure = true)
    public static int indexOf(
        Iterable<?> iterable,
        @Nullable Object value
    ) {
        return indexOfFirst(
            iterable,
            element -> Objects.equals(element, value)
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        Collection<?> collection,
        @Nullable Object value
    ) {
        return indexOfFirst(
            collection,
            element -> Objects.equals(element, value)
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        List<?> list,
        @Nullable Object value
    ) {
        requireNotNull(list);
        try {
            return list.indexOf(value);
        } catch (NullPointerException | ClassCastException e) {
            // Implementations of List may throw a NullPointerException when value is null
            // or a ClassCastException when value is of an incompatible type
            // Treat this as not found
            return -1;
        }
    }

    @Contract(pure = true)
    public static <E> int indexOfFirst(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        int index = 0;
        for (final E element : iterable) {
            if (predicate.test(element)) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    @Contract(pure = true)
    public static <E> int indexOfFirst(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return -1;
        }
        return indexOfFirst(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> int indexOfLast(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        int result = -1;
        int index = 0;
        for (final E element : iterable) {
            if (predicate.test(element)) {
                result = index;
            }
            ++index;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> int indexOfLast(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return -1;
        }
        return indexOfLast(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> int indexOfLast(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        requireNotNull(list);
        requireNotNull(predicate);
        if (!list.isEmpty()) {
            for (final ListIterator<? extends E> iterator = list.listIterator(list.size()); iterator.hasPrevious(); ) {
                final E element = iterator.previous();
                if (predicate.test(element)) {
                    return iterator.nextIndex();
                }
            }
        }
        return -1;
    }

    @Contract(pure = true)
    public static <E> Set<E> intersect(
        Iterable<? extends E> iterable,
        Iterable<? extends E> other
    ) {
        return intersect(
            toSet(iterable),
            toSet(other)
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> intersect(
        Iterable<? extends E> iterable,
        Collection<? extends E> other
    ) {
        return intersect(
            toSet(iterable),
            toSet(other)
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> intersect(
        Collection<? extends E> collection,
        Iterable<? extends E> other
    ) {
        return intersect(
            toSet(collection),
            toSet(other)
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> intersect(
        Collection<? extends E> collection,
        Collection<? extends E> other
    ) {
        return intersect(
            toSet(collection),
            toSet(other)
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> intersect(
        Set<? extends E> set,
        Set<? extends E> other
    ) {
        requireNotNull(set);
        requireNotNull(other);
        if (set.isEmpty() || other.isEmpty()) {
            return emptySet();
        }
        // Determine set1 and set2, where set1 is the smaller one
        final Set<? extends E> set1;
        final Set<? extends E> set2;
        if (set.size() <= other.size()) {
            set1 = set;
            set2 = other;
        } else {
            set1 = other;
            set2 = set;
        }
        final Set<E> result = new HashSet<>(set1.size());
        for (final E element : set1) {
            if (set2.contains(element)) {
                result.add(element);
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static boolean isEmpty(Iterable<?> iterable) {
        requireNotNull(iterable);
        return !iterable.iterator().hasNext();
    }

    @Contract(pure = true)
    public static boolean isEmpty(Collection<?> collection) {
        requireNotNull(collection);
        return collection.isEmpty();
    }

    @Contract(pure = true)
    public static boolean isNullOrEmpty(@Nullable Iterable<?> iterable) {
        return iterable == null || !iterable.iterator().hasNext();
    }

    @Contract(pure = true)
    public static boolean isNullOrEmpty(@Nullable Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    @Contract("_, _ -> param2")
    public static <A extends Appendable> A joinTo(
        Iterable<?> iterable,
        A buffer
    ) throws IOException {
        return joinTo(
            iterable,
            buffer,
            JoinToString.DEFAULT
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, A extends Appendable> A joinTo(
        Iterable<E> iterable,
        A buffer,
        Function<? super E, ? extends @Nullable CharSequence> transform
    ) throws IOException {
        return joinTo(
            iterable,
            buffer,
            JoinToString.DEFAULT,
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <A extends Appendable> A joinTo(
        Iterable<?> iterable,
        A buffer,
        JoinToString config
    ) throws IOException {
        return joinTo(
            iterable,
            buffer,
            config,
            Objects::toString
        );
    }

    @Contract("_, _, _, _ -> param2")
    public static <E, A extends Appendable> A joinTo(
        Iterable<E> iterable,
        A buffer,
        JoinToString config,
        Function<? super E, ? extends @Nullable CharSequence> transform
    ) throws IOException {
        requireNotNull(iterable);
        requireNotNull(buffer);
        requireNotNull(config);
        requireNotNull(transform);
        buffer.append(config.prefix);
        int count = 0;
        for (final E element : iterable) {
            final @Nullable CharSequence value = transform.apply(element);
            if (value == null && config.skipNulls) {
                continue;
            }
            if (count > 0) {
                buffer.append(config.separator);
            }
            if (count == config.limit) {
                buffer.append(config.truncated);
                break;
            }
            buffer.append(value);
            ++count;
        }
        buffer.append(config.postfix);
        return buffer;
    }

    @Contract("_, _ -> param2")
    public static StringBuilder joinTo(
        Iterable<?> iterable,
        StringBuilder buffer
    ) {
        return joinTo(
            iterable,
            buffer,
            JoinToString.DEFAULT
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E> StringBuilder joinTo(
        Iterable<E> iterable,
        StringBuilder buffer,
        Function<? super E, ? extends @Nullable CharSequence> transform
    ) {
        return joinTo(
            iterable,
            buffer,
            JoinToString.DEFAULT,
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static StringBuilder joinTo(
        Iterable<?> iterable,
        StringBuilder buffer,
        JoinToString config
    ) {
        return joinTo(
            iterable,
            buffer,
            config,
            Objects::toString
        );
    }

    @Contract("_, _, _, _ -> param2")
    public static <E> StringBuilder joinTo(
        Iterable<E> iterable,
        StringBuilder buffer,
        JoinToString config,
        Function<? super E, ? extends @Nullable CharSequence> transform
    ) {
        try {
            return Collections.<E, StringBuilder>joinTo(
                iterable,
                buffer,
                config,
                transform
            );
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Contract(pure = true)
    public static String joinToString(Iterable<?> iterable) {
        return joinToString(
            iterable,
            JoinToString.DEFAULT
        );
    }

    @Contract(pure = true)
    public static <E> String joinToString(
        Iterable<E> iterable,
        Function<? super E, ? extends @Nullable CharSequence> transform
    ) {
        return joinToString(
            iterable,
            JoinToString.DEFAULT,
            transform
        );
    }

    @Contract(pure = true)
    public static String joinToString(
        Iterable<?> iterable,
        JoinToString config
    ) {
        return joinToString(
            iterable,
            config,
            Objects::toString
        );
    }

    @Contract(pure = true)
    public static <E> String joinToString(
        Iterable<E> iterable,
        JoinToString config,
        Function<? super E, ? extends @Nullable CharSequence> transform
    ) {
        return joinTo(
            iterable,
            new StringBuilder(),
            config,
            transform
        )
            .toString();
    }

    @Contract(pure = true)
    public static <E> E last(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        E result;
        do {
            result = iterator.next();
        } while (iterator.hasNext());
        return result;
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E last(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        E result = null;
        for (final E element : collection) {
            result = element;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> E last(List<? extends E> list) {
        requireNotNull(list);
        if (list.isEmpty()) {
            throw new NoSuchElementException();
        }
        return list.get(lastIndex(list));
    }

    @Contract(pure = true)
    public static <E> @SuppressWarnings("NullAway") E last(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        E result = null;
        boolean found = false;
        for (final E element : iterable) {
            if (predicate.test(element)) {
                result = element;
                found = true;
            }
        }
        if (!found) {
            throw new NoSuchElementException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> E last(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        return last(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> E last(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        requireNotNull(list);
        requireNotNull(predicate);
        if (!list.isEmpty()) {
            for (
                final ListIterator<? extends E> iterator = list.listIterator(list.size());
                iterator.hasPrevious();
            ) {
                final E element = iterator.previous();
                if (predicate.test(element)) {
                    return element;
                }
            }
        }
        throw new NoSuchElementException();
    }

    @Contract(pure = true)
    public static int lastIndex(List<?> list) {
        requireNotNull(list);
        return list.size() - 1;
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        Iterable<?> iterable,
        @Nullable Object value
    ) {
        return indexOfLast(
            iterable,
            element -> Objects.equals(element, value)
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        Collection<?> collection,
        @Nullable Object value
    ) {
        return indexOfLast(
            collection,
            element -> Objects.equals(element, value)
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        List<?> list,
        @Nullable Object value
    ) {
        requireNotNull(list);
        try {
            return list.lastIndexOf(value);
        } catch (NullPointerException | ClassCastException e) {
            // Implementations of List may throw a NullPointerException when value is null
            // or a ClassCastException when value is of an incompatible type
            // Treat this as not found
            return -1;
        }
    }

    @Contract(pure = true)
    public static <E, R> R lastNotNullOf(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> transform
    ) {
        final R result = lastNotNullOfOrNull(
            iterable,
            transform
        );
        if (result == null) {
            throw new NoSuchElementException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R lastNotNullOf(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> transform
    ) {
        final R result = lastNotNullOfOrNull(
            collection,
            transform
        );
        if (result == null) {
            throw new NoSuchElementException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R lastNotNullOf(
        List<? extends E> list,
        Function<? super E, ? extends R> transform
    ) {
        final R result = lastNotNullOfOrNull(
            list,
            transform
        );
        if (result == null) {
            throw new NoSuchElementException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R lastNotNullOfOrNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(transform);
        R result = null;
        for (final E element : iterable) {
            final R value = transform.apply(element);
            if (value != null) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R lastNotNullOfOrNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return null;
        }
        return lastNotNullOfOrNull(
            (Iterable<? extends E>) collection,
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R lastNotNullOfOrNull(
        List<? extends E> list,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(list);
        requireNotNull(transform);
        if (!list.isEmpty()) {
            for (
                final ListIterator<? extends E> iterator = list.listIterator(list.size());
                iterator.hasPrevious();
            ) {
                final R value = transform.apply(iterator.previous());
                if (value != null) {
                    return value;
                }
            }
        }
        return null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E lastOrNull(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        E result = null;
        for (final E element : iterable) {
            result = element;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E lastOrNull(Collection<? extends E> collection) {
        requireNotNull(collection);
        return !collection.isEmpty() ? lastOrNull((Iterable<? extends E>) collection) : null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E lastOrNull(List<? extends E> list) {
        requireNotNull(list);
        return !list.isEmpty() ? list.get(lastIndex(list)) : null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E lastOrNull(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        E result = null;
        for (final E element : iterable) {
            if (predicate.test(element)) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E lastOrNull(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return null;
        }
        return lastOrNull(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E lastOrNull(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        requireNotNull(list);
        requireNotNull(predicate);
        if (!list.isEmpty()) {
            for (
                final ListIterator<? extends E> iterator = list.listIterator(list.size());
                iterator.hasPrevious();
            ) {
                final E element = iterator.previous();
                if (predicate.test(element)) {
                    return element;
                }
            }
        }
        return null;
    }

    @Contract("_, _ -> new")
    public static <E> List<E> list(
        int size,
        IntFunction<? extends E> init
    ) {
        requireGe0(size);
        requireNotNull(init);
        if (size == 0) {
            return emptyList();
        }
        final List<E> result = new ArrayList<>(size);
        for (int i = 0; i < size; ++i) {
            result.add(init.apply(i));
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> List<R> map(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> transform
    ) {
        return mapTo(
            iterable,
            new ArrayList<>(),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> map(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return mapTo(
            collection,
            new ArrayList<>(collection.size()),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> mapIndexed(
        Iterable<? extends E> iterable,
        IndexedFunction<? super E, ? extends R> transform
    ) {
        return mapIndexedTo(
            iterable,
            new ArrayList<>(),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> mapIndexed(
        Collection<? extends E> collection,
        IndexedFunction<? super E, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return mapIndexedTo(
            collection,
            new ArrayList<>(collection.size()),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> mapIndexedNotNull(
        Iterable<? extends E> iterable,
        IndexedFunction<? super E, ? extends R> transform
    ) {
        return mapIndexedNotNullTo(
            iterable,
            new ArrayList<>(),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> mapIndexedNotNull(
        Collection<? extends E> collection,
        IndexedFunction<? super E, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return mapIndexedNotNullTo(
            collection,
            new ArrayList<>(collection.size()),
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, R, C extends Collection<? super R>> C mapIndexedNotNullTo(
        Iterable<? extends E> iterable,
        C destination,
        IndexedFunction<? super E, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        int index = 0;
        for (final E element : iterable) {
            final R value = transform.apply(index, element);
            if (value != null) {
                destination.add(value);
            }
            ++index;
        }
        return destination;
    }

    @Contract("_, _, _ -> param2")
    public static <E, R, C extends Collection<? super R>> C mapIndexedTo(
        Iterable<? extends E> iterable,
        C destination,
        IndexedFunction<? super E, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        int index = 0;
        for (final E element : iterable) {
            destination.add(transform.apply(index, element));
            ++index;
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E, R> List<R> mapNotNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> transform
    ) {
        return mapNotNullTo(
            iterable,
            new ArrayList<>(),
            transform
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> mapNotNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return mapNotNullTo(
            collection,
            new ArrayList<>(collection.size()),
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <E, R, C extends Collection<? super R>> C mapNotNullTo(
        Iterable<? extends E> iterable,
        C destination,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        for (final E element : iterable) {
            final R value = transform.apply(element);
            if (value != null) {
                destination.add(value);
            }
        }
        return destination;
    }

    @Contract("_, _, _ -> param2")
    public static <E, R, C extends Collection<? super R>> C mapTo(
        Iterable<? extends E> iterable,
        C destination,
        Function<? super E, ? extends R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        requireNotNull(transform);
        for (final E element : iterable) {
            destination.add(transform.apply(element));
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable E maxByOrNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        E result = null;
        R max = null;
        for (final E element : iterable) {
            final R value = checkNotNull(selector.apply(element));
            if (max == null || value.compareTo(max) > 0) {
                result = element;
                max = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable E maxByOrNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            return null;
        }
        return maxByOrNull(
            (Iterable<? extends E>) collection,
            selector
        );
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> R maxOf(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        R result = checkNotNull(selector.apply(iterator.next()));
        while (iterator.hasNext()) {
            final R value = checkNotNull(selector.apply(iterator.next()));
            if (value.compareTo(result) > 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable R maxOfOrNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        R result = null;
        for (final E element : iterable) {
            final R value = checkNotNull(selector.apply(element));
            if (result == null || value.compareTo(result) > 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable R maxOfOrNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            return null;
        }
        return maxOfOrNull(
            (Iterable<? extends E>) collection,
            selector
        );
    }

    @Contract(pure = true)
    public static <E, R> R maxOfWith(
        Iterable<? extends E> iterable,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        requireNotNull(selector);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        R result = selector.apply(iterator.next());
        while (iterator.hasNext()) {
            final R value = checkNotNull(selector.apply(iterator.next()));
            if (comparator.compare(value, result) > 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R maxOfWith(
        Collection<? extends E> collection,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(comparator);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        return maxOfWith(
            (Iterable<? extends E>) collection,
            comparator,
            selector
        );
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R maxOfWithOrNull(
        Iterable<? extends E> iterable,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        requireNotNull(selector);
        R result = null;
        for (final E element : iterable) {
            final R value = checkNotNull(selector.apply(element));
            if (result == null || comparator.compare(value, result) > 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R maxOfWithOrNull(
        Collection<? extends E> collection,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(comparator);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            return null;
        }
        return maxOfWithOrNull(
            (Iterable<? extends E>) collection,
            comparator,
            selector
        );
    }

    @Contract(pure = true)
    public static <E extends Comparable<E>> @Nullable E maxOrNull(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        E result = null;
        for (final E element : iterable) {
            if (result == null || element.compareTo(result) > 0) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E extends Comparable<E>> @Nullable E maxOrNull(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return null;
        }
        return maxOrNull((Iterable<? extends E>) collection);
    }

    @Contract(pure = true)
    public static <E> E maxWith(
        Iterable<? extends E> iterable,
        Comparator<? super E> comparator
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        E result = iterator.next();
        while (iterator.hasNext()) {
            final E element = iterator.next();
            if (comparator.compare(element, result) > 0) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E maxWithOrNull(
        Iterable<? extends E> iterable,
        Comparator<? super E> comparator
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        E result = null;
        for (final E element : iterable) {
            if (result == null || comparator.compare(element, result) > 0) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable E minByOrNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        E result = null;
        R min = null;
        for (final E element : iterable) {
            final R value = checkNotNull(selector.apply(element));
            if (min == null || value.compareTo(min) < 0) {
                result = element;
                min = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable E minByOrNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            return null;
        }
        return minByOrNull(
            (Iterable<? extends E>) collection,
            selector
        );
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> R minOf(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        R result = checkNotNull(selector.apply(iterator.next()));
        while (iterator.hasNext()) {
            final R value = checkNotNull(selector.apply(iterator.next()));
            if (value.compareTo(result) < 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable R minOfOrNull(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        R result = null;
        for (final E element : iterable) {
            final R value = checkNotNull(selector.apply(element));
            if (result == null || value.compareTo(result) < 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<R>> @Nullable R minOfOrNull(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            return null;
        }
        return minOfOrNull(
            (Iterable<? extends E>) collection,
            selector
        );
    }

    @Contract(pure = true)
    public static <E, R> R minOfWith(
        Iterable<? extends E> iterable,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        requireNotNull(selector);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        R result = checkNotNull(selector.apply(iterator.next()));
        while (iterator.hasNext()) {
            final R value = checkNotNull(selector.apply(iterator.next()));
            if (comparator.compare(value, result) < 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> R minOfWith(
        Collection<? extends E> collection,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(comparator);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        return minOfWith(
            (Iterable<? extends E>) collection,
            comparator,
            selector
        );
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R minOfWithOrNull(
        Iterable<? extends E> iterable,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        requireNotNull(selector);
        R result = null;
        for (final E element : iterable) {
            final R value = checkNotNull(selector.apply(element));
            if (result == null || comparator.compare(value, result) < 0) {
                result = value;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E, R> @Nullable R minOfWithOrNull(
        Collection<? extends E> collection,
        Comparator<? super R> comparator,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(comparator);
        requireNotNull(selector);
        if (collection.isEmpty()) {
            return null;
        }
        return minOfWith(
            (Iterable<? extends E>) collection,
            comparator,
            selector
        );
    }

    @Contract(pure = true)
    public static <E extends Comparable<E>> @Nullable E minOrNull(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        E result = null;
        for (final E element : iterable) {
            if (result == null || element.compareTo(result) < 0) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E extends Comparable<E>> @Nullable E minOrNull(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return null;
        }
        return minOrNull((Iterable<? extends E>) collection);
    }

    @Contract(pure = true)
    public static <E> E minWith(
        Iterable<? extends E> iterable,
        Comparator<? super E> comparator
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        E result = iterator.next();
        while (iterator.hasNext()) {
            final E element = iterator.next();
            if (comparator.compare(element, result) < 0) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E minWithOrNull(
        Iterable<? extends E> iterable,
        Comparator<? super E> comparator
    ) {
        requireNotNull(iterable);
        requireNotNull(comparator);
        E result = null;
        for (final E element : iterable) {
            if (result == null || comparator.compare(element, result) < 0) {
                result = element;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> minusElement(
        Iterable<? extends E> iterable,
        E element
    ) {
        requireNotNull(iterable);
        final List<E> result = toArrayList(iterable);
        result.remove(element);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> minusElement(
        Collection<? extends E> collection,
        E element
    ) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptyList();
        }
        final List<E> result = toArrayList(collection);
        result.remove(element);
        return result;
    }

    @Contract(pure = true)
    public static <E> boolean none(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        for (final E element : iterable) {
            if (predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    @Contract(pure = true)
    public static <E> boolean none(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return true;
        }
        return none(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    /**
     * Performs the given action on each element of the given iterable,
     * and returns the iterable afterwards.
     *
     * @param iterable the iterable
     * @param action the action to perform on each element of {@code iterable}
     * @return {@code iterable}
     * @param <E> the type of elements in {@code iterable}
     * @param <C> the type of {@code iterable}
     * @see #forEach(Iterable, Consumer)
     */
    @Contract("_, _ -> param1")
    public static <E, C extends Iterable<? extends E>> C onEach(
        C iterable,
        Consumer<? super E> action
    ) {
        requireNotNull(iterable);
        requireNotNull(action);
        forEach(iterable, action);
        return iterable;
    }

    /**
     * Performs the given action on each element of the given iterable,
     * and returns the iterable afterwards.
     *
     * @param iterable the iterable
     * @param action the action to perform on each element of {@code iterable}
     * @return {@code iterable}
     * @param <E> the type of elements in {@code iterable}
     * @param <C> the type of {@code iterable}
     * @see #forEachIndexed(Iterable, IndexedConsumer)
     */
    @Contract("_, _ -> param1")
    public static <E, C extends Iterable<? extends E>> C onEachIndexed(
        C iterable,
        IndexedConsumer<? super E> action
    ) {
        requireNotNull(iterable);
        requireNotNull(action);
        forEachIndexed(iterable, action);
        return iterable;
    }

    /**
     * Returns the given iterable if it is not {@code null},
     * and the empty list otherwise.
     *
     * @param iterable the iterable
     * @return {@code iterable} if it is not {@code null}, the empty list otherwise
     * @param <E> the type of elements in {@code iterable}
     */
    @Contract(value = "!null -> param1", pure = true)
    public static <E> Iterable<E> orEmpty(@Nullable Iterable<E> iterable) {
        return iterable != null ? iterable : emptyList();
    }

    /**
     * Returns the given collection if it is not {@code null},
     * and the empty list otherwise.
     *
     * @param collection the collection
     * @return {@code collection} if it is not {@code null}, the empty list otherwise
     * @param <E> the type of elements in {@code collection}
     */
    @Contract(value = "!null -> param1", pure = true)
    public static <E> Collection<E> orEmpty(@Nullable Collection<E> collection) {
        return collection != null ? collection : emptyList();
    }

    /**
     * Returns the given list if it is not {@code null},
     * and the empty list otherwise.
     *
     * @param list the list
     * @return {@code list} if it is not {@code null}, the empty list otherwise
     * @param <E> the type of elements in {@code list}
     */
    @Contract(value = "!null -> param1", pure = true)
    public static <E> List<E> orEmpty(@Nullable List<E> list) {
        return list != null ? list : emptyList();
    }

    /**
     * Returns the given set if it is not {@code null},
     * and the empty set otherwise.
     *
     * @param set the set
     * @return {@code set} if it is not {@code null}, the empty set otherwise
     * @param <E> the type of elements in {@code set}
     */
    @Contract(value = "!null -> param1", pure = true)
    public static <E> Set<E> orEmpty(@Nullable Set<E> set) {
        return set != null ? set : emptySet();
    }

    /**
     * Returns the given sorted set if it is not {@code null},
     * and the empty sorted set otherwise.
     *
     * @param set the sorted set
     * @return {@code set} if it is not {@code null}, the empty sorted set otherwise
     * @param <E> the type of elements in {@code set}
     */
    @Contract(value = "!null -> param1", pure = true)
    public static <E> SortedSet<E> orEmpty(@Nullable SortedSet<E> set) {
        return set != null ? set : emptySortedSet();
    }

    @Contract(pure = true)
    public static <E> Pair<List<E>, List<E>> partition(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        final List<E> trueElements = new ArrayList<>();
        final List<E> falseElements = new ArrayList<>();
        partitionTo(
            iterable,
            predicate,
            trueElements,
            falseElements
        );
        return new Pair<>(trueElements, falseElements);
    }

    @Contract(pure = true)
    public static <E> Pair<List<E>, List<E>> partition(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        final List<E> trueElements;
        final List<E> falseElements;
        if (collection.isEmpty()) {
            trueElements = emptyList();
            falseElements = emptyList();
        } else {
            final int size = collection.size();
            trueElements = new ArrayList<>(size);
            falseElements = new ArrayList<>(size);
            partitionTo(
                collection,
                predicate,
                trueElements,
                falseElements
            );
        }
        return new Pair<>(trueElements, falseElements);
    }

    private static <E> void partitionTo(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate,
        Collection<? super E> trueElements,
        Collection<? super E> falseElements
    ) {
        for (final E element : iterable) {
            if (predicate.test(element)) {
                trueElements.add(element);
            } else {
                falseElements.add(element);
            }
        }
    }

    @Contract(pure = true)
    public static <E> List<E> plus(
        Iterable<? extends E> iterable,
        Iterable<? extends E> other
    ) {
        return plus(
            toList(iterable),
            toList(other)
        );
    }

    @Contract(pure = true)
    public static <E> List<E> plus(
        Iterable<? extends E> iterable,
        Collection<? extends E> other
    ) {
        return plus(
            toList(iterable),
            other
        );
    }

    @Contract(pure = true)
    public static <E> List<E> plus(
        Collection<? extends E> collection,
        Iterable<? extends E> other
    ) {
        return plus(
            collection,
            toList(other)
        );
    }

    @Contract(pure = true)
    public static <E> List<E> plus(
        Collection<? extends E> collection,
        Collection<? extends E> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        final List<E> result = new ArrayList<>(collection.size() + other.size());
        result.addAll(collection);
        result.addAll(other);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> plusElement(
        Iterable<? extends E> iterable,
        E element
    ) {
        requireNotNull(iterable);
        final List<E> result = toArrayList(iterable);
        result.add(element);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> plusElement(
        Collection<? extends E> collection,
        E element
    ) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return singletonList(element);
        }
        final List<E> result = new ArrayList<>(collection.size() + 1);
        result.addAll(collection);
        result.add(element);
        return result;
    }

    @Contract(pure = true)
    public static <E> E random(Collection<? extends E> collection) {
        return random(
            collection,
            GlobalRandom.INSTANCE
        );
    }

    @Contract(pure = true)
    public static <E> E random(List<? extends E> list) {
        return random(
            list,
            GlobalRandom.INSTANCE
        );
    }

    @Contract(pure = true)
    public static <E> E random(
        Collection<? extends E> collection,
        Random random
    ) {
        requireNotNull(collection);
        requireNotNull(random);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        return elementAt(
            collection,
            random.nextInt(collection.size())
        );
    }

    @Contract(pure = true)
    public static <E> E random(
        List<? extends E> list,
        Random random
    ) {
        requireNotNull(list);
        requireNotNull(random);
        if (list.isEmpty()) {
            throw new NoSuchElementException();
        }
        return elementAt(
            list,
            random.nextInt(list.size())
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E randomOrNull(Collection<? extends E> collection) {
        return randomOrNull(
            collection,
            GlobalRandom.INSTANCE
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E randomOrNull(List<? extends E> list) {
        return randomOrNull(
            list,
            GlobalRandom.INSTANCE
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E randomOrNull(
        Collection<? extends E> collection,
        Random random
    ) {
        requireNotNull(collection);
        requireNotNull(random);
        if (collection.isEmpty()) {
            return null;
        }
        return elementAt(
            collection,
            random.nextInt(collection.size())
        );
    }

    @Contract(pure = true)
    public static <E> @Nullable E randomOrNull(
        List<? extends E> list,
        Random random
    ) {
        requireNotNull(list);
        requireNotNull(random);
        if (list.isEmpty()) {
            return null;
        }
        return elementAt(
            list,
            random.nextInt(list.size())
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduce(
        Iterable<? extends E> iterable,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        return reduce(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    private static <R, E extends R> R reduce(
        Iterator<? extends E> iterator,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        R result = iterator.next();
        while (iterator.hasNext()) {
            result = operation.apply(result, iterator.next());
        }
        return result;
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceIndexed(
        Iterable<? extends E> iterable,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        return reduceIndexed(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    private static <R, E extends R> R reduceIndexed(
        Iterator<? extends E> iterator,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        R result = iterator.next();
        int index = 1;
        while (iterator.hasNext()) {
            result = operation.apply(index, result, iterator.next());
            ++index;
        }
        return result;
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceIndexedOrNull(
        Iterable<? extends E> iterable,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            return null;
        }
        return reduceIndexed(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceIndexedOrNull(
        Collection<? extends E> collection,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(collection);
        requireNotNull(operation);
        if (collection.isEmpty()) {
            return null;
        }
        return reduceIndexedOrNull(
            (Iterable<? extends E>) collection,
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceOrNull(
        Iterable<? extends E> iterable,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            return null;
        }
        return reduce(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceOrNull(
        Collection<? extends E> collection,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(collection);
        requireNotNull(operation);
        if (collection.isEmpty()) {
            return null;
        }
        return reduceOrNull(
            (Iterable<? extends E>) collection,
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceRight(
        Iterable<? extends E> iterable,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRight(
            toList(iterable),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceRight(
        Collection<? extends E> collection,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRight(
            toList(collection),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceRight(
        List<? extends E> list,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(list);
        requireNotNull(operation);
        final ListIterator<? extends E> iterator = list.listIterator(list.size());
        if (!iterator.hasPrevious()) {
            throw new NoSuchElementException();
        }
        return reduceRight(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    private static <R, E extends R> R reduceRight(
        ListIterator<? extends E> iterator,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        R result = iterator.previous();
        while (iterator.hasPrevious()) {
            result = operation.apply(result, iterator.previous());
        }
        return result;
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceRightIndexed(
        Iterable<? extends E> iterable,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRightIndexed(
            toList(iterable),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceRightIndexed(
        Collection<? extends E> collection,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRightIndexed(
            toList(collection),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> R reduceRightIndexed(
        List<? extends E> list,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(list);
        requireNotNull(operation);
        final ListIterator<? extends E> iterator = list.listIterator(list.size());
        if (!iterator.hasPrevious()) {
            throw new NoSuchElementException();
        }
        return reduceRightIndexed(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    private static <R, E extends R> R reduceRightIndexed(
        ListIterator<? extends E> iterator,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        R result = iterator.previous();
        while (iterator.hasPrevious()) {
            result = operation.apply(iterator.previousIndex(), result, iterator.previous());
        }
        return result;
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceRightIndexedOrNull(
        Iterable<? extends E> iterable,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRightIndexedOrNull(
            toList(iterable),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceRightIndexedOrNull(
        Collection<? extends E> collection,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRightIndexedOrNull(
            toList(collection),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceRightIndexedOrNull(
        List<? extends E> list,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(list);
        requireNotNull(operation);
        final ListIterator<? extends E> iterator = list.listIterator(list.size());
        if (!iterator.hasPrevious()) {
            return null;
        }
        return reduceRightIndexed(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceRightOrNull(
        Iterable<? extends E> iterable,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRightOrNull(
            toList(iterable),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceRightOrNull(
        Collection<? extends E> collection,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        return reduceRightOrNull(
            toList(collection),
            operation
        );
    }

    @Contract(pure = true)
    public static <R, E extends R> @Nullable R reduceRightOrNull(
        List<? extends E> list,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(list);
        requireNotNull(operation);
        final ListIterator<? extends E> iterator = list.listIterator(list.size());
        if (!iterator.hasPrevious()) {
            return null;
        }
        return reduceRight(
            iterator,
            operation
        );
    }

    @Contract(pure = true)
    public static <E> List<E> reversed(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        return reversed(toArrayList(iterable));
    }

    @Contract(pure = true)
    public static <E> List<E> reversed(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptyList();
        }
        final List<E> result = toArrayList(collection);
        ModifiableCollections.reverse(result);
        return result;
    }

    @Contract(pure = true)
    public static <E, R> List<R> runningFold(
        Iterable<? extends E> iterable,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        return runningFoldTo(
            iterable,
            new ArrayList<>(),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> runningFold(
        Collection<? extends E> collection,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(collection);
        requireNotNull(operation);
        if (collection.isEmpty()) {
            return singletonList(initial);
        }
        return runningFoldTo(
            collection,
            new ArrayList<>(1 + collection.size()),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> runningFoldIndexed(
        Iterable<? extends E> iterable,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        return runningFoldIndexedTo(
            iterable,
            new ArrayList<>(),
            initial,
            operation
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> runningFoldIndexed(
        Collection<? extends E> collection,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(collection);
        requireNotNull(operation);
        if (collection.isEmpty()) {
            return singletonList(initial);
        }
        return runningFoldIndexedTo(
            collection,
            new ArrayList<>(1 + collection.size()),
            initial,
            operation
        );
    }

    @Contract("_, _, _, _ -> param2")
    private static <E, R, C extends Collection<R>> C runningFoldIndexedTo(
        Iterable<? extends E> iterable,
        C destination,
        R initial,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        destination.add(initial);
        R previous = initial;
        int index = 0;
        for (final E element : iterable) {
            previous = operation.apply(index, previous, element);
            destination.add(previous);
            ++index;
        }
        return destination;
    }

    @Contract("_, _, _, _ -> param2")
    private static <E, R, C extends Collection<R>> C runningFoldTo(
        Iterable<? extends E> iterable,
        C destination,
        R initial,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        destination.add(initial);
        R previous = initial;
        for (final E element : iterable) {
            previous = operation.apply(previous, element);
            destination.add(previous);
        }
        return destination;
    }

    @Contract(pure = true)
    public static <R, E extends R> List<R> runningReduce(
        Iterable<? extends E> iterable,
        BiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        final List<R> result = new ArrayList<>();
        R previous = iterator.next();
        result.add(previous);
        while (iterator.hasNext()) {
            final E element = iterator.next();
            previous = operation.apply(previous, element);
            result.add(previous);
        }
        return result;
    }

    @Contract(pure = true)
    public static <R, E extends R> List<R> runningReduceIndexed(
        Iterable<? extends E> iterable,
        IndexedBiFunction<? super R, ? super E, ? extends R> operation
    ) {
        requireNotNull(iterable);
        requireNotNull(operation);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        final List<R> result = new ArrayList<>();
        R previous = iterator.next();
        result.add(previous);
        int index = 1;
        while (iterator.hasNext()) {
            final E element = iterator.next();
            previous = operation.apply(index, previous, element);
            result.add(previous);
            ++index;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> shuffled(Iterable<? extends E> iterable) {
        return shuffled(
            iterable,
            GlobalRandom.INSTANCE
        );
    }

    @Contract(pure = true)
    public static <E> List<E> shuffled(Collection<? extends E> collection) {
        return shuffled(
            collection,
            GlobalRandom.INSTANCE
        );
    }

    @Contract(pure = true)
    public static <E> List<E> shuffled(
        Iterable<? extends E> iterable,
        Random random
    ) {
        final List<E> result = toArrayList(iterable);
        ModifiableCollections.shuffle(result, random);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> shuffled(
        Collection<? extends E> collection,
        Random random
    ) {
        requireNotNull(collection);
        requireNotNull(random);
        if (collection.isEmpty()) {
            return emptyList();
        }
        final List<E> result = toArrayList(collection);
        ModifiableCollections.shuffle(result, random);
        return result;
    }

    @Contract(pure = true)
    public static <E> E single(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            throw new NoSuchElementException();
        }
        final E result = iterator.next();
        if (iterator.hasNext()) {
            throw new IllegalArgumentException();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> E single(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            throw new NoSuchElementException();
        }
        if (collection.size() > 1) {
            throw new IllegalArgumentException();
        }
        return collection.iterator().next();
    }

    @Contract(pure = true)
    public static <E> E single(List<? extends E> list) {
        requireNotNull(list);
        if (list.isEmpty()) {
            throw new NoSuchElementException();
        }
        if (list.size() > 1) {
            throw new IllegalArgumentException();
        }
        return list.get(0);
    }

    @Contract(pure = true)
    public static <E> @Nullable E singleOrNull(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final Iterator<? extends E> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            return null;
        }
        final E result = iterator.next();
        if (iterator.hasNext()) {
            return null;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> @Nullable E singleOrNull(Collection<? extends E> collection) {
        requireNotNull(collection);
        return collection.size() == 1 ? collection.iterator().next() : null;
    }

    @Contract(pure = true)
    public static <E> @Nullable E singleOrNull(List<? extends E> list) {
        requireNotNull(list);
        return list.size() == 1 ? list.get(0) : null;
    }

    @Contract(pure = true)
    public static int size(Iterable<?> iterable) {
        requireNotNull(iterable);
        int result = 0;
        for (final @SuppressWarnings("unused") var element : iterable) {
            ++result;
        }
        return result;
    }

    @Contract(pure = true)
    public static int size(Collection<?> collection) {
        requireNotNull(collection);
        return collection.size();
    }

    @Contract(pure = true)
    public static <E extends Comparable<? super E>> List<E> sorted(Iterable<? extends E> iterable) {
        final List<E> result = toArrayList(iterable);
        ModifiableCollections.sort(result);
        return result;
    }

    @Contract(pure = true)
    public static <E extends Comparable<? super E>> List<E> sorted(Collection<? extends E> collection) {
        final List<E> result = toArrayList(collection);
        ModifiableCollections.sort(result);
        return result;
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<? super R>> List<E> sortedBy(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        return sortedWith(
            iterable,
            Comparator.comparing(selector)
        );
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<? super R>> List<E> sortedBy(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(selector);
        return sortedWith(
            collection,
            Comparator.comparing(selector)
        );
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<? super R>> List<E> sortedByDescending(
        Iterable<? extends E> iterable,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(iterable);
        requireNotNull(selector);
        return sortedWith(
            iterable,
            Comparator.comparing(selector).reversed()
        );
    }

    @Contract(pure = true)
    public static <E, R extends Comparable<? super R>> List<E> sortedByDescending(
        Collection<? extends E> collection,
        Function<? super E, ? extends R> selector
    ) {
        requireNotNull(collection);
        requireNotNull(selector);
        return sortedWith(
            collection,
            Comparator.comparing(selector).reversed()
        );
    }

    @Contract(pure = true)
    public static <E extends Comparable<? super E>> List<E> sortedDescending(Iterable<? extends E> iterable) {
        return sortedWith(
            iterable,
            Comparator.reverseOrder()
        );
    }

    @Contract(pure = true)
    public static <E extends Comparable<? super E>> List<E> sortedDescending(Collection<? extends E> collection) {
        return sortedWith(
            collection,
            Comparator.reverseOrder()
        );
    }

    @Contract(pure = true)
    public static <E> List<E> sortedWith(
        Iterable<? extends E> iterable,
        Comparator<? super E> comparator
    ) {
        final List<E> result = toArrayList(iterable);
        ModifiableCollections.sortWith(result, comparator);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> sortedWith(
        Collection<? extends E> collection,
        Comparator<? super E> comparator
    ) {
        final List<E> result = toArrayList(collection);
        ModifiableCollections.sortWith(result, comparator);
        return result;
    }

    @Contract(pure = true)
    public static <E> Set<E> subtract(
        Iterable<? extends E> iterable,
        Iterable<?> other
    ) {
        return subtract(
            toSet(iterable),
            other
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> subtract(
        Iterable<? extends E> iterable,
        Collection<?> other
    ) {
        return subtract(
            toSet(iterable),
            other
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> subtract(
        Collection<? extends E> collection,
        Iterable<?> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        final Set<E> result = toHashSet(collection);
        ModifiableCollections.removeAll(result, other);
        return result;
    }

    @Contract(pure = true)
    public static <E> Set<E> subtract(
        Collection<? extends E> collection,
        Collection<?> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        final Set<E> result = toHashSet(collection);
        ModifiableCollections.removeAll(result, other);
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> take(
        Iterable<? extends E> iterable,
        int n
    ) {
        requireNotNull(iterable);
        requireGe0(n);
        if (n == 0) {
            return emptyList();
        }
        final List<E> result = new ArrayList<>(n);
        int count = 0;
        final Iterator<? extends E> iterator = iterable.iterator();
        while (count < n && iterator.hasNext()) {
            result.add(iterator.next());
            ++count;
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> take(
        Collection<? extends E> collection,
        int n
    ) {
        requireNotNull(collection);
        requireGe0(n);
        if (n == 0 || collection.isEmpty()) {
            return emptyList();
        }
        return take(
            (Iterable<? extends E>) collection,
            n
        );
    }

    @Contract(pure = true)
    public static <E> List<E> takeLast(
        Iterable<? extends E> iterable,
        int n
    ) {
        requireNotNull(iterable);
        requireGe0(n);
        if (n == 0) {
            return emptyList();
        }
        return takeLast(
            toList(iterable),
            n
        );
    }

    @Contract(pure = true)
    public static <E> List<E> takeLast(
        Collection<? extends E> collection,
        int n
    ) {
        requireNotNull(collection);
        requireGe0(n);
        if (n == 0 || collection.isEmpty()) {
            return emptyList();
        }
        return takeLast(
            toList(collection),
            n
        );
    }

    @Contract(pure = true)
    public static <E> List<E> takeLast(
        List<? extends E> list,
        int n
    ) {
        requireNotNull(list);
        requireGe0(n);
        if (n == 0 || list.isEmpty()) {
            return emptyList();
        }
        if (n >= list.size()) {
            return toList(list);
        }
        return Iterators.toList(list.listIterator(list.size() - n));
    }

    @Contract(pure = true)
    public static <E> List<E> takeLastWhile(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        return takeLastWhile(
            toList(iterable),
            predicate
        );
    }

    @Contract(pure = true)
    public static <E> List<E> takeLastWhile(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return takeLastWhile(
            toList(collection),
            predicate
        );
    }

    @Contract(pure = true)
    @SuppressWarnings("JdkObsolete")
    public static <E> List<E> takeLastWhile(
        List<? extends E> list,
        Predicate<? super E> predicate
    ) {
        requireNotNull(list);
        requireNotNull(predicate);
        if (list.isEmpty()) {
            return emptyList();
        }
        List<E> result = null;
        for (final ListIterator<? extends E> iterator = list.listIterator(list.size()); iterator.hasPrevious(); ) {
            final E element = iterator.previous();
            if (!predicate.test(element)) {
                break;
            }
            if (result == null) {
                result = new LinkedList<>();
            }
            result.add(0, element);
        }
        if (result == null) {
            return emptyList();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> takeWhile(
        Iterable<? extends E> iterable,
        Predicate<? super E> predicate
    ) {
        requireNotNull(iterable);
        requireNotNull(predicate);
        List<E> result = null;
        for (final E element : iterable) {
            if (!predicate.test(element)) {
                break;
            }
            if (result == null) {
                result = new ArrayList<>();
            }
            result.add(element);
        }
        if (result == null) {
            return emptyList();
        }
        return result;
    }

    @Contract(pure = true)
    public static <E> List<E> takeWhile(
        Collection<? extends E> collection,
        Predicate<? super E> predicate
    ) {
        requireNotNull(collection);
        requireNotNull(predicate);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return takeWhile(
            (Iterable<? extends E>) collection,
            predicate
        );
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> ArrayList<E> toArrayList(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final ArrayList<E> result = new ArrayList<>();
        ModifiableCollections.addAll(result, iterable);
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> ArrayList<E> toArrayList(Collection<? extends E> collection) {
        requireNotNull(collection);
        return new ArrayList<>(collection);
    }

    @Contract(value = "_ -> new", pure = true)
    public static boolean[] toBooleanArray(Iterable<Boolean> iterable) {
        return toBooleanArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static boolean[] toBooleanArray(Collection<Boolean> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_BOOLEAN_ARRAY;
        }
        return Arrays.toBooleanArray(collection.toArray(Boolean[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    public static byte[] toByteArray(Iterable<Byte> iterable) {
        return toByteArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static byte[] toByteArray(Collection<Byte> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_BYTE_ARRAY;
        }
        return Arrays.toByteArray(collection.toArray(Byte[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    public static char[] toCharArray(Iterable<Character> iterable) {
        return toCharArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static char[] toCharArray(Collection<Character> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_CHAR_ARRAY;
        }
        return Arrays.toCharArray(collection.toArray(Character[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    public static double[] toDoubleArray(Iterable<Double> iterable) {
        return toDoubleArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static double[] toDoubleArray(Collection<Double> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_DOUBLE_ARRAY;
        }
        return Arrays.toDoubleArray(collection.toArray(Double[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    public static float[] toFloatArray(Iterable<Float> iterable) {
        return toFloatArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static float[] toFloatArray(Collection<Float> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_FLOAT_ARRAY;
        }
        return Arrays.toFloatArray(collection.toArray(Float[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> HashSet<E> toHashSet(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final HashSet<E> result = new HashSet<>();
        ModifiableCollections.addAll(result, iterable);
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> HashSet<E> toHashSet(Collection<? extends E> collection) {
        requireNotNull(collection);
        return new HashSet<>(collection);
    }

    @Contract(value = "_ -> new", pure = true)
    public static int[] toIntArray(Iterable<Integer> iterable) {
        return toIntArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static int[] toIntArray(Collection<Integer> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_INT_ARRAY;
        }
        return Arrays.toIntArray(collection.toArray(Integer[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> LinkedHashSet<E> toLinkedHashSet(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final LinkedHashSet<E> result = new LinkedHashSet<>();
        ModifiableCollections.addAll(result, iterable);
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> LinkedHashSet<E> toLinkedHashSet(Collection<? extends E> collection) {
        requireNotNull(collection);
        return new LinkedHashSet<>(collection);
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings({"NonApiType", "JdkObsolete"})
    public static <E> LinkedList<E> toLinkedList(Iterable<? extends E> iterable) {
        requireNotNull(iterable);
        final LinkedList<E> result = new LinkedList<>();
        ModifiableCollections.addAll(result, iterable);
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings({"NonApiType", "JdkObsolete"})
    public static <E> LinkedList<E> toLinkedList(Collection<? extends E> collection) {
        requireNotNull(collection);
        return new LinkedList<>(collection);
    }

    @Contract(pure = true)
    public static <E> List<E> toList(Iterable<? extends E> iterable) {
        return toArrayList(iterable);
    }

    @Contract(pure = true)
    public static <E> List<E> toList(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptyList();
        }
        return toArrayList(collection);
    }

    @Contract(value = "_ -> new", pure = true)
    public static long[] toLongArray(Iterable<Long> iterable) {
        return toLongArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static long[] toLongArray(Collection<Long> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_LONG_ARRAY;
        }
        return Arrays.toLongArray(collection.toArray(Long[]::new));
    }

    @Contract(pure = true)
    public static <K, V> Map<K, V> toMap(Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        return toMap(
            iterable,
            new HashMap<>()
        );
    }

    @Contract(pure = true)
    public static <K, V> Map<K, V> toMap(Collection<? extends Pair<? extends K, ? extends V>> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptyMap();
        }
        return toMap(
            collection,
            new HashMap<>(collection.size())
        );
    }

    @Contract("_, _ -> param2")
    public static <K, V, M extends Map<? super K, ? super V>> M toMap(
        Iterable<? extends Pair<? extends K, ? extends V>> iterable,
        M destination
    ) {
        requireNotNull(iterable);
        requireNotNull(destination);
        for (final Pair<? extends K, ? extends V> element : iterable) {
            destination.put(element.first(), element.second());
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> Set<E> toSet(Iterable<? extends E> iterable) {
        return toHashSet(iterable);
    }

    @Contract(pure = true)
    public static <E> Set<E> toSet(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptySet();
        }
        return toHashSet(collection);
    }

    @Contract(value = "_ -> new", pure = true)
    public static short[] toShortArray(Iterable<Short> iterable) {
        return toShortArray(toList(iterable));
    }

    @Contract(value = "_ -> new", pure = true)
    public static short[] toShortArray(Collection<Short> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return Arrays.EMPTY_SHORT_ARRAY;
        }
        return Arrays.toShortArray(collection.toArray(Short[]::new));
    }

    @Contract(value = "_ -> new", pure = true)
    public static <E> SortedSet<E> toSortedSet(Iterable<? extends E> iterable) {
        return toTreeSet(iterable);
    }

    @Contract(value = "_ -> new", pure = true)
    public static <E> SortedSet<E> toSortedSet(Collection<? extends E> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return emptySortedSet();
        }
        return toTreeSet(collection);
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> TreeSet<E> toTreeSet(Iterable<? extends E> iterable) {
        final TreeSet<E> result = new TreeSet<>();
        ModifiableCollections.addAll(result, iterable);
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static <E> TreeSet<E> toTreeSet(Collection<? extends E> collection) {
        requireNotNull(collection);
        return new TreeSet<>(collection);
    }

    @Contract(pure = true)
    public static <E> Set<E> union(
        Iterable<? extends E> iterable,
        Iterable<? extends E> other
    ) {
        return union(
            toSet(iterable),
            toSet(other)
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> union(
        Iterable<? extends E> iterable,
        Collection<? extends E> other
    ) {
        return union(
            toSet(iterable),
            other
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> union(
        Collection<? extends E> collection,
        Iterable<? extends E> other
    ) {
        return union(
            collection,
            toSet(other)
        );
    }

    @Contract(pure = true)
    public static <E> Set<E> union(
        Collection<? extends E> collection,
        Collection<? extends E> other
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        if (collection.isEmpty() && other.isEmpty()) {
            return emptySet();
        }
        final Set<E> result = new HashSet<>(collection.size() + other.size());
        result.addAll(collection);
        result.addAll(other);
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    public static <E, F> Pair<List<E>, List<F>> unzip(Iterable<Pair<E, F>> iterable) {
        requireNotNull(iterable);
        return unzipTo(
            iterable,
            new ArrayList<>(),
            new ArrayList<>()
        );
    }

    @Contract(value = "_ -> new", pure = true)
    public static <E, F> Pair<List<E>, List<F>> unzip(Collection<Pair<E, F>> collection) {
        requireNotNull(collection);
        if (collection.isEmpty()) {
            return new Pair<>(emptyList(), emptyList());
        }
        final int size = collection.size();
        return unzipTo(
            collection,
            new ArrayList<>(size),
            new ArrayList<>(size)
        );
    }

    @Contract("_, _, _ -> new")
    private static <E, F> Pair<List<E>, List<F>> unzipTo(
        Iterable<Pair<E, F>> iterable,
        List<E> list1,
        List<F> list2
    ) {
        for (final Pair<E, F> element : iterable) {
            list1.add(element.first());
            list2.add(element.second());
        }
        return new Pair<>(list1, list2);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <E> List<List<E>> windowed(
        Iterable<? extends E> iterable,
        Windowed config
    ) {
        return windowed(
            iterable,
            config,
            Function.identity()
        );
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public static <E, R> List<R> windowed(
        Iterable<? extends E> iterable,
        Windowed config,
        Function<? super List<E>, ? extends R> transform
    ) {
        return windowed(
            toList(iterable),
            config,
            transform
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <E> List<List<E>> windowed(
        Collection<? extends E> collection,
        Windowed config
    ) {
        return windowed(
            collection,
            config,
            Function.identity()
        );
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public static <E, R> List<R> windowed(
        Collection<? extends E> collection,
        Windowed config,
        Function<? super List<E>, ? extends R> transform
    ) {
        return windowed(
            toList(collection),
            config,
            transform
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <E> List<List<E>> windowed(
        List<? extends E> list,
        Windowed config
    ) {
        return windowed(
            list,
            config,
            Function.identity()
        );
    }

    @Contract(value = "_, _, _ -> new", pure = true)
    public static <E, R> List<R> windowed(
        List<? extends E> list,
        Windowed config,
        Function<? super List<E>, ? extends R> transform
    ) {
        requireNotNull(list);
        requireNotNull(config);
        requireNotNull(transform);
        if (list.isEmpty()) {
            return emptyList();
        }
        final List<R> result = new ArrayList<>();
        final int size = list.size();
        final int minRemaining = config.partialWindows ? 1 : config.size;
        int firstIndex = 0;
        while (size - firstIndex >= minRemaining) {
            final List<E> window = new ArrayList<>(config.size);
            final Iterator<? extends E> iterator = list.listIterator(firstIndex);
            for (int i = 0; i < config.size && iterator.hasNext(); ++i) {
                window.add(iterator.next());
            }
            result.add(transform.apply(window));
            firstIndex += config.step;
        }
        return result;
    }

    @Contract(value = "_ -> new", pure = true)
    public static <E> Iterable<IndexedValue<E>> withIndex(Iterable<E> iterable) {
        requireNotNull(iterable);
        return () -> new IndexedIterator<>(iterable.iterator());
    }

    @Contract(pure = true)
    public static <E, F> List<Pair<E, F>> zip(
        Iterable<? extends E> iterable,
        Iterable<? extends F> other
    ) {
        return zip(
            iterable,
            other,
            Pair::new
        );
    }

    @Contract(pure = true)
    public static <E, F> List<Pair<E, F>> zip(
        Collection<? extends E> collection,
        Collection<? extends F> other
    ) {
        return zip(
            collection,
            other,
            Pair::new
        );
    }

    @Contract(pure = true)
    public static <E, F, R> List<R> zip(
        Iterable<? extends E> iterable,
        Iterable<? extends F> other,
        BiFunction<? super E, ? super F, R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(other);
        requireNotNull(transform);
        return zipTo(
            iterable,
            other,
            transform,
            new ArrayList<>()
        );
    }

    @Contract(pure = true)
    public static <E, F, R> List<R> zip(
        Collection<? extends E> collection,
        Collection<? extends F> other,
        BiFunction<? super E, ? super F, R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(other);
        requireNotNull(transform);
        if (collection.isEmpty() || other.isEmpty()) {
            return emptyList();
        }
        return zipTo(
            collection,
            other,
            transform,
            new ArrayList<>(min(collection.size(), other.size()))
        );
    }

    @Contract("_, _, _, _ -> param4")
    private static <E, F, R, C extends Collection<R>> C zipTo(
        Iterable<? extends E> iterable,
        Iterable<? extends F> other,
        BiFunction<? super E, ? super F, R> transform,
        C destination
    ) {
        final Iterator<? extends E> iterator = iterable.iterator();
        final Iterator<? extends F> otherIterator = other.iterator();
        while (iterator.hasNext() && otherIterator.hasNext()) {
            destination.add(transform.apply(iterator.next(), otherIterator.next()));
        }
        return destination;
    }

    @Contract(pure = true)
    public static <E> List<Pair<E, E>> zipWithNext(Iterable<? extends E> iterable) {
        return zipWithNext(
            iterable,
            Pair::new
        );
    }

    @Contract(pure = true)
    public static <E> List<Pair<E, E>> zipWithNext(Collection<? extends E> collection) {
        return zipWithNext(
            collection,
            Pair::new
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> zipWithNext(
        Iterable<? extends E> iterable,
        BiFunction<? super E, ? super E, R> transform
    ) {
        requireNotNull(iterable);
        requireNotNull(transform);
        return zipWithNextTo(
            iterable,
            transform,
            new ArrayList<>()
        );
    }

    @Contract(pure = true)
    public static <E, R> List<R> zipWithNext(
        Collection<? extends E> collection,
        BiFunction<? super E, ? super E, R> transform
    ) {
        requireNotNull(collection);
        requireNotNull(transform);
        final int size = collection.size();
        if (size <= 1) {
            return emptyList();
        }
        return zipWithNextTo(
            collection,
            transform,
            new ArrayList<>(size - 1)
        );
    }

    @Contract("_, _, _ -> param3")
    private static <E, R, C extends Collection<R>> C zipWithNextTo(
        Iterable<? extends E> iterable,
        BiFunction<? super E, ? super E, R> transform,
        C destination
    ) {
        final Iterator<? extends E> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            E previous = iterator.next();
            while (iterator.hasNext()) {
                final E element = iterator.next();
                destination.add(transform.apply(previous, element));
                previous = element;
            }
        }
        return destination;
    }

    private Collections() {}
}
