/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySortedMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.function.BiFunction;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link Map}s and {@link SortedMap}s.
 *
 * @author Laurent Pireyn
 */
public final class Maps {
    @Contract(pure = true)
    public static <K> @Nullable K firstKeyOrNull(SortedMap<K, ?> map) {
        requireNotNull(map);
        return !map.isEmpty() ? map.firstKey() : null;
    }

    @Contract(pure = true)
    public static boolean isNullOrEmpty(@Nullable Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    @Contract(pure = true)
    public static <K> @Nullable K lastKeyOrNull(SortedMap<K, ?> map) {
        requireNotNull(map);
        return !map.isEmpty() ? map.lastKey() : null;
    }

    @Contract(pure = true)
    public static <K, V, R> Map<R, V> mapKeys(
        Map<? extends K, ? extends V> map,
        BiFunction<? super K, ? super V, ? extends R> transform
    ) {
        requireNotNull(map);
        requireNotNull(transform);
        return mapKeysTo(
            map,
            new HashMap<>(map.size()),
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <K, V, R, M extends Map<? super R, ? super V>> M mapKeysTo(
        Map<? extends K, V> map,
        M destination,
        BiFunction<? super K, ? super V, ? extends R> transform
    ) {
        requireNotNull(map);
        requireNotNull(destination);
        requireNotNull(transform);
        for (final Map.Entry<? extends K, V> entry : map.entrySet()) {
            final V value = entry.getValue();
            destination.put(transform.apply(entry.getKey(), value), value);
        }
        return destination;
    }

    @Contract(pure = true)
    public static <K, V, R> Map<K, R> mapValues(
        Map<? extends K, ? extends V> map,
        BiFunction<? super K, ? super V, ? extends R> transform
    ) {
        requireNotNull(map);
        requireNotNull(transform);
        return mapValuesTo(
            map,
            new HashMap<>(map.size()),
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <K, V, R, M extends Map<? super K, ? super R>> M mapValuesTo(
        Map<K, ? extends V> map,
        M destination,
        BiFunction<? super K, ? super V, ? extends R> transform
    ) {
        requireNotNull(map);
        requireNotNull(destination);
        requireNotNull(transform);
        for (final Map.Entry<K, ? extends V> entry : map.entrySet()) {
            final K key = entry.getKey();
            destination.put(key, transform.apply(key, entry.getValue()));
        }
        return destination;
    }

    @Contract(pure = true)
    public static <K, V> Map<K, V> orEmpty(@Nullable Map<K, V> map) {
        return map != null ? map : emptyMap();
    }

    @Contract(pure = true)
    public static <K, V> SortedMap<K, V> orEmpty(@Nullable SortedMap<K, V> map) {
        return map != null ? map : emptySortedMap();
    }

    @Contract(value = "_ -> new", pure = true)
    public static Properties toProperties(Map<String, String> map) {
        requireNotNull(map);
        final Properties result = new Properties(map.size());
        result.putAll(map);
        return result;
    }

    private Maps() {}
}
