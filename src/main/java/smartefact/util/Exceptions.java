/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireNoNullElements;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Utilities for {@link Exception}s, {@link Error}s and {@link Throwable}s.
 *
 * @author Laurent Pireyn
 */
public final class Exceptions {
    @Contract("_, _ -> param1")
    public static <T extends Throwable> T addSuppressed(
        T throwable,
        Iterable<? extends Throwable> exceptions
    ) {
        requireNotNull(throwable);
        requireNoNullElements(exceptions);
        for (final var exception : exceptions) {
            throwable.addSuppressed(exception);
        }
        return throwable;
    }

    /**
     * Returns the chain of causes of the given {@link Throwable}.
     * <p>
     * The chain of cause always starts with the throwable itself.
     * </p>
     *
     * @param throwable the throwable
     * @return the chain of causes of {@code throwable} (never empty)
     */
    public static Iterable<Throwable> causeChain(Throwable throwable) {
        requireNotNull(throwable);
        return () -> new Iterator<>() {
            private @Nullable Throwable next = throwable;

            @Override
            public boolean hasNext() {
                return next != null;
            }

            @Override
            public Throwable next() {
                if (next == null) {
                    throw new NoSuchElementException();
                }
                final Throwable result = next;
                next = next.getCause();
                return result;
            }
        };
    }

    /**
     * Returns the root cause of the given {@link Throwable}.
     *
     * @param throwable the throwable
     * @return the root cause of {@code throwable} (possibly {@code throwable} itself)
     */
    public static Throwable rootCause(Throwable throwable) {
        return Collections.last(causeChain(throwable));
    }

    /**
     * Throws the given {@link Throwable} if it is an {@link Error},
     * or returns it otherwise.
     *
     * @param throwable the throwable
     * @param <T> the type of {@code throwable}
     * @return {@code throwable} if it is an {@link Error}
     */
    public static <T extends Throwable> T throwIfError(T throwable) {
        requireNotNull(throwable);
        if (throwable instanceof Error) {
            throw (Error) throwable;
        }
        return throwable;
    }

    /**
     * Throws the given {@link Throwable} if it is either an {@link Error} or a {@link RuntimeException},
     * or returns it as an {@link Exception} otherwise.
     *
     * @param throwable the throwable
     * @return {@code throwable} if it is either an {@code Error} or a {@code RuntimeException}
     * @throws IllegalArgumentException if {@code throwable} is not an {@code Error} nor an {@code Exception}
     * @see #throwIfError(Throwable)
     * @see #throwIfRuntimeException(Throwable)
     * @see #throwIfUnchecked(Throwable)
     */
    public static Exception throwIfNotCheckedException(Throwable throwable) {
        throwIfUnchecked(throwable);
        require(
            throwable instanceof Exception,
            () -> throwable.getClass().getName() + " is not an Error nor an Exception"
        );
        return (Exception) throwable;
    }

    /**
     * Throws the given {@link Throwable} if it is a {@link RuntimeException}, or returns it otherwise.
     *
     * @param throwable the throwable
     * @param <T> the type of {@code throwable}
     * @return {@code throwable} if it is a {@link RuntimeException}
     */
    public static <T extends Throwable> T throwIfRuntimeException(T throwable) {
        requireNotNull(throwable);
        if (throwable instanceof RuntimeException) {
            throw (RuntimeException) throwable;
        }
        return throwable;
    }

    /**
     * Throws the given {@link Throwable} if it is unchecked, or returns it if it is checked.
     *
     * @param throwable the throwable
     * @param <T> the type of {@code throwable}
     * @return {@code throwable} if it is checked
     * @see #throwIfError(Throwable)
     * @see #throwIfRuntimeException(Throwable)
     * @see #throwIfNotCheckedException(Throwable)
     */
    public static <T extends Throwable> T throwIfUnchecked(T throwable) {
        return throwIfRuntimeException(throwIfError(throwable));
    }

    private Exceptions() {}
}
