/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Iterator;

/**
 * {@link Iterator} on {@link IndexedValue}s.
 *
 * @author Laurent Pireyn
 */
final class IndexedIterator<E> implements Iterator<IndexedValue<E>> {
    private final Iterator<? extends E> iterator;
    private int index;

    IndexedIterator(Iterator<? extends E> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public IndexedValue<E> next() {
        final IndexedValue<E> result = new IndexedValue<>(index, iterator.next());
        ++index;
        return result;
    }

    @Override
    public void remove() {
        iterator.remove();
        --index;
    }
}
