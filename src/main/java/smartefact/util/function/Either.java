/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import smartefact.util.Objects;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Value that is either a <em>left</em> or a <em>right</em> value.
 * <p>
 * An {@code Either} value is typically used to represent a success or a failure:
 * a failure is a left value, while a success is a right value.
 * </p>
 * <p>
 * This class is somewhat similar to {@link Optional},
 * but it supports {@code null} as either value.
 * </p>
 *
 * @param <L> the type of the left value
 * @param <R> the type of the right value
 * @author Laurent Pireyn
 */
public abstract class Either<L, R> {
    private static final class Left<L, R> extends Either<L, R> {
        private final L value;

        Left(L value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public L getLeft() {
            return value;
        }

        @Override
        public R getRight() {
            throw new NoSuchElementException("Left value");
        }

        @Override
        public L getLeftOrElse(L defaultValue) {
            return value;
        }

        @Override
        public R getRightOrElse(R defaultValue) {
            return defaultValue;
        }

        @Override
        public L getLeftOrElseGet(Supplier<? extends L> defaultValue) {
            requireNotNull(defaultValue);
            return value;
        }

        @Override
        public R getRightOrElseGet(Supplier<? extends R> defaultValue) {
            requireNotNull(defaultValue);
            return defaultValue.get();
        }

        @Override
        public <X extends Throwable> L getLeftOrElseThrow(Function<? super R, ? extends X> exceptionSupplier) throws X {
            requireNotNull(exceptionSupplier);
            return value;
        }

        @Override
        public <X extends Throwable> R getRightOrElseThrow(Function<? super L, ? extends X> exceptionSupplier) throws X {
            requireNotNull(exceptionSupplier);
            throw exceptionSupplier.apply(value);
        }

        @Override
        public Optional<L> getOptionalLeft() {
            return Optional.ofNullable(value);
        }

        @Override
        public Optional<R> getOptionalRight() {
            return Optional.empty();
        }

        @Override
        public <T> Either<T, R> mapLeft(Function<? super L, ? extends T> transform) {
            requireNotNull(transform);
            return left(transform.apply(value));
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Either<L, T> mapRight(Function<? super R, ? extends T> transform) {
            requireNotNull(transform);
            return (Either<L, T>) this;
        }

        @Override
        public <T> Either<T, R> flatMapLeft(Function<? super L, ? extends Either<T, R>> transform) {
            requireNotNull(transform);
            return transform.apply(value);
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Either<L, T> flatMapRight(Function<? super R, ? extends Either<L, T>> transform) {
            requireNotNull(transform);
            return (Either<L, T>) this;
        }

        @Override
        public Either<L, R> ifLeft(Consumer<? super L> action) {
            requireNotNull(action);
            action.accept(value);
            return this;
        }

        @Override
        public Either<L, R> ifRight(Consumer<? super R> action) {
            requireNotNull(action);
            return this;
        }

        @Override
        public Either<L, R> ifLeftOrElse(Consumer<? super L> action, Runnable rightAction) {
            requireNotNull(action);
            requireNotNull(rightAction);
            action.accept(value);
            return this;
        }

        @Override
        public Either<L, R> ifRightOrElse(Consumer<? super R> action, Runnable leftAction) {
            requireNotNull(action);
            requireNotNull(leftAction);
            leftAction.run();
            return this;
        }

        @Override
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Left)) {
                return false;
            }
            final Left<?, ?> other = (Left<?, ?>) object;
            return Objects.equals(value, other.value);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(value);
        }

        @Override
        public String toString() {
            return "Left[" + value + ']';
        }
    }

    private static final class Right<L, R> extends Either<L, R> {
        private final R value;

        Right(R value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public boolean isRight() {
            return true;
        }

        @Override
        public L getLeft() {
            throw new NoSuchElementException("Right value");
        }

        @Override
        public R getRight() {
            return value;
        }

        @Override
        public L getLeftOrElse(L defaultValue) {
            return defaultValue;
        }

        @Override
        public R getRightOrElse(R defaultValue) {
            return value;
        }

        @Override
        public L getLeftOrElseGet(Supplier<? extends L> defaultValue) {
            requireNotNull(defaultValue);
            return defaultValue.get();
        }

        @Override
        public R getRightOrElseGet(Supplier<? extends R> defaultValue) {
            requireNotNull(defaultValue);
            return value;
        }

        @Override
        public <X extends Throwable> L getLeftOrElseThrow(Function<? super R, ? extends X> exceptionSupplier) throws X {
            requireNotNull(exceptionSupplier);
            throw exceptionSupplier.apply(value);
        }

        @Override
        public <X extends Throwable> R getRightOrElseThrow(Function<? super L, ? extends X> exceptionSupplier) throws X {
            requireNotNull(exceptionSupplier);
            return value;
        }

        @Override
        public Optional<L> getOptionalLeft() {
            return Optional.empty();
        }

        @Override
        public Optional<R> getOptionalRight() {
            return Optional.ofNullable(value);
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Either<T, R> mapLeft(Function<? super L, ? extends T> transform) {
            requireNotNull(transform);
            return (Either<T, R>) this;
        }

        @Override
        public <T> Either<L, T> mapRight(Function<? super R, ? extends T> transform) {
            requireNotNull(transform);
            return right(transform.apply(value));
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> Either<T, R> flatMapLeft(Function<? super L, ? extends Either<T, R>> transform) {
            requireNotNull(transform);
            return (Either<T, R>) this;
        }

        @Override
        public <T> Either<L, T> flatMapRight(Function<? super R, ? extends Either<L, T>> transform) {
            requireNotNull(transform);
            return transform.apply(value);
        }

        @Override
        public Either<L, R> ifLeft(Consumer<? super L> action) {
            requireNotNull(action);
            return this;
        }

        @Override
        public Either<L, R> ifRight(Consumer<? super R> action) {
            requireNotNull(action);
            action.accept(value);
            return this;
        }

        @Override
        public Either<L, R> ifLeftOrElse(Consumer<? super L> action, Runnable rightAction) {
            requireNotNull(action);
            requireNotNull(rightAction);
            rightAction.run();
            return this;
        }

        @Override
        public Either<L, R> ifRightOrElse(Consumer<? super R> action, Runnable leftAction) {
            requireNotNull(action);
            requireNotNull(leftAction);
            action.accept(value);
            return this;
        }

        @Override
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Right)) {
                return false;
            }
            final Right<?, ?> other = (Right<?, ?>) object;
            return Objects.equals(value, other.value);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(value);
        }

        @Override
        public String toString() {
            return "Right[" + value + ']';
        }
    }

    public static <L, R> Either<L, R> left(L value) {
        return new Left<>(value);
    }

    public static <L, R> Either<L, R> right(R value) {
        return new Right<>(value);
    }

    private Either() {}

    @Contract(pure = true)
    public abstract boolean isLeft();

    @Contract(pure = true)
    public abstract boolean isRight();

    @Contract(pure = true)
    public abstract L getLeft();

    @Contract(pure = true)
    public abstract R getRight();

    @Contract(pure = true)
    public abstract L getLeftOrElse(L defaultValue);

    @Contract(pure = true)
    public abstract R getRightOrElse(R defaultValue);

    @Contract(pure = true)
    public abstract L getLeftOrElseGet(Supplier<? extends L> defaultValue);

    @Contract(pure = true)
    public abstract R getRightOrElseGet(Supplier<? extends R> defaultValue);

    @Contract(pure = true)
    public abstract <X extends Throwable> L getLeftOrElseThrow(Function<? super R, ? extends X> exceptionSupplier) throws X;

    @Contract(pure = true)
    public abstract <X extends Throwable> R getRightOrElseThrow(Function<? super L, ? extends X> exceptionSupplier) throws X;

    @Contract(pure = true)
    public abstract Optional<L> getOptionalLeft();

    @Contract(pure = true)
    public abstract Optional<R> getOptionalRight();

    @Contract(pure = true)
    public abstract <T> Either<T, R> mapLeft(Function<? super L, ? extends T> transform);

    @Contract(pure = true)
    public abstract <T> Either<L, T> mapRight(Function<? super R, ? extends T> transform);

    @Contract(pure = true)
    public abstract <T> Either<T, R> flatMapLeft(Function<? super L, ? extends Either<T, R>> transform);

    @Contract(pure = true)
    public abstract <T> Either<L, T> flatMapRight(Function<? super R, ? extends Either<L, T>> transform);

    @Contract(value = "_ -> this", pure = true)
    public abstract Either<L, R> ifLeft(Consumer<? super L> action);

    @Contract(value = "_ -> this", pure = true)
    public abstract Either<L, R> ifRight(Consumer<? super R> action);

    @Contract(value = "_, _ -> this", pure = true)
    public abstract Either<L, R> ifLeftOrElse(
        Consumer<? super L> action,
        Runnable rightAction
    );

    @Contract(value = "_, _ -> this", pure = true)
    public abstract Either<L, R> ifRightOrElse(
        Consumer<? super R> action,
        Runnable leftAction
    );

    @Override
    @Contract(value = "null -> false", pure = true)
    public abstract boolean equals(@Nullable Object object);

    @Override
    @Contract(pure = true)
    public abstract int hashCode();

    /**
     * Returns a string representation of this value.
     * <p>
     * The format of the string is similar to that of {@link Optional}.
     * </p>
     *
     * @return a string representation of this value
     */
    @Override
    @Contract(pure = true)
    public abstract String toString();
}
