/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Failed {@link Future}.
 *
 * @author Laurent Pireyn
 */
public final class FailedFuture<V> implements Future<V> {
    private final ExecutionException executionException;

    public FailedFuture(Throwable exception) {
        requireNotNull(exception);
        executionException = new ExecutionException(exception);
    }

    @Override
    @Contract(value = "-> true", pure = true)
    public boolean isDone() {
        return true;
    }

    @Override
    @Contract(value = "-> false", pure = true)
    public boolean isCancelled() {
        return false;
    }

    @Override
    @Contract(value = "-> fail", pure = true)
    public V get() throws ExecutionException {
        throw executionException;
    }

    @Override
    @Contract(value = "_, _ -> fail", pure = true)
    public V get(long timeout, TimeUnit unit) throws ExecutionException {
        requireNotNull(unit);
        throw executionException;
    }

    @Override
    @Contract(value = "_ -> false", pure = true)
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }
}
