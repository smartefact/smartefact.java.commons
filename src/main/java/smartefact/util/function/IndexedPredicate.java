/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Predicate on a value and its index.
 *
 * @author Laurent Pireyn
 * @param <T> the type of value
 */
@FunctionalInterface
public interface IndexedPredicate<T> {
    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("unchecked")
    static <T> IndexedPredicate<T> not(IndexedPredicate<? super T> predicate) {
        requireNotNull(predicate);
        return (IndexedPredicate<T>) predicate.negate();
    }

    boolean test(int index, T arg);

    default IndexedPredicate<T> negate() {
        return (index, arg) -> !test(index, arg);
    }

    default IndexedPredicate<T> and(IndexedPredicate<? super T> other) {
        requireNotNull(other);
        return (index, arg) -> test(index, arg) && other.test(index, arg);
    }

    default IndexedPredicate<T> or(IndexedPredicate<? super T> other) {
        requireNotNull(other);
        return (index, arg) -> test(index, arg) || other.test(index, arg);
    }
}
