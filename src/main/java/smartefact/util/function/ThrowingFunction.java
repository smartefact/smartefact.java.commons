/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link java.util.function.Function}-like that can throw exceptions.
 *
 * @author Laurent Pireyn
 */
@FunctionalInterface
public interface ThrowingFunction<T, R, X extends Throwable> {
    @Contract(pure = true)
    static <T, X extends Throwable> ThrowingFunction<T, T, X> identity() {
        return value -> value;
    }

    R apply(T arg) throws X;

    @Contract(value = "_ -> new", pure = true)
    default <V> ThrowingFunction<V, R, X> compose(ThrowingFunction<? super V, ? extends T, ? extends X> before) {
        requireNotNull(before);
        return arg -> apply(before.apply(arg));
    }

    @Contract(value = "_ -> new", pure = true)
    default <V> ThrowingFunction<T, V, X> andThen(ThrowingFunction<? super R, ? extends V, ? extends X> after) {
        requireNotNull(after);
        return arg -> after.apply(apply(arg));
    }
}
