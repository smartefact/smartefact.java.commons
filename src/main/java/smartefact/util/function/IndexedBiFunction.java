/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import java.util.function.Function;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Function that takes an index and two parameters.
 *
 * @author Laurent Pireyn
 * @param <T> the type of the first parameter
 * @param <U> the type of the second parameter
 * @param <R> the type of result
 */
@FunctionalInterface
public interface IndexedBiFunction<T, U, R> {
    R apply(int index, T arg1, U arg2);

    default <V> IndexedBiFunction<T, U, V> andThen(Function<? super R, ? extends V> after) {
        requireNotNull(after);
        return (index, arg1, arg2) -> after.apply(apply(index, arg1, arg2));
    }
}
