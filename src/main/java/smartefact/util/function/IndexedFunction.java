/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import static smartefact.util.Preconditions.requireNotNull;

/**
 * Function that takes a value and its index.
 *
 * @author Laurent Pireyn
 * @param <T> the type of value
 * @param <R> the type of result
 */
@FunctionalInterface
public interface IndexedFunction<T, R> {
    R apply(int index, T arg);

    default <V> IndexedFunction<V, R> compose(IndexedFunction<? super V, ? extends T> before) {
        requireNotNull(before);
        return (index, arg) -> apply(index, before.apply(index, arg));
    }

    default <V> IndexedFunction<T, V> andThen(IndexedFunction<? super R, ? extends V> after) {
        requireNotNull(after);
        return (index, arg) -> after.apply(index, apply(index, arg));
    }
}
