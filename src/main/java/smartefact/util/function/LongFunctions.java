/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util.function;

import java.util.function.LongFunction;

/**
 * Utilities for {@link LongFunction}s.
 *
 * @author Laurent Pireyn
 */
public final class LongFunctions {
    private static long identity(long n) {
        return n;
    }

    public static LongFunction<Long> identity() {
        return LongFunctions::identity;
    }

    private LongFunctions() {}
}
