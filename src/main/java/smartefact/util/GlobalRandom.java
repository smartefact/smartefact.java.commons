/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Random;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Global instance of {@link Random}.
 *
 * @author Laurent Pireyn
 */
public final class GlobalRandom {
    public static final Random INSTANCE = new Random();

    public static boolean nextBoolean() {
        return INSTANCE.nextBoolean();
    }

    public static void nextBytes(byte[] array) {
        requireNotNull(array);
        INSTANCE.nextBytes(array);
    }

    public static double nextDouble() {
        return INSTANCE.nextDouble();
    }

    public static float nextFloat() {
        return INSTANCE.nextFloat();
    }

    public static double nextGaussian() {
        return INSTANCE.nextGaussian();
    }

    public static int nextInt() {
        return INSTANCE.nextInt();
    }

    public static int nextInt(int bound) {
        return INSTANCE.nextInt(bound);
    }

    public static long nextLong() {
        return INSTANCE.nextLong();
    }

    private GlobalRandom() {}
}
