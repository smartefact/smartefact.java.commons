/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.Math.max;
import static java.lang.Math.min;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.emptyIterator;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import static java.util.Spliterators.emptySpliterator;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;
import static smartefact.util.Preconditions.requireGe0;
import static smartefact.util.Preconditions.requireGt0;
import static smartefact.util.Preconditions.requireNotNull;
import smartefact.util.function.CharConsumer;
import smartefact.util.function.CharFunction;
import smartefact.util.function.CharPredicate;
import smartefact.util.function.CharToCharFunction;
import smartefact.util.function.IndexedCharConsumer;
import smartefact.util.function.IndexedCharFunction;
import smartefact.util.function.IntToCharFunction;

/**
 * Utilities for {@link String}s and {@link CharSequence}s.
 *
 * @author Laurent Pireyn
 */
@SuppressWarnings("MixedMutabilityReturnType")
public final class Strings {
    /**
     * The empty string.
     */
    public static final String EMPTY = "";

    /**
     * Empty array of strings.
     */
    public static final String[] EMPTY_STRING_ARRAY = {};

    private static final String[] LINE_DELIMITERS = {"\r\n", "\n", "\r"};

    @Contract(pure = true)
    public static String abbreviate(
        String string,
        int maxLength
    ) {
        return abbreviate(
            string,
            maxLength,
            "..."
        );
    }

    @Contract(pure = true)
    public static String abbreviate(
        String string,
        int maxLength,
        String marker
    ) {
        requireNotNull(string);
        requireGe0(maxLength);
        requireNotNull(marker);
        final int markerLength = marker.length();
        require(
            maxLength >= markerLength,
            () -> "The maximum length <" + maxLength + "> is less than the length of the marker <" + markerLength + '>'
        );
        final int length = string.length();
        if (length <= maxLength) {
            return string;
        }
        return string.substring(0, maxLength - markerLength) + marker;
    }

    @Contract(pure = true)
    public static String abbreviateMiddle(
        String string,
        int maxLength
    ) {
        return abbreviateMiddle(
            string,
            maxLength,
            "..."
        );
    }

    @Contract(pure = true)
    public static String abbreviateMiddle(
        String string,
        int maxLength,
        String marker
    ) {
        requireNotNull(string);
        requireGe0(maxLength);
        requireNotNull(marker);
        final int markerLength = marker.length();
        require(
            maxLength >= markerLength,
            () -> "The maximum length <" + maxLength + "> is less than the length of the marker <" + markerLength + '>'
        );
        final int length = string.length();
        if (length <= maxLength) {
            return string;
        }
        final int keptLength = maxLength - markerLength;
        final int endLength = keptLength >> 1;
        final int startLength = endLength + (keptLength & 1);
        return string.substring(0, startLength) + marker + string.substring(length - endLength, length);
    }

    /**
     * Returns whether all characters in the given string match the given predicate.
     * <p>
     * This method returns {@code true} if the given string is empty.
     * </p>
     *
     * @param string the string
     * @param predicate the predicate
     * @return {@code true} if all characters in {@code string} match {@code predicate},
     * {@code false} otherwise
     * @see #any(CharSequence, CharPredicate)
     * @see #none(CharSequence, CharPredicate)
     */
    @Contract(pure = true)
    public static boolean all(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = 0; i < string.length(); ++i) {
            if (!predicate.test(string.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns whether any character in the given string matches the given predicate.
     * <p>
     * This method returns {@code false} if the given string is empty.
     * </p>
     *
     * @param string the string
     * @param predicate the predicate
     * @return {@code true} if any character in {@code string} matches {@code predicate},
     * {@code false} otherwise
     * @see #all(CharSequence, CharPredicate)
     * @see #none(CharSequence, CharPredicate)
     */
    @Contract(pure = true)
    public static boolean any(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = 0; i < string.length(); ++i) {
            if (predicate.test(string.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    @Contract(value = "_ -> new", pure = true)
    public static Iterable<Character> asIterable(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return emptyList();
        }
        return () -> iterator(string);
    }

    @Contract(value = "_ -> new", pure = true)
    public static Stream<Character> asParallelStream(CharSequence string) {
        return asStream(
            string,
            true
        );
    }

    @Contract(value = "_ -> new", pure = true)
    public static Stream<Character> asStream(CharSequence string) {
        return asStream(
            string,
            false
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static Stream<Character> asStream(
        CharSequence string,
        boolean parallel
    ) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return Stream.empty();
        }
        return StreamSupport.stream(spliterator(string), parallel);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <K, V> Map<K, V> associate(
        CharSequence string,
        CharFunction<? extends Pair<? extends K, ? extends V>> transform
    ) {
        requireNotNull(string);
        requireNotNull(transform);
        if (isEmpty(string)) {
            return emptyMap();
        }
        return associateTo(
            string,
            new HashMap<>(string.length()),
            transform
        );
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <K> Map<K, Character> associateBy(
        CharSequence string,
        CharFunction<? extends K> keySelector
    ) {
        requireNotNull(string);
        requireNotNull(keySelector);
        if (isEmpty(string)) {
            return emptyMap();
        }
        return associateByTo(
            string,
            new HashMap<>(string.length()),
            keySelector
        );
    }

    @Contract("_, _, _ -> param2")
    public static <K, M extends Map<? super K, ? super Character>> M associateByTo(
        CharSequence string,
        M destination,
        CharFunction<? extends K> keySelector
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        requireNotNull(keySelector);
        for (int i = 0; i < string.length(); ++i) {
            final char c = string.charAt(i);
            final K key = keySelector.apply(c);
            if (key != null) {
                destination.put(key, c);
            }
        }
        return destination;
    }

    @Contract("_, _, _ -> param2")
    public static <K, V, M extends Map<? super K, ? super V>> M associateTo(
        CharSequence string,
        M destination,
        CharFunction<? extends Pair<? extends K, ? extends V>> transform
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        requireNotNull(transform);
        for (int i = 0; i < string.length(); ++i) {
            final Pair<? extends K, ? extends V> pair = transform.apply(string.charAt(i));
            if (pair != null) {
                destination.put(pair.first(), pair.second());
            }
        }
        return destination;
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static <V> Map<Character, V> associateWith(
        CharSequence string,
        CharFunction<? extends V> valueSelector
    ) {
        requireNotNull(string);
        requireNotNull(valueSelector);
        if (isEmpty(string)) {
            return emptyMap();
        }
        return associateWithTo(
            string,
            new HashMap<>(string.length()),
            valueSelector
        );
    }

    @Contract("_, _, _ -> param2")
    public static <V, M extends Map<? super Character, ? super V>> M associateWithTo(
        CharSequence string,
        M destination,
        CharFunction<? extends V> valueSelector
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        requireNotNull(valueSelector);
        for (int i = 0; i < string.length(); ++i) {
            final char c = string.charAt(i);
            destination.put(c, valueSelector.apply(c));
        }
        return destination;
    }

    @Contract(pure = true)
    public static String capitalize(CharSequence string) {
        return replaceFirstChar(
            string,
            Character::toUpperCase
        );
    }

    @Contract(pure = true)
    public static char charAtOrElse(
        CharSequence string,
        int index,
        char defaultValue
    ) {
        requireNotNull(string);
        if (index < 0 || index >= string.length()) {
            return defaultValue;
        }
        return string.charAt(index);
    }

    @Contract(pure = true)
    public static char charAtOrElse(
        CharSequence string,
        int index,
        IntToCharFunction defaultValue
    ) {
        requireNotNull(string);
        requireNotNull(defaultValue);
        if (index < 0 || index >= string.length()) {
            return defaultValue.applyAsChar(index);
        }
        return string.charAt(index);
    }

    @Contract(pure = true)
    public static @Nullable Character charAtOrNull(
        CharSequence string,
        int index
    ) {
        requireNotNull(string);
        if (index < 0 || index >= string.length()) {
            return null;
        }
        return string.charAt(index);
    }

    @Contract(pure = true)
    public static List<String> chunked(
        CharSequence string,
        int size
    ) {
        requireNotNull(string);
        requireGt0(size);
        if (isEmpty(string)) {
            return emptyList();
        }
        final int length = string.length();
        final List<String> result = new ArrayList<>(length / size + 1);
        int firstIndex = 0;
        while (firstIndex < length) {
            final int lastIndex = min(firstIndex + size, length);
            result.add(substring(string, firstIndex, lastIndex));
            firstIndex = lastIndex;
        }
        return result;
    }

    @Contract(pure = true)
    public static String commonPrefixWith(
        CharSequence string,
        CharSequence other
    ) {
        return commonPrefixWith(
            string,
            other,
            false
        );
    }

    @Contract(pure = true)
    public static String commonPrefixWith(
        CharSequence string,
        CharSequence other,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        final int length = string.length();
        final int otherLength = other.length();
        int count = 0;
        while (count < length
            && count < otherLength
            && Characters.equals(string.charAt(count), other.charAt(count), ignoreCase)
        ) {
            ++count;
        }
        return take(string, count);
    }

    @Contract(pure = true)
    public static String commonSuffixWith(
        CharSequence string,
        CharSequence other
    ) {
        return commonSuffixWith(
            string,
            other,
            false
        );
    }

    @Contract(pure = true)
    public static String commonSuffixWith(
        CharSequence string,
        CharSequence other,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        final int length = string.length();
        final int otherLength = other.length();
        int count = 0;
        while (count < length
            && count < otherLength
            && Characters.equals(string.charAt(length - 1 - count), other.charAt(otherLength - 1 - count), ignoreCase)
        ) {
            ++count;
        }
        return takeLast(string, count);
    }

    /**
     * Returns whether the given string contains the given character.
     *
     * @param string the string
     * @param c the character
     * @return {@code true} if {@code string} contains {@code c}, {@code false} otherwise
     * @see #contains(CharSequence, char, boolean)
     * @see <a href="https://javadoc.io/static/org.apache.commons/commons-lang3/3.13.0/org/apache/commons/lang3/StringUtils.html#contains-java.lang.CharSequence-int-">Similar in Apache Commons Lang</a>
     */
    @Contract(pure = true)
    public static boolean contains(
        CharSequence string,
        char c
    ) {
        return contains(
            string,
            c,
            false
        );
    }

    /**
     * Returns whether the given string contains the given character,
     * optionally ignoring case.
     *
     * @param string the string
     * @param c the character
     * @param ignoreCase whether to ignore the case
     * @return {@code true} if {@code string} contains {@code c} (ignoring case if {@code ignoreCase} is {@code true}), {@code false} otherwise
     * @see #contains(CharSequence, char)
     */
    @Contract(pure = true)
    public static boolean contains(
        CharSequence string,
        char c,
        boolean ignoreCase
    ) {
        return indexOf(string, c, ignoreCase) != -1;
    }

    /**
     * Returns whether the given string contains the given string.
     *
     * @param string the string
     * @param other the other string
     * @return {@code true} if {@code string} contains {@code other}, {@code false} otherwise
     * @see #contains(CharSequence, CharSequence, boolean)
     * @see <a href="https://javadoc.io/static/org.apache.commons/commons-lang3/3.13.0/org/apache/commons/lang3/StringUtils.html#contains-java.lang.CharSequence-java.lang.CharSequence-">Similar in Apache Commons Lang</a>
     */
    @Contract(pure = true)
    public static boolean contains(
        CharSequence string,
        CharSequence other
    ) {
        return contains(
            string,
            other,
            false
        );
    }

    /**
     * Returns whether the given string contains the given string,
     * optionally ignoring case.
     *
     * @param string the string
     * @param other the other string
     * @param ignoreCase whether to ignore the case
     * @return {@code true} if {@code string} contains {@code other} (ignoring case if {@code ignoreCase} is {@code true}), {@code false} otherwise
     * @see #contains(CharSequence, CharSequence)
     * @see <a href="https://javadoc.io/static/org.apache.commons/commons-lang3/3.13.0/org/apache/commons/lang3/StringUtils.html#containsIgnoreCase-java.lang.CharSequence-java.lang.CharSequence-">Similar in Apache Commons Lang</a>
     */
    @Contract(pure = true)
    public static boolean contains(
        CharSequence string,
        CharSequence other,
        boolean ignoreCase
    ) {
        return indexOf(string, other, ignoreCase) != -1;
    }

    @Contract(pure = true)
    public static boolean contentEquals(
        CharSequence string,
        CharSequence other
    ) {
        return contentEquals(
            string,
            other,
            false
        );
    }

    @Contract(pure = true)
    public static boolean contentEquals(
        CharSequence string,
        CharSequence other,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        final int length = string.length();
        final int otherLength = other.length();
        if (length != otherLength) {
            return false;
        }
        for (int i = 0; i < length; ++i) {
            if (!Characters.equals(string.charAt(i), other.charAt(i), ignoreCase)) {
                return false;
            }
        }
        return true;
    }

    @Contract(pure = true)
    public static int count(
        CharSequence string,
        char c
    ) {
        return count(
            string,
            c,
            false
        );
    }

    @Contract(pure = true)
    public static int count(
        CharSequence string,
        char c,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        int result = 0;
        for (int i = 0; i < string.length(); ++i) {
            if (Characters.equals(string.charAt(i), c, ignoreCase)) {
                ++result;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static int count(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        int result = 0;
        for (int i = 0; i < string.length(); ++i) {
            if (predicate.test(string.charAt(i))) {
                ++result;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static String drop(
        CharSequence string,
        int n
    ) {
        requireNotNull(string);
        requireGe0(n);
        final int length = string.length();
        if (n >= length) {
            return EMPTY;
        }
        return substring(string, n, length);
    }

    @Contract(pure = true)
    public static String dropLast(
        CharSequence string,
        int n
    ) {
        requireNotNull(string);
        requireGe0(n);
        final int length = string.length();
        if (n >= length) {
            return EMPTY;
        }
        return substring(string, 0, length - n);
    }

    @Contract(pure = true)
    public static String dropLastWhile(
        CharSequence string,
        CharPredicate predicate
    ) {
        return trimEnd(
            string,
            predicate
        );
    }

    @Contract(pure = true)
    public static String dropWhile(
        CharSequence string,
        CharPredicate predicate
    ) {
        return trimStart(
            string,
            predicate
        );
    }

    @Contract(pure = true)
    public static boolean endsWith(
        CharSequence string,
        char c
    ) {
        return endsWith(
            string,
            c,
            false
        );
    }

    @Contract(pure = true)
    public static boolean endsWith(
        CharSequence string,
        char c,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        final int lastIndex = lastIndex(string);
        return lastIndex >= 0
            && Characters.equals(string.charAt(lastIndex), c, ignoreCase);
    }

    @Contract(pure = true)
    public static boolean endsWith(
        CharSequence string,
        CharSequence suffix
    ) {
        return endsWith(
            string,
            suffix,
            false
        );
    }

    @Contract(pure = true)
    public static boolean endsWith(
        CharSequence string,
        CharSequence suffix,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(suffix);
        final int suffixLength = suffix.length();
        if (suffixLength == 0) {
            return true;
        }
        final int length = string.length();
        if (length < suffixLength) {
            return false;
        }
        return contentEquals(substring(string, length - suffixLength, length), suffix, ignoreCase);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static String filter(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        if (isEmpty(string)) {
            return EMPTY;
        }
        try {
            return filterTo(
                string,
                new StringBuilder(string.length()),
                predicate
            )
                .toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Contract("_, _, _ -> param2")
    public static <A extends Appendable> A filterTo(
        CharSequence string,
        A destination,
        CharPredicate predicate
    ) throws IOException {
        requireNotNull(string);
        requireNotNull(destination);
        requireNotNull(predicate);
        final int length = string.length();
        for (int i = 0; i < length; ++i) {
            final char c = string.charAt(i);
            if (predicate.test(c)) {
                destination.append(c);
            }
        }
        return destination;
    }

    @Contract(pure = true)
    public static @Nullable Character find(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = 0; i < string.length(); ++i) {
            final char c = string.charAt(i);
            if (predicate.test(c)) {
                return c;
            }
        }
        return null;
    }

    @Contract(pure = true)
    public static @Nullable Character findLast(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = string.length() - 1; i >= 0; --i) {
            final char c = string.charAt(i);
            if (predicate.test(c)) {
                return c;
            }
        }
        return null;
    }

    @Contract(pure = true)
    public static char first(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            throw new NoSuchElementException();
        }
        return string.charAt(0);
    }

    @Contract(pure = true)
    public static char first(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = 0; i < string.length(); ++i) {
            final char c = string.charAt(i);
            if (predicate.test(c)) {
                return c;
            }
        }
        throw new NoSuchElementException();
    }

    @Contract(pure = true)
    public static @Nullable Character firstOrNull(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return null;
        }
        return string.charAt(0);
    }

    @Contract(pure = true)
    public static void forEach(
        CharSequence string,
        CharConsumer action
    ) {
        requireNotNull(string);
        requireNotNull(action);
        final int length = string.length();
        for (int i = 0; i < length; ++i) {
            action.accept(string.charAt(i));
        }
    }

    @Contract(pure = true)
    public static void forEachIndexed(
        CharSequence string,
        IndexedCharConsumer action
    ) {
        requireNotNull(string);
        requireNotNull(action);
        final int length = string.length();
        for (int i = 0; i < length; ++i) {
            action.accept(i, string.charAt(i));
        }
    }

    @Contract(pure = true)
    public static <R extends CharSequence> R ifBlank(
        R string,
        R defaultValue
    ) {
        requireNotNull(string);
        requireNotNull(defaultValue);
        if (isBlank(string)) {
            return defaultValue;
        }
        return string;
    }

    @Contract(pure = true)
    public static <R extends CharSequence> R ifBlank(
        R string,
        Supplier<? extends R> defaultValue
    ) {
        requireNotNull(string);
        requireNotNull(defaultValue);
        if (isBlank(string)) {
            return defaultValue.get();
        }
        return string;
    }

    @Contract(pure = true)
    public static <R extends CharSequence> R ifEmpty(
        R string,
        R defaultValue
    ) {
        requireNotNull(string);
        requireNotNull(defaultValue);
        if (isEmpty(string)) {
            return defaultValue;
        }
        return string;
    }

    @Contract(pure = true)
    public static <R extends CharSequence> R ifEmpty(
        R string,
        Supplier<? extends R> defaultValue
    ) {
        requireNotNull(string);
        requireNotNull(defaultValue);
        if (isEmpty(string)) {
            return defaultValue.get();
        }
        return string;
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        char c
    ) {
        return indexOf(
            string,
            c,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        char c
    ) {
        return indexOf(
            string,
            c,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        char c,
        int startIndex
    ) {
        return indexOf(
            string,
            c,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        char c,
        int startIndex
    ) {
        return indexOf(
            string,
            c,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        char c,
        boolean ignoreCase
    ) {
        return indexOf(
            string,
            c,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        char c,
        boolean ignoreCase
    ) {
        return indexOf(
            string,
            c,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        char c,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        for (int i = max(startIndex, 0); i < string.length(); ++i) {
            if (Characters.equals(string.charAt(i), c, ignoreCase)) {
                return i;
            }
        }
        return -1;
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        char c,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        if (!ignoreCase) {
            return string.indexOf(c, startIndex);
        }
        return indexOf(
            (CharSequence) string,
            c,
            startIndex,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        CharSequence other
    ) {
        return indexOf(
            string,
            other,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        String other
    ) {
        return indexOf(
            string,
            other,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        CharSequence other,
        int startIndex
    ) {
        return indexOf(
            string,
            other,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        String other,
        int startIndex
    ) {
        return indexOf(
            string,
            other,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        CharSequence other,
        boolean ignoreCase
    ) {
        return indexOf(
            string,
            other,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        String other,
        boolean ignoreCase
    ) {
        return indexOf(
            string,
            other,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOf(
        CharSequence string,
        CharSequence other,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        final int stringLength = string.length();
        final int actualStartIndex = Integers.coerceIn(startIndex, 0, stringLength);
        final int otherLength = other.length();
        if (otherLength == 0) {
            return actualStartIndex;
        }
        loop_index:
        for (int index = actualStartIndex; index < stringLength; ++index) {
            int j = 0;
            while (j < otherLength) {
                final int i = index + j;
                if (i == stringLength) {
                    break;
                }
                if (!Characters.equals(string.charAt(i), other.charAt(j), ignoreCase)) {
                    continue loop_index;
                }
                ++j;
            }
            if (j == otherLength) {
                return index;
            }
        }
        return -1;
    }

    @Contract(pure = true)
    public static int indexOf(
        String string,
        String other,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        if (!ignoreCase) {
            return string.indexOf(other, startIndex);
        }
        return indexOf(
            (CharSequence) string,
            other,
            startIndex,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        char[] chars
    ) {
        return indexOfAny(
            string,
            chars,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        char[] chars,
        int startIndex
    ) {
        return indexOfAny(
            string,
            chars,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        char[] chars,
        boolean ignoreCase
    ) {
        return indexOfAny(
            string,
            chars,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        char[] chars,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(chars);
        int result = -1;
        for (final char candidate : chars) {
            final int index = indexOf(string, candidate, startIndex, ignoreCase);
            if (index != -1 && (result == -1 || index < result)) {
                result = index;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        CharSequence[] strings
    ) {
        return indexOfAny(
            string,
            strings,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        CharSequence[] strings,
        int startIndex
    ) {
        return indexOfAny(
            string,
            strings,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        CharSequence[] strings,
        boolean ignoreCase
    ) {
        return indexOfAny(
            string,
            strings,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        CharSequence[] strings,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(strings);
        int result = -1;
        for (final CharSequence candidate : strings) {
            final int index = indexOf(string, candidate, startIndex, ignoreCase);
            if (index != -1 && (result == -1 || index < result)) {
                result = index;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        Iterable<? extends CharSequence> strings
    ) {
        return indexOfAny(
            string,
            strings,
            0,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        Iterable<? extends CharSequence> strings,
        int startIndex
    ) {
        return indexOfAny(
            string,
            strings,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        Iterable<? extends CharSequence> strings,
        boolean ignoreCase
    ) {
        return indexOfAny(
            string,
            strings,
            0,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int indexOfAny(
        CharSequence string,
        Iterable<? extends CharSequence> strings,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(strings);
        int result = -1;
        for (final CharSequence candidate : strings) {
            final int index = indexOf(string, candidate, startIndex, ignoreCase);
            if (index != -1 && (result == -1 || index < result)) {
                result = index;
            }
        }
        return result;
    }

    @Contract(pure = true)
    public static int indexOfFirst(
        CharSequence string,
        CharPredicate predicate
    ) {
        return indexOfFirst(
            string,
            predicate,
            0
        );
    }

    @Contract(pure = true)
    public static int indexOfFirst(
        CharSequence string,
        CharPredicate predicate,
        int startIndex
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = startIndex; i < string.length(); ++i) {
            if (predicate.test(string.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    @Contract(pure = true)
    public static int indexOfLast(
        CharSequence string,
        CharPredicate predicate
    ) {
        return indexOfLast(
            string,
            predicate,
            lastIndex(string)
        );
    }

    @Contract(pure = true)
    public static int indexOfLast(
        CharSequence string,
        CharPredicate predicate,
        int startIndex
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = startIndex; i >= 0; --i) {
            if (predicate.test(string.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    @Contract(value = "_ -> new", pure = true)
    public static Iterator<IndexedChar> indexedIterator(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return emptyIterator();
        }
        return new Iterator<>() {
            private int index;

            @Override
            public boolean hasNext() {
                return index < string.length();
            }

            @Override
            public IndexedChar next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                final char c = string.charAt(index);
                ++index;
                return new IndexedChar(index, c);
            }
        };
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static InputStream inputStream(
        String string,
        Charset charset
    ) {
        return new ByteArrayInputStream(toByteArray(string, charset));
    }

    @Contract(value = "_ -> new", pure = true)
    public static InputStream inputStreamUtf8(String string) {
        return inputStream(string, StandardCharsets.UTF_8);
    }

    /**
     * Returns whether the given string is blank.
     * <p>
     * A string is blank if it is empty
     * or all its characters are {@linkplain Character#isWhitespace(char) whitespaces}.
     * </p>
     *
     * @param string the string
     * @return {@code true} if {@code string} is blank,
     * {@code false} otherwise
     */
    @Contract(pure = true)
    public static boolean isBlank(CharSequence string) {
        return all(
            string,
            Character::isWhitespace
        );
    }

    @Contract(pure = true)
    public static boolean isBlank(String string) {
        requireNotNull(string);
        return string.isBlank();
    }

    /**
     * Returns whether the given string is empty.
     *
     * @param string the string
     * @return {@code true} if {@code string} is empty,
     * {@code false} otherwise
     */
    @Contract(pure = true)
    public static boolean isEmpty(CharSequence string) {
        requireNotNull(string);
        return string.length() == 0;
    }

    @Contract(pure = true)
    public static boolean isEmpty(String string) {
        requireNotNull(string);
        return string.isEmpty();
    }

    /**
     * Returns whether the given string is {@code null} or {@linkplain #isBlank(CharSequence) blank}.
     *
     * @param string the string
     * @return {@code true} if {@code string} is {@code null} or blank,
     * {@code false} otherwise
     * @see #isBlank(CharSequence)
     */
    @Contract(value = "null -> true", pure = true)
    public static boolean isNullOrBlank(@Nullable CharSequence string) {
        return string == null || isBlank(string);
    }

    @Contract(value = "null -> true", pure = true)
    public static boolean isNullOrBlank(@Nullable String string) {
        return string == null || isBlank(string);
    }

    /**
     * Returns whether the given string is {@code null} or {@linkplain #isEmpty(CharSequence) empty}.
     *
     * @param string the string
     * @return {@code true} if {@code string} is {@code null} or empty,
     * {@code false} otherwise
     * @see #isEmpty(CharSequence)
     */
    @Contract(value = "null -> true", pure = true)
    public static boolean isNullOrEmpty(@Nullable CharSequence string) {
        return string == null || isEmpty(string);
    }

    @Contract(value = "null -> true", pure = true)
    public static boolean isNullOrEmpty(@Nullable String string) {
        return string == null || isEmpty(string);
    }

    @Contract(value = "_ -> new", pure = true)
    public static Iterator<Character> iterator(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return emptyIterator();
        }
        return new Iterator<>() {
            private int index;

            @Override
            public boolean hasNext() {
                return index < string.length();
            }

            @Override
            public Character next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                final char c = string.charAt(index);
                ++index;
                return c;
            }
        };
    }

    @Contract(pure = true)
    public static String javaEscaped(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return EMPTY;
        }
        final int length = string.length();
        final var builder = new StringBuilder(length);
        for (int i = 0; i < length; ++i) {
            final char c = string.charAt(i);
            switch (c) {
                case '\\':
                    builder.append("\\\\");
                    break;
                case '"':
                    builder.append("\\\"");
                    break;
                case '\b':
                    builder.append("\\b");
                    break;
                case '\t':
                    builder.append("\\t");
                    break;
                case '\n':
                    builder.append("\\n");
                    break;
                case '\f':
                    builder.append("\\f");
                    break;
                case '\r':
                    builder.append("\\r");
                    break;
                default:
                    if (Characters.in(c, ' ', '~')) {
                        builder.append(c);
                    } else {
                        // TODO: Improve implementation
                        builder.append("\\u").append(takeLast("0000" + Integers.toString(c, 16), 4));
                    }
            }
        }
        return builder.toString();
    }

    @Contract(pure = true)
    public static char last(CharSequence string) {
        requireNotNull(string);
        final int lastIndex = lastIndex(string);
        if (lastIndex == -1) {
            throw new NoSuchElementException();
        }
        return string.charAt(lastIndex);
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        char c
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            c,
            lastIndex(string),
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        char c
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            c,
            lastIndex(string),
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        char c,
        int startIndex
    ) {
        return lastIndexOf(
            string,
            c,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        char c,
        int startIndex
    ) {
        return lastIndexOf(
            string,
            c,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        char c,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            c,
            lastIndex(string),
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        char c,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            c,
            lastIndex(string),
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        char c,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        for (int i = min(startIndex, lastIndex(string)); i >= 0; --i) {
            if (Characters.equals(string.charAt(i), c, ignoreCase)) {
                return i;
            }
        }
        return -1;
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        char c,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        if (!ignoreCase) {
            return string.lastIndexOf(c, startIndex);
        }
        return lastIndexOf(
            (CharSequence) string,
            c,
            startIndex,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        CharSequence other
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            other,
            lastIndex(string),
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        String other
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            other,
            lastIndex(string),
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        CharSequence other,
        int startIndex
    ) {
        return lastIndexOf(
            string,
            other,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        String other,
        int startIndex
    ) {
        return lastIndexOf(
            string,
            other,
            startIndex,
            false
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        CharSequence other,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            other,
            lastIndex(string),
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        String other,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        return lastIndexOf(
            string,
            other,
            lastIndex(string),
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        CharSequence string,
        CharSequence other,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        final int stringLength = string.length();
        final int actualStartIndex = Integers.coerceIn(startIndex, 0, stringLength);
        final int otherLength = other.length();
        if (otherLength == 0) {
            return actualStartIndex;
        }
        loop_index:
        for (int index = actualStartIndex; index >= 0; --index) {
            int j = 0;
            while (j < otherLength) {
                final int i = index + j;
                if (i == stringLength) {
                    break;
                }
                if (!Characters.equals(string.charAt(i), other.charAt(j), ignoreCase)) {
                    continue loop_index;
                }
                ++j;
            }
            if (j == otherLength) {
                return index;
            }
        }
        return -1;
    }

    @Contract(pure = true)
    public static int lastIndexOf(
        String string,
        String other,
        int startIndex,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(other);
        if (!ignoreCase) {
            return string.lastIndexOf(other, startIndex);
        }
        return lastIndexOf(
            (CharSequence) string,
            other,
            startIndex,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static @Nullable Character lastOrNull(CharSequence string) {
        requireNotNull(string);
        final int lastIndex = lastIndex(string);
        if (lastIndex == -1) {
            return null;
        }
        return string.charAt(lastIndex);
    }

    @Contract(pure = true)
    public static char last(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = string.length() - 1; i >= 0; --i) {
            final char c = string.charAt(i);
            if (predicate.test(c)) {
                return c;
            }
        }
        throw new NoSuchElementException();
    }

    /**
     * Returns the index of the last character of the given string.
     *
     * @param string the string
     * @return the index of the last character of {@code string},
     * or -1 if {@code string} is empty
     */
    @Contract(pure = true)
    public static int lastIndex(CharSequence string) {
        requireNotNull(string);
        return string.length() - 1;
    }

    @Contract(pure = true)
    public static Iterable<String> lines(CharSequence string) {
        return split(
            string,
            LINE_DELIMITERS
        );
    }

    @Contract(pure = true)
    public static String lowercase(String string) {
        return lowercase(
            string,
            Locale.ROOT
        );
    }

    @Contract(pure = true)
    public static String lowercase(
        String string,
        Locale locale
    ) {
        requireNotNull(string);
        requireNotNull(locale);
        return string.toLowerCase(locale);
    }

    @Contract(pure = true)
    public static <R> List<R> map(
        CharSequence string,
        CharFunction<? extends R> transform
    ) {
        requireNotNull(string);
        requireNotNull(transform);
        if (isEmpty(string)) {
            return emptyList();
        }
        return mapTo(
            string,
            new ArrayList<>(string.length()),
            transform
        );
    }

    @Contract(pure = true)
    public static <R> List<R> mapIndexed(
        CharSequence string,
        IndexedCharFunction<? extends R> transform
    ) {
        requireNotNull(string);
        requireNotNull(transform);
        if (isEmpty(string)) {
            return emptyList();
        }
        return mapIndexedTo(
            string,
            new ArrayList<>(string.length()),
            transform
        );
    }

    @Contract("_, _, _ -> param2")
    public static <R, C extends Collection<? super R>> C mapIndexedTo(
        CharSequence string,
        C destination,
        IndexedCharFunction<? extends R> transform
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        requireNotNull(transform);
        for (int i = 0; i < string.length(); ++i) {
            destination.add(transform.apply(i, string.charAt(i)));
        }
        return destination;
    }

    @Contract("_, _, _ -> param2")
    public static <R, C extends Collection<? super R>> C mapTo(
        CharSequence string,
        C destination,
        CharFunction<? extends R> transform
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        requireNotNull(transform);
        for (int i = 0; i < string.length(); ++i) {
            destination.add(transform.apply(string.charAt(i)));
        }
        return destination;
    }

    /**
     * Returns whether the given string matches the given pattern.
     *
     * @param string the string
     * @param pattern the pattern
     * @return {@code true} if {@code string} matches {@code pattern},
     * {@code false} otherwise
     * @see Pattern#matches(String, CharSequence)
     * @see Matcher#matches()
     */
    @Contract(pure = true)
    public static boolean matches(
        CharSequence string,
        Pattern pattern
    ) {
        requireNotNull(string);
        requireNotNull(pattern);
        return pattern.matcher(string).matches();
    }

    /**
     * Returns whether no characters in the given string match the given predicate.
     * <p>
     * This method returns {@code true} if the given string is empty.
     * </p>
     *
     * @param string the string
     * @param predicate the predicate
     * @return {@code true} if no characters in {@code string} match {@code predicate},
     * {@code false} otherwise
     * @see #all(CharSequence, CharPredicate)
     * @see #any(CharSequence, CharPredicate)
     */
    @Contract(pure = true)
    public static boolean none(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = 0; i < string.length(); ++i) {
            if (predicate.test(string.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    @Contract(pure = true)
    public static CharSequence orEmpty(@Nullable CharSequence string) {
        return Objects.orElse(string, EMPTY);
    }

    @Contract(pure = true)
    public static String orEmpty(@Nullable String string) {
        return Objects.orElse(string, EMPTY);
    }

    @Contract(pure = true)
    public static String pad(
        CharSequence string,
        int n
    ) {
        return pad(
            string,
            n,
            ' '
        );
    }

    @Contract(pure = true)
    public static String pad(
        CharSequence string,
        int n,
        char padding
    ) {
        requireNotNull(string);
        requireGe0(n);
        final int length = string.length();
        if (length >= n) {
            return string.toString();
        }
        final StringBuilder builder = new StringBuilder(n);
        final int paddingCount = n - length;
        final int endPaddingCount = paddingCount >> 1;
        final int startPaddingCount = endPaddingCount + (paddingCount & 1);
        for (int i = 0; i < startPaddingCount; ++i) {
            builder.append(padding);
        }
        builder.append(string);
        for (int i = 0; i < endPaddingCount; ++i) {
            builder.append(padding);
        }
        return builder.toString();
    }

    @Contract(pure = true)
    public static String padLeft(
        CharSequence string,
        int n
    ) {
        return padLeft(
            string,
            n,
            ' '
        );
    }

    @Contract(pure = true)
    public static String padLeft(
        CharSequence string,
        int n,
        char padding
    ) {
        requireNotNull(string);
        requireGe0(n);
        final int length = string.length();
        if (length >= n) {
            return string.toString();
        }
        final StringBuilder builder = new StringBuilder(n);
        final int paddingCount = n - length;
        for (int i = 0; i < paddingCount; ++i) {
            builder.append(padding);
        }
        builder.append(string);
        return builder.toString();
    }

    @Contract(pure = true)
    public static String padRight(
        CharSequence string,
        int n
    ) {
        return padRight(
            string,
            n,
            ' '
        );
    }

    @Contract(pure = true)
    public static String padRight(
        CharSequence string,
        int n,
        char padding
    ) {
        requireNotNull(string);
        requireGe0(n);
        final int length = string.length();
        if (length >= n) {
            return string.toString();
        }
        final StringBuilder builder = new StringBuilder(n);
        builder.append(string);
        final int paddingCount = n - length;
        for (int i = 0; i < paddingCount; ++i) {
            builder.append(padding);
        }
        return builder.toString();
    }

    @Contract(pure = true)
    public static Pair<String, String> partition(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        final String trueChars;
        final String falseChars;
        if (isEmpty(string)) {
            trueChars = EMPTY;
            falseChars = EMPTY;
        } else {
            final int length = string.length();
            final StringBuilder trueBuilder = new StringBuilder(length);
            final StringBuilder falseBuilder = new StringBuilder(length);
            for (int i = 0; i < length; ++i) {
                final char c = string.charAt(i);
                if (predicate.test(c)) {
                    trueBuilder.append(c);
                } else {
                    falseBuilder.append(c);
                }
            }
            trueChars = trueBuilder.toString();
            falseChars = falseBuilder.toString();
        }
        return new Pair<>(trueChars, falseChars);
    }

    @Contract(pure = true)
    public static String repeat(
        String string,
        int n
    ) {
        return repeat(
            string,
            n,
            EMPTY
        );
    }

    @Contract(pure = true)
    public static String repeat(
        String string,
        int n,
        CharSequence separator
    ) {
        requireNotNull(string);
        requireGe0(n);
        requireNotNull(separator);
        if (isEmpty(separator)) {
            return string.repeat(n);
        }
        if (n == 0) {
            return EMPTY;
        }
        if (n == 1) {
            return string;
        }
        final int separatorLength = separator.length();
        final StringBuilder builder = new StringBuilder((string.length() + separatorLength) * n - separatorLength);
        for (int i = 0; i < n; ++i) {
            if (i > 0) {
                builder.append(separator);
            }
            builder.append(string);
        }
        return builder.toString();
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static String replaceFirstChar(
        CharSequence string,
        CharToCharFunction transform
    ) {
        requireNotNull(string);
        requireNotNull(transform);
        if (isEmpty(string)) {
            return EMPTY;
        }
        final int length = string.length();
        return new StringBuilder(length)
            .append(transform.applyAsChar(string.charAt(0)))
            .append(string, 1, length)
            .toString();
    }

    @Contract(value = "_ -> new", pure = true)
    public static String reversed(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return EMPTY;
        }
        final int length = string.length();
        final char[] chars = new char[length];
        for (int i = 0; i < length; ++i) {
            chars[i] = string.charAt(length - 1 - i);
        }
        return new String(chars);
    }

    @Contract(pure = true)
    public static char single(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            throw new NoSuchElementException();
        }
        if (string.length() > 1) {
            throw new IllegalArgumentException();
        }
        return string.charAt(0);
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        char delimiter
    ) {
        return split(
            string,
            delimiter,
            false,
            -1
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        char delimiter,
        boolean ignoreCase
    ) {
        return split(
            string,
            delimiter,
            ignoreCase,
            -1
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        char delimiter,
        int limit
    ) {
        return split(
            string,
            delimiter,
            false,
            limit
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        char delimiter,
        boolean ignoreCase,
        int limit
    ) {
        requireNotNull(string);
        return () -> new DelimiterSplittingIterator(
            string,
            limit,
            delimiter,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        CharPredicate predicate
    ) {
        return split(
            string,
            predicate,
            -1
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        CharPredicate predicate,
        int limit
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        return () -> new PredicateSplittingIterator(
            string,
            limit,
            predicate
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        CharSequence[] delimiters
    ) {
        return split(
            string,
            delimiters,
            false,
            -1
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        CharSequence[] delimiters,
        boolean ignoreCase
    ) {
        return split(
            string,
            delimiters,
            ignoreCase,
            -1
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        CharSequence[] delimiters,
        int limit
    ) {
        return split(
            string,
            delimiters,
            false,
            limit
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        CharSequence[] delimiters,
        boolean ignoreCase,
        int limit
    ) {
        requireNotNull(string);
        requireNotNull(delimiters);
        return () -> new DelimitersSplittingIterator(
            string,
            limit,
            delimiters,
            ignoreCase
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        Pattern pattern
    ) {
        return split(
            string,
            pattern,
            -1
        );
    }

    @Contract(pure = true)
    public static Iterable<String> split(
        CharSequence string,
        Pattern pattern,
        int limit
    ) {
        requireNotNull(string);
        requireNotNull(pattern);
        return () -> new PatternSplittingIterator(
            string,
            limit,
            pattern
        );
    }

    @Contract(value = "_ -> new", pure = true)
    public static Spliterator<Character> spliterator(CharSequence string) {
        requireNotNull(string);
        if (isEmpty(string)) {
            return emptySpliterator();
        }
        return Spliterators.spliterator(
            iterator(string),
            string.length(),
            Spliterator.IMMUTABLE | Spliterator.NONNULL | Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SUBSIZED
        );
    }

    @Contract(pure = true)
    public static boolean startsWith(
        CharSequence string,
        char c
    ) {
        return startsWith(
            string,
            c,
            false
        );
    }

    @Contract(pure = true)
    public static boolean startsWith(
        CharSequence string,
        char c,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        return !isEmpty(string)
            && Characters.equals(string.charAt(0), c, ignoreCase);
    }

    @Contract(pure = true)
    public static boolean startsWith(
        CharSequence string,
        CharSequence prefix
    ) {
        return startsWith(
            string,
            prefix,
            false
        );
    }

    @Contract(pure = true)
    public static boolean startsWith(
        CharSequence string,
        CharSequence prefix,
        boolean ignoreCase
    ) {
        requireNotNull(string);
        requireNotNull(prefix);
        final int prefixLength = prefix.length();
        if (prefixLength == 0) {
            return true;
        }
        final int length = string.length();
        if (length < prefixLength) {
            return false;
        }
        return contentEquals(substring(string, 0, prefixLength), prefix, ignoreCase);
    }

    @Contract(pure = true)
    public static String substring(
        CharSequence string,
        int startIndex
    ) {
        requireNotNull(string);
        return substring(string, startIndex, string.length());
    }

    @Contract(pure = true)
    public static String substring(
        CharSequence string,
        int startIndex,
        int endIndex
    ) {
        requireNotNull(string);
        return string.subSequence(startIndex, endIndex).toString();
    }

    @Contract(pure = true)
    public static String substring(
        String string,
        int startIndex
    ) {
        requireNotNull(string);
        return string.substring(startIndex);
    }

    @Contract(pure = true)
    public static String substring(
        String string,
        int startIndex,
        int endIndex
    ) {
        requireNotNull(string);
        return string.substring(startIndex, endIndex);
    }

    @Contract(pure = true)
    public static String substringAfter(
        String string,
        char delimiter,
        String missingDelimiterValue
    ) {
        requireNotNull(string);
        requireNotNull(missingDelimiterValue);
        final int index = indexOf(string, delimiter);
        return index != -1 ? substring(string, index + 1) : missingDelimiterValue;
    }

    @Contract(pure = true)
    public static String substringAfterLast(
        String string,
        char delimiter,
        String missingDelimiterValue
    ) {
        requireNotNull(string);
        requireNotNull(missingDelimiterValue);
        final int index = lastIndexOf(string, delimiter);
        return index != -1 ? substring(string, index + 1) : missingDelimiterValue;
    }

    @Contract(pure = true)
    public static String substringBefore(
        String string,
        char delimiter,
        String missingDelimiterValue
    ) {
        requireNotNull(string);
        requireNotNull(missingDelimiterValue);
        final int index = indexOf(string, delimiter);
        return index != -1 ? substring(string, 0, index) : missingDelimiterValue;
    }

    @Contract(pure = true)
    public static String substringBeforeLast(
        String string,
        char delimiter,
        String missingDelimiterValue
    ) {
        requireNotNull(string);
        requireNotNull(missingDelimiterValue);
        final int index = lastIndexOf(string, delimiter);
        return index != -1 ? substring(string, 0, index) : missingDelimiterValue;
    }

    /**
     * Returns the first n characters of the given string.
     *
     * @param string the string
     * @param n the number of first characters to return
     * @return the first {@code n} characters of {@code string}
     * @see <a href="https://javadoc.io/static/org.apache.commons/commons-lang3/3.13.0/org/apache/commons/lang3/StringUtils.html#left-java.lang.String-int-">Similar in Apache Commons Lang</a>
     */
    @Contract(pure = true)
    public static String take(
        CharSequence string,
        int n
    ) {
        requireNotNull(string);
        requireGe0(n);
        if (n == 0) {
            return EMPTY;
        }
        if (n >= string.length()) {
            return string.toString();
        }
        return substring(string, 0, n);
    }

    /**
     * Returns the last n characters of the given string.
     *
     * @param string the string
     * @param n the number of last characters to return
     * @return the last {@code n} characters of {@code string}
     * @see <a href="https://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/StringUtils.html#right-java.lang.String-int-">Similar in Apache Commons Lang</a>
     */
    @Contract(pure = true)
    public static String takeLast(
        CharSequence string,
        int n
    ) {
        requireNotNull(string);
        requireGe0(n);
        if (n == 0) {
            return EMPTY;
        }
        final int length = string.length();
        if (n >= length) {
            return string.toString();
        }
        return substring(string, length - n, length);
    }

    @Contract(pure = true)
    public static String takeLastWhile(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        final int length = string.length();
        int firstIndex = length;
        while (firstIndex > 0 && predicate.test(string.charAt(firstIndex - 1))) {
            --firstIndex;
        }
        return substring(string, firstIndex, length);
    }

    @Contract(pure = true)
    public static String takeWhile(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        int lastIndex = 0;
        while (lastIndex < string.length() && predicate.test(string.charAt(lastIndex))) {
            ++lastIndex;
        }
        if (lastIndex == 0) {
            return EMPTY;
        }
        return substring(string, 0, lastIndex);
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static ArrayList<Character> toArrayList(CharSequence string) {
        requireNotNull(string);
        return toCollection(
            string,
            new ArrayList<>(string.length())
        );
    }

    @Contract(pure = true)
    public static BigDecimal toBigDecimal(String string) {
        return toBigDecimal(
            string,
            MathContext.UNLIMITED
        );
    }

    @Contract(pure = true)
    public static BigDecimal toBigDecimal(
        String string,
        MathContext mathContext
    ) {
        requireNotNull(string);
        requireNotNull(mathContext);
        return new BigDecimal(string, mathContext);
    }

    @Contract(pure = true)
    public static @Nullable BigDecimal toBigDecimalOrNull(String string) {
        return toBigDecimalOrNull(
            string,
            MathContext.UNLIMITED
        );
    }

    @Contract(pure = true)
    public static @Nullable BigDecimal toBigDecimalOrNull(
        String string,
        MathContext mathContext
    ) {
        requireNotNull(string);
        requireNotNull(mathContext);
        try {
            return new BigDecimal(string, mathContext);
        } catch (NumberFormatException | ArithmeticException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static BigInteger toBigInteger(String string) {
        return toBigInteger(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static BigInteger toBigInteger(
        String string,
        int radix
    ) {
        requireNotNull(string);
        return new BigInteger(string, radix);
    }

    @Contract(pure = true)
    public static @Nullable BigInteger toBigIntegerOrNull(String string) {
        return toBigIntegerOrNull(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static @Nullable BigInteger toBigIntegerOrNull(
        String string,
        int radix
    ) {
        try {
            return toBigInteger(
                string,
                radix
            );
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static boolean toBooleanStrict(String string) {
        return requireNotNull(
            toBooleanStrictOrNull(string),
            () -> "The string <" + string + "> is not <true> or <false>"
        );
    }

    @Contract(pure = true)
    public static @Nullable Boolean toBooleanStrictOrNull(String string) {
        requireNotNull(string);
        switch (string) {
            case "true":
                return Boolean.TRUE;
            case "false":
                return Boolean.FALSE;
            default:
                return null;
        }
    }

    @Contract(pure = true)
    public static byte toByte(String string) {
        return toByte(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static byte toByte(
        String string,
        int radix
    ) {
        requireNotNull(string);
        return Byte.parseByte(string, radix);
    }

    @Contract(value = "_, _ -> new", pure = true)
    public static byte[] toByteArray(
        String string,
        Charset charset
    ) {
        requireNotNull(string);
        requireNotNull(charset);
        return string.getBytes(charset);
    }

    @Contract(value = "_ -> new", pure = true)
    public static byte[] toByteArrayUtf8(String string) {
        return toByteArray(string, StandardCharsets.UTF_8);
    }

    @Contract(pure = true)
    public static @Nullable Byte toByteOrNull(String string) {
        return toByteOrNull(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static @Nullable Byte toByteOrNull(
        String string,
        int radix
    ) {
        try {
            return toByte(
                string,
                radix
            );
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract("_, _ -> param2")
    public static char[] toCharArray(
        CharSequence string,
        char[] destination
    ) {
        return toCharArray(
            string,
            destination,
            0
        );
    }

    @Contract("_, _, _ -> param2")
    public static char[] toCharArray(
        CharSequence string,
        char[] destination,
        int destinationOffset
    ) {
        requireNotNull(string);
        return toCharArray(
            string,
            destination,
            destinationOffset,
            0,
            string.length()
        );
    }

    @Contract("_, _, _, _, _ -> param2")
    public static char[] toCharArray(
        CharSequence string,
        char[] destination,
        int destinationOffset,
        int startIndex,
        int endIndex
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        if (startIndex < 0) {
            throw new IndexOutOfBoundsException(startIndex);
        }
        if (endIndex > string.length()) {
            throw new IndexOutOfBoundsException(endIndex);
        }
        final int length = endIndex - startIndex;
        requireGe0(
            length,
            () -> "The start index <" + startIndex + "> is greater than the end index <" + endIndex + '>'
        );
        for (int i = 0; i < length; ++i) {
            destination[destinationOffset + i] = string.charAt(startIndex + i);
        }
        return destination;
    }

    @Contract("_, _ -> param2")
    public static <C extends Collection<? super Character>> C toCollection(
        CharSequence string,
        C destination
    ) {
        requireNotNull(string);
        return toCollection(
            string,
            destination,
            0,
            string.length()
        );
    }

    @Contract("_, _, _, _ -> param2")
    public static <C extends Collection<? super Character>> C toCollection(
        CharSequence string,
        C destination,
        int startIndex,
        int endIndex
    ) {
        requireNotNull(string);
        requireNotNull(destination);
        if (startIndex < 0) {
            throw new IndexOutOfBoundsException(startIndex);
        }
        if (endIndex > string.length()) {
            throw new IndexOutOfBoundsException(endIndex);
        }
        require(
            endIndex >= startIndex,
            () -> "The start index <" + startIndex + "> is greater than the end index <" + endIndex + '>'
        );
        for (int i = startIndex; i < endIndex; ++i) {
            destination.add(string.charAt(i));
        }
        return destination;
    }

    @Contract(pure = true)
    public static double toDouble(String string) {
        requireNotNull(string);
        return Double.parseDouble(string);
    }

    @Contract(pure = true)
    public static @Nullable Double toDoubleOrNull(String string) {
        try {
            return toDouble(string);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static float toFloat(String string) {
        requireNotNull(string);
        return Float.parseFloat(string);
    }

    @Contract(pure = true)
    public static @Nullable Float toFloatOrNull(String string) {
        try {
            return toFloat(string);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract(value = "_ -> new", pure = true)
    @SuppressWarnings("NonApiType")
    public static HashSet<Character> toHashSet(CharSequence string) {
        requireNotNull(string);
        return toCollection(
            string,
            new HashSet<>(string.length())
        );
    }

    @Contract(pure = true)
    public static int toInt(String string) {
        return toInt(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static int toInt(
        String string,
        int radix
    ) {
        requireNotNull(string);
        return Integer.parseInt(string, radix);
    }

    @Contract(pure = true)
    public static @Nullable Integer toIntegerOrNull(String string) {
        return toIntegerOrNull(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static @Nullable Integer toIntegerOrNull(
        String string,
        int radix
    ) {
        try {
            return toInt(
                string,
                radix
            );
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract(value = "_ -> new", pure = true)
    public static List<Character> toList(CharSequence string) {
        if (isEmpty(string)) {
            return emptyList();
        }
        return toArrayList(string);
    }

    @Contract(pure = true)
    public static long toLong(String string) {
        return toLong(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static long toLong(
        String string,
        int radix
    ) {
        requireNotNull(string);
        return Long.parseLong(string, radix);
    }

    @Contract(pure = true)
    public static @Nullable Long toLongOrNull(String string) {
        return toLongOrNull(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static @Nullable Long toLongOrNull(
        String string,
        int radix
    ) {
        try {
            return toLong(
                string,
                radix
            );
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract(value = "_ -> new", pure = true)
    public static Set<Character> toSet(CharSequence string) {
        if (isEmpty(string)) {
            return emptySet();
        }
        return toHashSet(string);
    }

    @Contract(pure = true)
    public static short toShort(String string) {
        return toShort(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static short toShort(
        String string,
        int radix
    ) {
        requireNotNull(string);
        return Short.parseShort(string, radix);
    }

    @Contract(pure = true)
    public static @Nullable Short toShortOrNull(String string) {
        return toShortOrNull(
            string,
            10
        );
    }

    @Contract(pure = true)
    public static @Nullable Short toShortOrNull(
        String string,
        int radix
    ) {
        try {
            return toShort(
                string,
                radix
            );
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Contract(pure = true)
    public static String trim(CharSequence string) {
        return trim(
            string,
            Character::isWhitespace
        );
    }

    @Contract(pure = true)
    public static String trim(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        final int length = string.length();
        int firstIndex = 0;
        while (firstIndex < length && predicate.test(string.charAt(firstIndex))) {
            ++firstIndex;
        }
        int lastIndex = length - 1;
        while (lastIndex >= firstIndex && predicate.test(string.charAt(lastIndex))) {
            --lastIndex;
        }
        return substring(string, firstIndex, lastIndex + 1);
    }

    @Contract(pure = true)
    public static String trimEnd(CharSequence string) {
        return trimEnd(
            string,
            Character::isWhitespace
        );
    }

    @Contract(pure = true)
    public static String trimEnd(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        for (int i = string.length() - 1; i >= 0; --i) {
            if (!predicate.test(string.charAt(i))) {
                return substring(string, 0, i + 1);
            }
        }
        return EMPTY;
    }

    @Contract(pure = true)
    public static String trimStart(CharSequence string) {
        return trimStart(
            string,
            Character::isWhitespace
        );
    }

    @Contract(pure = true)
    public static String trimStart(
        CharSequence string,
        CharPredicate predicate
    ) {
        requireNotNull(string);
        requireNotNull(predicate);
        final int length = string.length();
        for (int i = 0; i < length; ++i) {
            if (!predicate.test(string.charAt(i))) {
                return substring(string, i, length);
            }
        }
        return EMPTY;
    }

    @Contract(pure = true)
    public static Iterable<IndexedChar> withIndex(String string) {
        requireNotNull(string);
        return () -> indexedIterator(string);
    }

    @Contract(pure = true)
    public static List<Pair<Character, Character>> zip(
        CharSequence string,
        CharSequence other
    ) {
        return zip(
            string,
            other,
            Pair::new
        );
    }

    @Contract(pure = true)
    public static <R> List<R> zip(
        CharSequence string,
        CharSequence other,
        BiFunction<? super Character, ? super Character, ? extends R> transform
    ) {
        requireNotNull(string);
        requireNotNull(other);
        requireNotNull(transform);
        final int length = min(string.length(), other.length());
        if (length == 0) {
            return emptyList();
        }
        final List<R> result = new ArrayList<>(length);
        for (int i = 0; i < length; ++i) {
            result.add(transform.apply(string.charAt(i), other.charAt(i)));
        }
        return result;
    }

    @Contract(pure = true)
    public static List<Pair<Character, Character>> zipWithNext(CharSequence string) {
        return zipWithNext(
            string,
            Pair::new
        );
    }

    @Contract(pure = true)
    public static <R> List<R> zipWithNext(
        CharSequence string,
        BiFunction<? super Character, ? super Character, ? extends R> transform
    ) {
        requireNotNull(string);
        requireNotNull(transform);
        final int length = string.length();
        if (length <= 1) {
            return emptyList();
        }
        final List<R> result = new ArrayList<>(length - 1);
        for (int i = 1; i < length; ++i) {
            result.add(transform.apply(string.charAt(i - 1), string.charAt(i)));
        }
        return result;
    }

    private Strings() {}
}
