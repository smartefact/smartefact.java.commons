/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

/**
 * {@link SplittingIterator} based on a delimiter.
 *
 * @author Laurent Pireyn
 */
final class DelimiterSplittingIterator extends SplittingIterator {
    private final char delimiter;
    private final boolean ignoreCase;
    private int delimiterLastIndex = -1;

    DelimiterSplittingIterator(
        CharSequence string,
        int limit,
        char delimiter,
        boolean ignoreCase
    ) {
        super(string, limit);

        this.delimiter = delimiter;
        this.ignoreCase = ignoreCase;
    }

    @Override
    int getDelimiterFirstIndex(int startIndex) {
        final int result = Strings.indexOf(string, delimiter, startIndex, ignoreCase);
        delimiterLastIndex = result;
        return result;
    }

    @Override
    int getDelimiterLastIndex() {
        return delimiterLastIndex;
    }
}
