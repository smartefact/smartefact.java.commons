/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;

/**
 * Utilities for {@code byte}s and {@link Byte}s.
 *
 * @author Laurent Pireyn
 */
public final class Bytes {
    @Contract(pure = true)
    public static byte clearBit(
        byte n,
        int index
    ) {
        return (byte) (n & 0xff & ~(1 << index));
    }

    @Contract(pure = true)
    public static byte coerceAtLeast(
        byte n,
        byte min
    ) {
        if (n < min) {
            return min;
        }
        return n;
    }

    @Contract(pure = true)
    public static byte coerceAtMost(
        byte n,
        byte max
    ) {
        if (n > max) {
            return max;
        }
        return n;
    }

    /**
     * Returns the given {@code byte} coerced to a value range.
     *
     * @param n the value
     * @param min the minimum value
     * @param max the maximum value
     * @return {@code n} coerced between {@code min} and {@code max}
     */
    @Contract(pure = true)
    public static byte coerceIn(
        byte n,
        byte min,
        byte max
    ) {
        require(
            min <= max,
            () -> "The minimum value <" + min + "> is greater than the maximum value <" + max + '>'
        );
        if (n < min) {
            return min;
        }
        if (n > max) {
            return max;
        }
        return n;
    }

    @Contract(pure = true)
    public static boolean in(
        byte n,
        byte first,
        byte last
    ) {
        return n >= first && n <= last;
    }

    @Contract(pure = true)
    public static boolean isEven(byte n) {
        return (n & 1) == 0;
    }

    @Contract(pure = true)
    public static boolean isOdd(byte n) {
        return (n & 1) != 0;
    }

    @Contract(pure = true)
    public static byte orElse(
        @Nullable Byte n,
        byte defaultValue
    ) {
        return n != null ? n : defaultValue;
    }

    @Contract(pure = true)
    public static byte setBit(
        byte n,
        int index
    ) {
        return (byte) (n & 0xff | 1 << index);
    }

    @Contract(pure = true)
    public static boolean testBit(
        byte n,
        int index
    ) {
        return (n & 0xff & 1 << index) != 0;
    }

    @Contract(pure = true)
    public static String toString(byte n) {
        return toString(n, 10);
    }

    @Contract(pure = true)
    public static String toString(
        byte n,
        int radix
    ) {
        // Byte.toString doesn't support radix
        return Integer.toString(n, radix);
    }

    @Contract(pure = true)
    public static byte toggleBit(
        byte n,
        int index
    ) {
        return (byte) (n & 0xff ^ 1 << index);
    }

    private Bytes() {}
}
