/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Objects;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.ToStringBuilder.toStringBuilder;

/**
 * Value and its index.
 *
 * @author Laurent Pireyn
 * @param <T> the type of value
 */
public final class IndexedValue<T> {
    private final int index;
    private final @Nullable T value;

    public IndexedValue(int index, @Nullable T value) {
        this.index = index;
        this.value = value;
    }

    @Contract(pure = true)
    public int index() {
        return index;
    }

    @Contract(pure = true)
    public @Nullable T value() {
        return value;
    }

    @Override
    @Contract(pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IndexedValue)) {
            return false;
        }
        final IndexedValue<?> other = (IndexedValue<?>) object;
        return index == other.index
            && Objects.equals(value, other.value);
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCodeBuilder()
            .property(index)
            .property(value)
            .build();
    }

    @Override
    @Contract(pure = true)
    public String toString() {
        return toStringBuilder(this)
            .property("index", index)
            .property("value", value)
            .build();
    }
}
