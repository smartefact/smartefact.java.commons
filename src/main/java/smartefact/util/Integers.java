/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;

/**
 * Utilities for {@code int}s and {@link Integer}s.
 *
 * @author Laurent Pireyn
 */
public final class Integers {
    @Contract(pure = true)
    public static int clearBit(
        int n,
        int index
    ) {
        return n & ~(1 << index);
    }

    @Contract(pure = true)
    public static int coerceAtLeast(
        int n,
        int min
    ) {
        return Math.max(n, min);
    }

    @Contract(pure = true)
    public static int coerceAtMost(
        int n,
        int max
    ) {
        return Math.min(n, max);
    }

    /**
     * Returns the given {@code int} coerced to a value range.
     *
     * @param n the value
     * @param min the minimum value
     * @param max the maximum value
     * @return {@code n} coerced between {@code min} and {@code max}
     */
    @Contract(pure = true)
    public static int coerceIn(
        int n,
        int min,
        int max
    ) {
        require(
            min <= max,
            () -> "The minimum value <" + min + "> is greater than the maximum value <" + max + '>'
        );
        return Math.min(Math.max(n, min), max);
    }

    @Contract(pure = true)
    public static boolean in(
        int n,
        int first,
        int last
    ) {
        return n >= first && n <= last;
    }

    @Contract(pure = true)
    public static boolean isEven(int n) {
        return (n & 1) == 0;
    }

    @Contract(pure = true)
    public static boolean isOdd(int n) {
        return (n & 1) != 0;
    }

    @Contract(pure = true)
    public static int orElse(
        @Nullable Integer n,
        int defaultValue
    ) {
        return n != null ? n : defaultValue;
    }

    @Contract(pure = true)
    public static int setBit(
        int n,
        int index
    ) {
        return n | 1 << index;
    }

    @Contract(pure = true)
    public static boolean testBit(
        int n,
        int index
    ) {
        return (n & 1 << index) != 0;
    }

    @Contract(pure = true)
    public static String toString(int n) {
        return toString(n, 10);
    }

    @Contract(pure = true)
    public static String toString(
        int n,
        int radix
    ) {
        return Integer.toString(n, radix);
    }

    @Contract(pure = true)
    public static int toggleBit(
        int n,
        int index
    ) {
        return n ^ 1 << index;
    }

    private Integers() {}
}
