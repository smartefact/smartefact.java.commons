/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;

/**
 * Utilities for {@code long}s and {@link Long}s.
 *
 * @author Laurent Pireyn
 */
public final class Longs {
    @Contract(pure = true)
    public static long clearBit(
        long n,
        int index
    ) {
        return n & ~(1L << index);
    }

    @Contract(pure = true)
    public static long coerceAtLeast(
        long n,
        long min
    ) {
        return Math.max(n, min);
    }

    @Contract(pure = true)
    public static long coerceAtMost(
        long n,
        long max
    ) {
        return Math.min(n, max);
    }

    /**
     * Returns the given {@code long} coerced to a value range.
     *
     * @param n the value
     * @param min the minimum value
     * @param max the maximum value
     * @return {@code n} coerced between {@code min} and {@code max}
     */
    @Contract(pure = true)
    public static long coerceIn(
        long n,
        long min,
        long max
    ) {
        require(
            min <= max,
            () -> "The minimum value <" + min + "> is greater than the maximum value <" + max + '>'
        );
        return Math.min(Math.max(n, min), max);
    }

    @Contract(pure = true)
    public static boolean in(
        long n,
        long first,
        long last
    ) {
        return n >= first && n <= last;
    }

    @Contract(pure = true)
    public static boolean isEven(long n) {
        return (n & 1L) == 0;
    }

    @Contract(pure = true)
    public static boolean isOdd(long n) {
        return (n & 1L) != 0;
    }

    @Contract(pure = true)
    public static long orElse(
        @Nullable Long n,
        long defaultValue
    ) {
        return n != null ? n : defaultValue;
    }

    @Contract(pure = true)
    public static long setBit(
        long n,
        int index
    ) {
        return n | 1L << index;
    }

    @Contract(pure = true)
    public static boolean testBit(
        long n,
        int index
    ) {
        return (n & 1L << index) != 0;
    }

    @Contract(pure = true)
    public static String toString(long n) {
        return toString(n, 10);
    }

    @Contract(pure = true)
    public static String toString(
        long n,
        int radix
    ) {
        return Long.toString(n, radix);
    }

    @Contract(pure = true)
    public static long toggleBit(
        long n,
        int index
    ) {
        return n ^ 1L << index;
    }

    private Longs() {}
}
