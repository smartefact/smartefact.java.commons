/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Locale;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Operating system family.
 *
 * @author Laurent Pireyn
 */
public enum OsFamily {
    LINUX,
    MAC,
    SOLARIS,
    UNIX,
    WINDOWS,
    OTHER;

    public static OsFamily fromOsName(String osName) {
        requireNotNull(osName);
        final String normalizedOsName = osName.toLowerCase(Locale.ROOT);
        if (normalizedOsName.contains("linux")) {
            return LINUX;
        }
        if (normalizedOsName.contains("mac")) {
            return MAC;
        }
        if (normalizedOsName.contains("sunos")) {
            return SOLARIS;
        }
        if (normalizedOsName.contains("unix")) {
            return UNIX;
        }
        if (normalizedOsName.contains("win")) {
            return WINDOWS;
        }
        return OTHER;
    }
}
