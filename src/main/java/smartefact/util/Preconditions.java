/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

/**
 * Functions that check preconditions.
 * <p>
 * There are two types of preconditions:
 * </p>
 * <ul>
 *     <li>
 *         Requirements:
 *         {@code require*}, throw {@link IllegalArgumentException}s
 *     </li>
 *     <li>
 *         Checks:
 *         {@code check*}, throw {@link IllegalStateException}s
 *     </li>
 * </ul>
 *
 * @author Laurent Pireyn
 */
public final class Preconditions {
    /**
     * Requires the given value to be {@code true}.
     *
     * @param value the value
     * @throws IllegalArgumentException if {@code value} is {@code false}
     */
    @Contract(value = "false -> fail", pure = true)
    public static void require(boolean value) {
        require(value, (String) null);
    }

    /**
     * Requires the given value to be {@code true}.
     *
     * @param value the value
     * @param message the exception message
     * @throws IllegalArgumentException if {@code value} is {@code false}
     */
    @Contract(value = "false, _ -> fail", pure = true)
    public static void require(
        boolean value,
        @Nullable String message
    ) {
        if (!value) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Requires the given value to be {@code true}.
     *
     * @param value the value
     * @param lazyMessage the supplier of the exception message
     * @throws IllegalArgumentException if {@code value} is {@code false}
     */
    @Contract(value = "false, _ -> fail", pure = true)
    public static void require(
        boolean value,
        Supplier<@Nullable String> lazyMessage
    ) {
        requireNotNull(lazyMessage);
        if (!value) {
            throw new IllegalArgumentException(lazyMessage.get());
        }
    }

    /**
     * Requires the given value <em>not</em> to be {@code null}.
     *
     * @param value the value
     * @param <T> the type of {@code value}
     * @return {@code value} if it is not {@code null}
     * @throws IllegalArgumentException if {@code value} is {@code null}
     */
    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T requireNotNull(@Nullable T value) {
        return requireNotNull(value, "The value is null");
    }

    /**
     * Requires the given value <em>not</em> to be {@code null}.
     *
     * @param value the value
     * @param message the exception message
     * @param <T> the type of {@code value}
     * @return {@code value} if it is not {@code null}
     * @throws IllegalArgumentException if {@code value} is {@code null}
     */
    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T requireNotNull(
        @Nullable T value,
        @Nullable String message
    ) {
        require(value != null, message);
        return value;
    }

    /**
     * Requires the given value <em>not</em> to be {@code null}.
     *
     * @param value the value
     * @param lazyMessage the supplier of the exception message
     * @param <T> the type of {@code value}
     * @return {@code value} if it is not {@code null}
     * @throws IllegalArgumentException if {@code value} is {@code null}
     */
    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T requireNotNull(
        @Nullable T value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value != null, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireGe0(int value) {
        return requireGe0(value, () -> "The value <" + value + "> is less than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireGe0(
        int value,
        @Nullable String message
    ) {
        require(value >= 0, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireGe0(
        int value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value >= 0, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireGe0(long value) {
        return requireGe0(value, () -> "The value <" + value + "> is less than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireGe0(
        long value,
        @Nullable String message
    ) {
        require(value >= 0L, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireGe0(
        long value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value >= 0L, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireGt0(int value) {
        return requireGt0(value, () -> "The value <" + value + "> is not greater than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireGt0(
        int value,
        @Nullable String message
    ) {
        require(value > 0, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireGt0(
        int value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value > 0, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireGt0(long value) {
        return requireGt0(value, () -> "The value <" + value + "> is not greater than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireGt0(
        long value,
        @Nullable String message
    ) {
        require(value > 0L, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireGt0(
        long value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value > 0L, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireLe0(int value) {
        return requireLe0(value, () -> "The value <" + value + "> is greater than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireLe0(
        int value,
        @Nullable String message
    ) {
        require(value <= 0, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireLe0(
        int value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value <= 0, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireLe0(long value) {
        return requireLe0(value, () -> "The value <" + value + "> is greater than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireLe0(
        long value,
        @Nullable String message
    ) {
        require(value <= 0L, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireLe0(
        long value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value <= 0L, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireLt0(int value) {
        return requireLt0(value, () -> "The value <" + value + "> is not less than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireLt0(
        int value,
        @Nullable String message
    ) {
        require(value < 0, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static int requireLt0(
        int value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value < 0, lazyMessage);
        return value;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireLt0(long value) {
        return requireLt0(value, () -> "The value <" + value + "> is not less than zero");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireLt0(
        long value,
        @Nullable String message
    ) {
        require(value < 0L, message);
        return value;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static long requireLt0(
        long value,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(value < 0L, lazyMessage);
        return value;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static boolean[] requireNotEmpty(boolean @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static boolean[] requireNotEmpty(
        boolean @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static byte[] requireNotEmpty(byte @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static byte[] requireNotEmpty(
        byte @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static char[] requireNotEmpty(char @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static char[] requireNotEmpty(
        char @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static double[] requireNotEmpty(double @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static double[] requireNotEmpty(
        double @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static float[] requireNotEmpty(float @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static float[] requireNotEmpty(
        float @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static int[] requireNotEmpty(int @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static int[] requireNotEmpty(
        int @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static long[] requireNotEmpty(long @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static long[] requireNotEmpty(
        long @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static short[] requireNotEmpty(short @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static short[] requireNotEmpty(
        short @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T[] requireNotEmpty(T @Nullable [] array) {
        return requireNotEmpty(array, "The array is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T[] requireNotEmpty(
        T @Nullable [] array,
        @Nullable String message
    ) {
        require(requireNotNull(array, message).length > 0, message);
        return array;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T extends CharSequence> T requireNotEmpty(@Nullable T string) {
        return requireNotEmpty(string, "The string is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T extends CharSequence> T requireNotEmpty(
        @Nullable T string,
        @Nullable String message
    ) {
        require(requireNotNull(string, message).length() > 0, message);
        return string;
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T extends CharSequence> T requireNotEmpty(
        @Nullable T string,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(requireNotNull(string, lazyMessage).length() > 0, lazyMessage);
        return string;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T extends Collection<?>> T requireNotEmpty(@Nullable T collection) {
        return requireNotEmpty(collection, "The collection is null or empty");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T extends Collection<?>> T requireNotEmpty(
        @Nullable T collection,
        @Nullable String message
    ) {
        require(!requireNotNull(collection, message).isEmpty(), message);
        return collection;
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T extends Collection<?>> T requireNotEmpty(
        @Nullable T collection,
        Supplier<@Nullable String> lazyMessage
    ) {
        require(!requireNotNull(collection, lazyMessage).isEmpty(), lazyMessage);
        return collection;
    }

    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T[] requireNoNullElements(@Nullable T @Nullable [] array) {
        return requireNoNullElements(array, "The array contains a null element");
    }

    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T[] requireNoNullElements(
        @Nullable T @Nullable [] array,
        @Nullable String message
    ) {
        requireNotNull(array);
        for (final T element : array) {
            if (element == null) {
                throw new IllegalArgumentException(message);
            }
        }
        return array;
    }

    @Contract(value = "_ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static <T extends Iterable<?>> T requireNoNullElements(T iterable) {
        return requireNoNullElements(iterable, "The iterable contains a null element");
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static <T extends Iterable<?>> T requireNoNullElements(
        T iterable,
        @Nullable String message
    ) {
        requireNotNull(iterable);
        for (final Object element : iterable) {
            if (element == null) {
                throw new IllegalArgumentException(message);
            }
        }
        return iterable;
    }

    @Contract(value = "_, _ -> param1", pure = true)
    @CanIgnoreReturnValue
    public static <T extends Iterable<?>> T requireNoNullElements(
        T iterable,
        IntFunction<@Nullable String> lazyMessage
    ) {
        requireNotNull(iterable);
        requireNotNull(lazyMessage);
        int index = 0;
        for (final Object element : iterable) {
            if (element == null) {
                throw new IllegalArgumentException(lazyMessage.apply(index));
            }
            ++index;
        }
        return iterable;
    }

    /**
     * Checks that the given value is {@code true}.
     *
     * @param value the value
     * @throws IllegalStateException if {@code value} is {@code false}
     */
    @Contract(value = "false -> fail", pure = true)
    public static void check(boolean value) {
        check(value, (String) null);
    }

    /**
     * Checks that the given value is {@code true}.
     *
     * @param value the value
     * @param message the exception message
     * @throws IllegalStateException if {@code value} is {@code false}
     */
    @Contract(value = "false, _ -> fail", pure = true)
    public static void check(
        boolean value,
        @Nullable String message
    ) {
        if (!value) {
            error(message);
        }
    }

    /**
     * Checks that the given value is {@code true}.
     *
     * @param value the value
     * @param lazyMessage the supplier of the exception message
     * @throws IllegalStateException if {@code value} is {@code false}
     */
    @Contract(value = "false, _ -> fail", pure = true)
    public static void check(
        boolean value,
        Supplier<@Nullable String> lazyMessage
    ) {
        requireNotNull(lazyMessage);
        if (!value) {
            error(lazyMessage.get());
        }
    }

    /**
     * Checks that the given value is <em>not</em> {@code null}.
     *
     * @param value the value
     * @param <T> the type of {@code value}
     * @return {@code value} if it is not {@code null}
     * @throws IllegalStateException if {@code value} is {@code null}
     */
    @Contract(value = "!null -> param1; null -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T checkNotNull(@Nullable T value) {
        return checkNotNull(value, "The value is null");
    }

    /**
     * Checks that the given value is <em>not</em> {@code null}.
     *
     * @param value the value
     * @param message the exception message
     * @param <T> the type of {@code value}
     * @return {@code value} if it is not {@code null}
     * @throws IllegalStateException if {@code value} is {@code null}
     */
    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T checkNotNull(
        @Nullable T value,
        @Nullable String message
    ) {
        check(value != null, message);
        return value;
    }

    /**
     * Checks that the given value is <em>not</em> {@code null}.
     *
     * @param value the value
     * @param lazyMessage the supplier of the exception message
     * @param <T> the type of {@code value}
     * @return {@code value} if it is not {@code null}
     * @throws IllegalStateException if {@code value} is {@code null}
     */
    @Contract(value = "!null, _ -> param1; null, _ -> fail", pure = true)
    @CanIgnoreReturnValue
    public static <T> T checkNotNull(
        @Nullable T value,
        Supplier<@Nullable String> lazyMessage
    ) {
        check(value != null, lazyMessage);
        return value;
    }

    /**
     * Throws an {@link IllegalStateException} with the given message.
     *
     * @param message the exception message
     * @throws IllegalStateException always
     */
    @Contract(value = "_ -> fail", pure = true)
    @SuppressWarnings("DoNotCallSuggester")
    public static void error(@Nullable String message) {
        throw new IllegalStateException(message);
    }

    private Preconditions() {}
}
