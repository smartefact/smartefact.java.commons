/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Line separator.
 *
 * @author Laurent Pireyn
 */
public enum LineSeparator {
    /**
     * The LF line separator (Unix, Linux, Mac OS X 10 and after).
     */
    LF("\n"),

    /**
     * The CR LF line separator (Windows).
     */
    CRLF("\r\n"),

    /**
     * The CR line separator (Mac OS 9 and before).
     */
    CR("\r");

    public static LineSeparator forString(String string) {
        requireNotNull(string);
        for (final var value : values()) {
            if (value.string.equals(string)) {
                return value;
            }
        }
        throw new IllegalArgumentException("The string <" + string + "> is not a supported line separator");
    }

    private final String string;
    
    @SuppressWarnings("ImmutableEnumChecker")
    private final byte[] bytes;

    LineSeparator(String string) {
        this.string = string;
        bytes = string.getBytes(StandardCharsets.US_ASCII);
    }

    /**
     * Returns the string value of this line separator.
     *
     * @return the string value of this line separator
     */
    public String getString() {
        return string;
    }

    /**
     * Writes this line separator to the given {@link Appendable}.
     *
     * @param appendable the appendable
     */
    public void writeTo(Appendable appendable) throws IOException {
        requireNotNull(appendable);
        appendable.append(string);
    }

    /**
     * Writes this line separator to the given {@link DataOutput}.
     *
     * @param output the output
     */
    public void writeTo(DataOutput output) throws IOException {
        requireNotNull(output);
        output.write(bytes);
    }
}
