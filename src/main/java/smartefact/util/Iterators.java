/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.emptyList;
import java.util.Iterator;
import java.util.List;
import org.jetbrains.annotations.Contract;

/**
 * Utilities for {@link Iterator}s.
 *
 * @author Laurent Pireyn
 */
@SuppressWarnings("MixedMutabilityReturnType")
final class Iterators {
    @Contract("_ -> new")
    @SuppressWarnings("NonApiType")
    static <E> ArrayList<E> toArrayList(Iterator<? extends E> iterator) {
        return toCollection(
            iterator,
            new ArrayList<>()
        );
    }

    @Contract("_, _ -> param2")
    static <E, C extends Collection<? super E>> C toCollection(
        Iterator<? extends E> iterator,
        C destination
    ) {
        while (iterator.hasNext()) {
            destination.add(iterator.next());
        }
        return destination;
    }

    static <E> List<E> toList(Iterator<? extends E> iterator) {
        if (!iterator.hasNext()) {
            return emptyList();
        }
        return toArrayList(iterator);
    }

    static int skip(
        Iterator<?> iterator,
        int n
    ) {
        int result = 0;
        while (result < n && iterator.hasNext()) {
            iterator.next();
            ++result;
        }
        return result;
    }

    private Iterators() {}
}
