/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.require;

/**
 * Utilities for {@code float}s and {@link Float}s.
 *
 * @author Laurent Pireyn
 */
public final class Floats {
    @Contract(pure = true)
    public static float coerceAtLeast(
        float n,
        float min
    ) {
        return Math.max(n, min);
    }

    @Contract(pure = true)
    public static float coerceAtMost(
        float n,
        float max
    ) {
        return Math.min(n, max);
    }

    /**
     * Returns the given {@code float} coerced to a value range.
     *
     * @param n the value
     * @param min the minimum value
     * @param max the maximum value
     * @return {@code n} coerced between {@code min} and {@code max}
     */
    @Contract(pure = true)
    public static float coerceIn(
        float n,
        float min,
        float max
    ) {
        require(
            min <= max,
            () -> "The minimum value <" + min + "> is greater than the maximum value <" + max + '>'
        );
        return Math.min(Math.max(n, min), max);
    }

    @Contract(pure = true)
    public static boolean in(
        float n,
        float first,
        float last
    ) {
        return n >= first && n <= last;
    }

    @Contract(pure = true)
    public static float orElse(
        @Nullable Float n,
        float defaultValue
    ) {
        return n != null ? n : defaultValue;
    }

    private Floats() {}
}
