/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.util;

import java.util.Collection;
import static java.util.Collections.unmodifiableList;
import java.util.List;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNoNullElements;
import static smartefact.util.Preconditions.requireNotEmpty;

/**
 * {@link RuntimeException} that contains multiple {@link Exception}s.
 *
 * @author Laurent Pireyn
 */
public class MultipleException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public static void maybeThrow(
        @Nullable String message,
        Collection<? extends Exception> exceptions
    ) {
        requireNoNullElements(exceptions);
        if (!exceptions.isEmpty()) {
            throw new MultipleException(message, exceptions);
        }
    }

    public static void maybeThrow(Collection<? extends Exception> exceptions) {
        maybeThrow(null, exceptions);
    }

    private final List<Exception> exceptions;

    public MultipleException(
        @Nullable String message,
        Collection<? extends Exception> exceptions
    ) {
        super(Objects.orElse(message, "Multiple exceptions"));
        requireNoNullElements(exceptions);
        requireNotEmpty(exceptions);
        this.exceptions = unmodifiableList(Collections.toList(exceptions));
    }

    public MultipleException(Collection<? extends Exception> exceptions) {
        this(null, exceptions);
    }

    @Contract(pure = true)
    public final List<Exception> getExceptions() {
        return exceptions;
    }
}
