/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.security;

/**
 * Standard {@link Digest}s.
 *
 * @author Laurent Pireyn
 * @see Digest#createDigest(String)
 */
public final class StandardDigests {
    /**
     * {@link Digest} for the <em>MD5</em> algorithm.
     *
     * @see <a href="https://en.wikipedia.org/wiki/MD5">Wikipedia</a>
     */
    public static final Digest MD5 = new MessageDigestDigest("MD5");

    /**
     * {@link Digest} for the <em>SHA-1</em> algorithm.
     *
     * @see <a href="https://en.wikipedia.org/wiki/SHA-1">Wikipedia</a>
     */
    public static final Digest SHA_1 = new MessageDigestDigest("SHA-1");

    /**
     * {@link Digest} for the <em>SHA-256</em> algorithm.
     *
     * @see <a href="https://en.wikipedia.org/wiki/SHA-2">Wikipedia</a>
     */
    public static final Digest SHA_256 = new MessageDigestDigest("SHA-256");

    private StandardDigests() {}
}
