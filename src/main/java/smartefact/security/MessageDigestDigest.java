/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.security;

import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import static smartefact.util.Preconditions.requireGe0;
import static smartefact.util.Preconditions.requireNotEmpty;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link Digest} based on a {@link MessageDigest}.
 * <p>
 * Instances of this class are created with an <em>algorithm</em>
 * rather than an instance of {@code MessageDigest},
 * as that class is not thread-safe.
 * </p>
 *
 * @author Laurent Pireyn
 */
final class MessageDigestDigest implements Digest {
    private final String algorithm;

    /**
     * Canonical name of the algorithm.
     */
    private final String name;
    private final int digestLength;

    MessageDigestDigest(String algorithm) {
        this.algorithm = requireNotEmpty(algorithm);
        final var messageDigest = createMessageDigest();
        name = messageDigest.getAlgorithm();
        digestLength = messageDigest.getDigestLength();
    }

    private MessageDigest createMessageDigest() {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("The algorithm <" + algorithm + "> is not supported", e);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getDigestLength() {
        return digestLength;
    }

    @Override
    public int digestTo(byte[] array, byte[] destination, int offset) {
        requireNotNull(array);
        requireNotNull(destination);
        requireGe0(offset);
        final var messageDigest = createMessageDigest();
        messageDigest.update(array);
        try {
            return messageDigest.digest(destination, offset, digestLength);
        } catch (DigestException | IllegalArgumentException e) {
            // A DigestException is thrown when destination is too small
            // An IllegalArgumentException is thrown when destination is null (we already required that)
            // or too small
            // In both cases, we assume the error is that destination is too small
            throw new IllegalArgumentException("The destination of length <" + destination.length + "> " +
                "is too small to contain a digest of <" + digestLength + "> bytes " +
                "at offset <" + offset + '>'
            );
        }
    }
}
