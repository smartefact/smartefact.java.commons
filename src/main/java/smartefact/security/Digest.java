/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.security;

import org.jetbrains.annotations.Contract;
import static smartefact.util.Preconditions.check;

/**
 * Digest.
 * <p>
 * This interface is a better alternative to {@link java.security.MessageDigest}.
 * </p>
 * <p>
 * Classes implementing this interface must be thread-safe.
 * </p>
 *
 * @author Laurent Pireyn
 */
public interface Digest {
    /**
     * Creates a {@code Digest} for the given algorithm.
     * <p>
     * Only the algorithms supported by {@link java.security.MessageDigest} are currently supported.
     * </p>
     *
     * @param algorithm the algorithm
     * @return a {@code Digest} for {@code algorithm} (never {@code null})
     * @see StandardDigests
     */
    static Digest createDigest(String algorithm) {
        return new MessageDigestDigest(algorithm);
    }

    @Contract(pure = true)
    String getName();

    @Contract(pure = true)
    int getDigestLength();

    @Contract(pure = true)
    default byte[] digest(byte[] array) {
        final var digestLength = getDigestLength();
        check(digestLength > 0);
        final var destination = new byte[digestLength];
        final var actualLength = digestTo(array, destination);
        check(actualLength == digestLength);
        return destination;
    }

    default int digestTo(byte[] array, byte[] destination) {
        return digestTo(array, destination, 0);
    }

    int digestTo(byte[] array, byte[] destination, int offset);
}
