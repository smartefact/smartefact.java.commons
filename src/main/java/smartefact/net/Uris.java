/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.net;

import java.net.URI;
import static java.util.Collections.emptyList;
import java.util.List;
import org.jetbrains.annotations.Contract;
import smartefact.util.Collections;
import smartefact.util.Objects;
import static smartefact.util.Preconditions.requireNotNull;
import smartefact.util.Strings;

/**
 * Utilities for {@link URI}s and their path component.
 *
 * @author Laurent Pireyn
 */
public final class Uris {
    public static final char SLASH = '/';

    @Contract(pure = true)
    public static List<String> getPathElements(String path) {
        requireNotNull(path);
        if (path.isEmpty()) {
            return emptyList();
        }
        final var elements = Strings.split(path, SLASH);
        assert !Collections.isEmpty(elements);
        return isAbsolutePath(path) ? Collections.drop(elements, 1) : Collections.toList(elements);
    }

    /**
     * Returns whether the given URI has the given scheme.
     *
     * @param uri the URI
     * @param scheme the scheme
     * @return {@code true} if {@code uri} has {@code scheme},
     * {@code false} otherwise
     * @see URI#getScheme()
     */
    @Contract(pure = true)
    public static boolean hasScheme(URI uri, String scheme) {
        requireNotNull(uri);
        requireNotNull(scheme);
        return Objects.equals(uri.getScheme(), scheme);
    }

    /**
     * Returns whether the given URI path is absolute.
     * <p>
     * A path is absolute if it starts with a slash.
     * </p>
     *
     * @param path the path
     * @return {@code true} if {@code path} is absolute,
     * {@code false} if it is relative
     * @see #isRelativePath(String)
     */
    @Contract(pure = true)
    public static boolean isAbsolutePath(String path) {
        return Strings.startsWith(path, SLASH);
    }

    /**
     * Returns whether the given URI path is relative.
     * <p>
     * A path is relative if it does not start with a slash.
     * </p>
     *
     * @param path the path
     * @return {@code true} if {@code path} is relative,
     * {@code false} if it is absolute
     * @see #isAbsolutePath(String)
     */
    @Contract(pure = true)
    public static boolean isRelativePath(String path) {
        return !isAbsolutePath(path);
    }

    /**
     * Returns whether the given URI is hierarchical.
     * <p>
     * A hierarchical URI is either an absolute URI whose path starts with a slash,
     * or a relative URI.
     * </p>
     *
     * @param uri the URI
     * @return {@code true} if {@code uri} is hierarchical,
     * {@code false} if it is opaque
     * @see URI#isOpaque()
     */
    @Contract(pure = true)
    public static boolean isHierarchical(URI uri) {
        requireNotNull(uri);
        return !uri.isOpaque();
    }

    /**
     * Returns whether the given URI is relative.
     * <p>
     * A relative URI does not have a scheme.
     * </p>
     *
     * @param uri the URI
     * @return {@code true} if {@code uri} is relative,
     * {@code false} if it is absolute
     * @see URI#isAbsolute()
     */
    @Contract(pure = true)
    public static boolean isRelative(URI uri) {
        requireNotNull(uri);
        return !uri.isAbsolute();
    }

    private Uris() {}
}
