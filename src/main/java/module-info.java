/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Common utilities.
 *
 * @author Laurent Pireyn
 */
module smartefact.commons {
    requires java.logging;

    requires static com.google.errorprone.annotations;
    requires static java.compiler;
    requires static org.jetbrains.annotations;

    exports smartefact.io;
    exports smartefact.net;
    exports smartefact.nio;
    exports smartefact.nio.file;
    exports smartefact.reflect;
    exports smartefact.security;
    exports smartefact.text;
    exports smartefact.util;
    exports smartefact.util.concurrent;
    exports smartefact.util.function;
    exports smartefact.util.logging;
}
