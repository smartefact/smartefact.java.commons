# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

## [0.3.0] - 2024-11-06

### Added

- Added `forEach` and `forEachIndexed` methods in `Strings`
- Added `javaEscaped` method in `Strings` and `Characters`
- Added `matches` method in `Strings`
- Added `single` method in `Strings`
- Added `joinTo` and `joinToString` method in `Collections`
- Added `addAll` method in `ModifiableCollections`
- Added `discard` methods in `InputStreams`
- Added `addSuppressed` method in `Exceptions`
- Added methods in `InputStreams`, `OutputStream` and `Readers`
- Added methods in `SystemProperties`
- Created `BigDecimals`
- Created `BigIntegers`
- Created `ByteBuffers`
- Created `LineSeparator`
- Created `Loggers`
- Created `MultipleException`
- Created `RuntimeIoException`
- Created `StandardSchemes`
- Created `Uris`

### Changed

- Changed signature of some methods in `SystemProperties`
- Convert properties of `JoinToString` from `CharSequence` to `String`

### Fixed

- Fixed handling of parse position in `RomanNumeralFormat`
- Fixed method in `ThrowingFunction`

## [0.2.0] - 2024-04-06

### Added

- Created `Doubles`
- Created `Floats`
- Created `RelativeDateTimeFormatter` (en, fr, nl)
- Created `DurationFormatter` (en, fr)
- Created `DigitalSizeFormatter` (en, fr)
- Created `ThrowingRunnable`
- Created `ThrowingPredicate`
- Created `ThrowingConsumer`
- Created `ThrowingSupplier`
- Created `ThrowingFunction`
- Created `ExecutorServiceWrapper`
- Created `ScheduledExecutorServiceWrapper`
- Created `SameThreadExecutorService`
- Created `ExecutorServices`
- Created `CompleteExecutor`
- Created `ScheduledExecutor`
- Created `CompletedFuture`
- Created `FailedFuture`
- Created `ShutdownTasks`
- Created `Digest`
- Created `StandardDigests`
- Added methods for arrays in `Preconditions`
- Added methods in `Strings`
- Added methods in `Classes`
- Added `skipEffectively` method in `InputStreams` and `Readers`
- Added overloaded methods that take `char` in `JoinToString`

### Changed

- Organized classes in subpackages
- Renamed type parameters in `IndexedBiFunction` for consistency

### Fixed

- Removed `@Nullable` annotations from properties in `Pair`

## [0.1.0] - 2023-09-04

### Added

- Created `AbstractClassLoader`
- Created `AbstractInputStream`
- Created `AbstractOutputStream`
- Created `AlphanumericString`
- Created `AlphanumericStringComparator`
- Created `Arrays`
- Created `BooleanPredicate`
- Created `BytePredicate`
- Created `Bytes`
- Created `CharFunction`
- Created `CharPredicate`
- Created `CharToCharFunction`
- Created `Characters`
- Created `Classes`
- Created `Collections`
- Created `CountingInputStream`
- Created `CountingOutputStream`
- Created `DoubleFunctions`
- Created `Either`
- Created `Exceptions`
- Created `Files`
- Created `FloatPredicate`
- Created `GlobalRandom`
- Created `HashCodeBuilder`
- Created `IndexedBiFunction`
- Created `IndexedChar`
- Created `IndexedConsumer`
- Created `IndexedFunction`
- Created `IndexedPredicate`
- Created `IndexedValue`
- Created `InputStreamWrapper`
- Created `InputStreams`
- Created `IntFunctions`
- Created `Integers`
- Created `LongFunctions`
- Created `Longs`
- Created `Maps`
- Created `ModifiableCollections`
- Created `Objects`
- Created `OsFamily`
- Created `OutputStreamWrapper`
- Created `OutputStreams`
- Created `Pair`
- Created `Paths`
- Created `Preconditions`
- Created `Randoms`
- Created `Readers`
- Created `RomanNumeralFormat`
- Created `ShortPredicate`
- Created `Strings`
- Created `SystemProperties`
- Created `ToStringBuilder`
- Created `TypeReference`
- Created `Writers`
