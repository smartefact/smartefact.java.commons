[![Latest release](https://gitlab.com/smartefact/smartefact.java.commons/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.commons/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.commons/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.commons/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.commons/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.commons/-/commits/main)

# smartefact.java.commons

Common Java utilities.

[Javadoc](https://smartefact.gitlab.io/smartefact.java.commons)

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
